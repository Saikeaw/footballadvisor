<?php 
/* Template Name: Template 2 */
require_once ABSPATH . 'wp-load.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
<head>
<link href="/" rel="canonical" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  
  <title><?php wp_title(''); ?></title>
  <style type="text/css">

	#wrapper { margin: 0 auto; width: 980px;padding:0;}
	.s-c-s #colmid { left:0px;}
	.s-c-s #colright { margin-left:-250px;}
	.s-c-s #col1pad { margin-left:250px;}
	.s-c-s #col2 { left:250px;width:0px;}
	.s-c-s #col3 { width:250px;}
	
	.s-c-x #colright { left:0px;}
	.s-c-x #col1wrap { right:0px;}
	.s-c-x #col1 { margin-left:0px;}
	.s-c-x #col2 { right:0px;width:0px;}
	
	.x-c-s #colright { margin-left:-250px;}
	.x-c-s #col1 { margin-left:250px;}
	.x-c-s #col3 { left:250px;width:250px;}
  </style>
  
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/jquery-ui-1.8.24.custom.css?v=5" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/theme.css?v=5" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/modules.css?v=5" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/custom_styles_modules.css?v=5" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/custom_styles.css?v=5" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/custom_css.php" />

  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/template/jquery-1.8.2.min.js?v=5"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/template/jquery-ui-1.8.24.custom.min-ck.js?v=5"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/template/jquery.ui.datepicker-en-GB.js?v=5"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/template/jreviews-ck.js?v=5"></script>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/light.css" type="text/css" />
<!--[if lte IE 6]>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/template/ie_suckerfish.js"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/styles.ie.css" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/template/styles.ie7.css" type="text/css" />
<![endif]-->
  
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<?php wp_head(); ?>
</head>
<body>
<div class="background"></div>

<?php if (have_posts()) : the_post(); ?>

<?php
	$header_in_grid = false;
	if ($qode_options_infographer['header_in_grid'] == "yes") $header_in_grid = true;
	
	$header_hide = false;
	if ($qode_options_infographer['header_hide'] == "yes") $header_hide = true;
	
	$menu_position = "left";
	if ($qode_options_infographer['menu_position'] == "right") $menu_position = "right";
	
	
	if($qode_options_infographer['menu_lineheight'] != ""){
		$line_height = $qode_options_infographer['menu_lineheight'];
	}else{
		$line_height = 80;
	}
?>

<?php if(!$header_hide){ ?>
<header <?php if(isset($qode_options_infographer['header_fixed']) && $qode_options_infographer['header_fixed'] == "no"){ echo "class='no_fixed'"; } ?>>
	<div class="header_outer">
		<?php if($header_in_grid){ ?>
			<div class="container">
				<div class="container_inner clearfix">
		<?php } ?>
				<div class="header_inner clearfix">
					<div class="logo" style="height:<?php echo $line_height?>px;"><a href="<?php echo home_url(); ?>/"><img src="<?php echo $qode_options_infographer['logo_image']; ?>" alt="Logo"/></a></div>
					
						<nav class="main_menu drop_down <?php echo $menu_position; ?>">
						<?php
								wp_nav_menu( array( 'theme_location' => 'top-navigation' , 
																		'container'  => '', 
																		'container_class' => '', 
																		'menu_class' => 'clearfix', 
																		'menu_id' => '',
																		'fallback_cb' => 'top_navigation_fallback',
																		'walker' => new qode_type1_walker_nav_menu()
							 ));
						?>
						</nav>
						
						
						
						<div class='selectnav_button'><span>&nbsp;</span></div>
						
						<?php	
						$display_header_widget = $qode_options_infographer['header_widget_area'];
						if($display_header_widget == "yes"){ ?> 
							<div class="header_right_widget">
								<?php dynamic_sidebar('header_right'); ?>
							</div>
						<?php } ?>
						
					

					<nav class="selectnav"></nav>
					
				</div>

<div class="header-inner second-menu-wrapper">
<?php if (!is_home() && !is_front_page() ) { ?>
							<nav class="main_menu drop_down <?php echo $menu_position; ?>">
						<?php
								wp_nav_menu( array( 								'container'  => '', 
																		'menu' => 47,
																		'container_class' => '', 
																		'menu_class' => 'clearfix drop-down', 
																		'menu_id' => '',
																		'walker' => new qode_type1_walker_nav_menu()
																		
							 ));
						?>
						</nav>
						<? } ?>
						<div class="clearfix"></div>
</div>
			
			
		<?php if($header_in_grid){ ?>
				</div>
			</div>
		<?php } ?>
		
		
	</div>
</header>
<?php } ?>

<div id="main">
	<div id="wrapper" class="foreground">
    	<!-- <div id="header">
        	<a href="<?php the_field('header_logo_url'); ?>" id="logo" style="background-image:url(<?php the_field('header_logo_image'); ?>);"></a>
    	</div> -->
    	<div id="nav">       
			<ul class="menu">
			<?php if(get_field('header_menu')): ?>
				<?php while(has_sub_field('header_menu')): ?>
					<li class="item menu-item"><a href="<?php the_sub_field('menu_link'); ?>"><?php the_sub_field('menu_title'); ?></a></li>
				<?php endwhile; ?>
			<?php endif; ?>
			</ul>
      		<div style="clear:both;"></div>
    	</div>
    	<div id="message">
			<div id="system-message-container"></div>
    	</div>
        
        <div id="main-content" class="x-c-s">
            <div id="colmask" class="ckl-color2">
                <div id="colmid" class="cdr-color1">
                    <div id="colright" class="ctr-color1">
                        <div id="col1wrap">
        				    <div id="col1pad">
                        		<div id="col1">
                                	<div class="component-pad">
                                        <div class="item-page">
											<?php the_field('betting_intro'); ?>

											<?php if(get_field('betting_sites')): $x = 0; ?>
												<?php while(has_sub_field('betting_sites')): $x++; ?>
<div class="listbox">
	<h3><?php echo $x; ?>. <?php the_sub_field('title'); ?> - <?php the_sub_field('subtitle'); ?></h3>
	<div class="listimage"><img width="255" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?> Logo">
	<div class="listimagevisit"><a href="<?php the_sub_field('link'); ?>" target="_blank">Click here to visit <?php the_sub_field('title'); ?></a></div>
	</div>
	<?php the_sub_field('content'); ?>
	<div class="listboxvisit"><a href="<?php the_sub_field('link'); ?>" target="_blank">Click here to visit <?php the_sub_field('title'); ?></a></div>
</div>
												<?php endwhile; ?>
											<?php endif; ?>
											
											<?php the_content(); ?>	
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- SIDEBAR -->
						<div id="col3" class="color1">
							<?php if(get_field('sidebar')): ?>
							<?php while(has_sub_field('sidebar')): ?>
							<div class="module">
								<?php if (get_sub_field('title') != '') : ?>
								<h3 class="module-title"><?php the_sub_field('title'); ?></h3>
								<?php endif; ?>
							    <div class="module-body">     
							    	<?php the_sub_field('content'); ?> 
								</div>
						    </div>
							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="module">
			<div class="module-body">
				<div class="custom"  >
					<table style="width: 100%;" border="0">
						<tbody>
							<tr>
								<td>Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('title'); ?></td>		
							</tr>
						</tbody>
					</table>
				</div>
		    </div>
		</div>
	</div>
</div>

<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>