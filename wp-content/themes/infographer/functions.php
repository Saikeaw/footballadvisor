<?php

define('PMPRO_FAILED_PAYMENT_LIMIT', 3);

show_admin_bar( false );
load_theme_textdomain( 'qode', get_template_directory().'/languages' );

if(isset($qode_toolbar)):
		
add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

/* Start session */
if (!function_exists('myStartSession')) {
function myStartSession() {
    if(!session_id()) {
        session_start();
    }
		
		if (!empty($_GET['animation']))
			$_SESSION['qode_animation'] = $_GET['animation'];
}}

/* End session */

if (!function_exists('myEndSession')) {
function myEndSession() {
    session_destroy ();
}
}

endif;

add_filter('widget_text', 'do_shortcode');
add_filter( 'the_excerpt', 'do_shortcode');

define('QODE_ROOT', get_template_directory_uri());
define('QODE_VAR_PREFIX', 'qode_'); 
include_once('widgets/relate_posts_widget.php');
include_once('includes/shortcodes.php');
include_once('includes/qode-options.php');
include_once('includes/custom-fields.php');
include_once('includes/qode-menu.php');

/* Add css */

if (!function_exists('qode_styles')) {
function qode_styles() {
	global $qode_options_infographer;
	global $wp_styles;
	global $qode_toolbar;
	wp_enqueue_style("default_style", QODE_ROOT."/style.css");
	wp_enqueue_style("stylesheet", QODE_ROOT . "/css/stylesheet.min.css");
	
	wp_enqueue_style('ie8-style',QODE_ROOT . '/css/ie8.min.css');
	$wp_styles->add_data( 'ie8-style', 'conditional', 'IE 8');
	
	wp_enqueue_style('ie9-style',QODE_ROOT . '/css/ie9.min.css');
	$wp_styles->add_data( 'ie9-style', 'conditional', 'IE 9' );
		
	wp_enqueue_style("style_dynamic", QODE_ROOT."/css/style_dynamic.php");
	
	$responsiveness = "yes";
	if (isset($qode_options_infographer['responsiveness'])) $responsiveness = $qode_options_infographer['responsiveness'];
	if($responsiveness != "no"):
	wp_enqueue_style("responsive", QODE_ROOT."/css/responsive.min.css");
	wp_enqueue_style("style_dynamic_responsive", QODE_ROOT."/css/style_dynamic_responsive.php");
	endif;
	if(isset($qode_toolbar)):
		wp_enqueue_style("toolbar", QODE_ROOT."/css/toolbar.css");
	endif;
	
	wp_enqueue_style("custom_css", QODE_ROOT."/css/custom_css.php");

	
	$fonts_array  = array(
						$qode_options_infographer['google_fonts'].':200,300,400',
						$qode_options_infographer['button_title_google_fonts'].':200,300,400',
						$qode_options_infographer['message_title_google_fonts'].':200,300,400',
						$qode_options_infographer['h1_google_fonts'].':200,300,400',
						$qode_options_infographer['page_title_google_fonts'].':200,300,400',
						$qode_options_infographer['h2_google_fonts'].':200,300,400',
						$qode_options_infographer['h3_google_fonts'].':200,300,400',
						$qode_options_infographer['h4_google_fonts'].':200,300,400',
						$qode_options_infographer['h5_google_fonts'].':200,300,400',
						$qode_options_infographer['h6_google_fonts'].':200,300,400',
						$qode_options_infographer['text_google_fonts'].':200,300,400',
						$qode_options_infographer['menu_google_fonts'].':200,300,400',
						$qode_options_infographer['dropdown_google_fonts'].':200,300,400',
						$qode_options_infographer['dropdown_google_fonts_thirdlvl'].':200,300,400',
						$qode_options_infographer['content_menu_google_fonts'].':200,300,400'
						
						);
	
	$fonts_array=array_diff($fonts_array, array("-1:200,300,400"));
	$google_fonts_string = implode( '|', $fonts_array);
	if(count($fonts_array) > 0) :
		printf("<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|PT+Sans:400,400italic,700,700italic|Oswald:400,300,700|%s&subset=latin,latin-ext' type='text/css' />\r\n", str_replace(' ', '+', $google_fonts_string));
	else :
		printf("<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|PT+Sans:400,400italic,700,700italic|Oswald:400,300,700&subset=latin,latin-ext' type='text/css' />\r\n");
	endif;
	
}
}

/* Add js */

if (!function_exists('qode_scripts')) {
function qode_scripts() {
	global $qode_options_infographer;
	global $is_IE;
	global $qode_toolbar;
	wp_enqueue_script("jquery");
	wp_enqueue_script("plugins", QODE_ROOT."/js/plugins.js",array(),false,true);
	if ( $is_IE ) {
		wp_enqueue_script("html5", QODE_ROOT."/js/html5.js",array(),false,false);
	}
	if($qode_options_infographer['enable_google_map'] == "yes") :
		wp_enqueue_script("google_map_api", "https://maps.googleapis.com/maps/api/js?sensor=false",array(),false,true);
	endif;
	wp_enqueue_script("default_dynamic", QODE_ROOT."/js/default_dynamic.php",array(),false,true);
	wp_enqueue_script("default", QODE_ROOT."/js/default.min.js",array(),false,true);
	wp_enqueue_script("custom_js", QODE_ROOT."/js/custom_js.php",array(),false,true);
	global $wp_scripts;
	$wp_scripts->add_data('comment-reply', 'group', 1 );
	if ( is_singular() ) wp_enqueue_script( "comment-reply");
	
	$has_ajax = false;
	$qode_animation = "";
	if (isset($_SESSION['qode_animation']))
		$qode_animation = $_SESSION['qode_animation'];
	if (($qode_options_infographer['page_transitions'] != "0") && (empty($qode_animation) || ($qode_animation != "no")))
		$has_ajax = true;
	elseif (!empty($qode_animation) && ($qode_animation != "no"))
		$has_ajax = true;
		
	if ($has_ajax) :
		wp_enqueue_script("ajax", QODE_ROOT."/js/ajax.min.js",array(),false,true);
	endif;
	
	if($qode_options_infographer['use_recaptcha'] == "yes") :
	wp_enqueue_script("recaptcha_ajax", "http://www.google.com/recaptcha/api/js/recaptcha_ajax.js",array(),false,true);
	endif;
	if(isset($qode_toolbar)):
		wp_enqueue_script("toolbar", QODE_ROOT."/js/toolbar.js",array(),false,true);
		
	endif;
	
	
}
}

add_action('wp_enqueue_scripts', 'qode_styles'); 
add_action('wp_enqueue_scripts', 'qode_scripts');

/* Add admin js and css */

if (!function_exists('qode_admin_jquery')) {
function qode_admin_jquery() {
		wp_enqueue_script('jquery'); 
		wp_enqueue_style('style', QODE_ROOT.'/css/admin/admin-style.css', false, '1.0', 'screen');
		wp_enqueue_style('colorstyle', QODE_ROOT.'/css/admin/colorpicker.css', false, '1.0', 'screen');
		wp_register_script('colorpickerss', QODE_ROOT.'/js/admin/colorpicker.js', array('jquery'), '1.0.0', false );
		wp_enqueue_script('colorpickerss'); 
		wp_enqueue_style('thickbox');
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script('jquery-ui-accordion');
		wp_register_script('default', QODE_ROOT.'/js/admin/default.js', array('jquery'), '1.0.0', false );
		wp_enqueue_script('default'); 
		wp_enqueue_script('common');
		wp_enqueue_script('wp-lists');
		wp_enqueue_script('postbox');
	
}
}
add_action('admin_print_scripts', 'qode_admin_jquery');


if (!isset( $content_width )) $content_width = 940;

/* Remove Generator from head */

remove_action('wp_head', 'wp_generator'); 

/* Register Menus */

if (!function_exists('qode_register_menus')) {
function qode_register_menus() {
    register_nav_menus(
        array('top-navigation' => __( 'Top Navigation', 'qode')
		)
		
    );
}
}
add_action( 'init', 'qode_register_menus' ); 

/* Add post thumbnails */

if ( function_exists( 'add_theme_support' ) ) { 

add_theme_support( 'post-thumbnails' );
add_image_size( 'blog-type-1', 388, 263, true );
add_image_size( 'blog-type-2', 1102, 412, true );
add_image_size( 'blog-type-3', 490, 256, true );
add_image_size( 'latest_posts', 503, 283, true );
}

/* Add feedlinks */

add_theme_support( 'automatic-feed-links' );

/* Qode comments */

if (!function_exists('qode_comment')) {
function qode_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>

<li>                        
	<div class="comment">
	  <div class="image"> <?php echo get_avatar($comment, 120); ?> </div>
	  <div class="text">
			<span class="name"><?php echo get_comment_author_link(); ?></span>
			<div class="text_holder" id="comment-<?php echo comment_ID(); ?>">
				<?php comment_text(); ?>
			</div>
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		
		</div>
	</div>
                             
                
<?php if ($comment->comment_approved == '0') : ?>
<p><em><?php _e('Your comment is awaiting moderation.', qode); ?></em></p>
<?php endif; ?>                
<?php 
}
}
/* Register Sidebar */

if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'Sidebar',
				'id' => 'sidebar',
        'description' => 'Default sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s posts_holder">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
		register_sidebar(array(
        'name' => 'SidebarPage',
				'id' => 'sidebar_page',
        'description' => 'Sidebar for Page',
        'before_widget' => '<div id="%1$s" class="widget %2$s posts_holder">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
		register_sidebar(array(
        'name' => 'Header right',
				'id' => 'header_right',
				'description' => 'Header right',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
		register_sidebar(array(
				'name' => 'Footer column 1',
				'id' => 'footer_column_1',
        'description' => 'Footer column 1',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
		register_sidebar(array(
				'name' => 'Footer column 2',
				'id' => 'footer_column_2',
        'description' => 'Footer column 2',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
		register_sidebar(array(
				'name' => 'Footer column 3',
				'id' => 'footer_column_3',
        'description' => 'Footer column 3',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
		register_sidebar(array(
				'name' => 'Footer column 4',
				'id' => 'footer_column_4',
        'description' => 'Footer column 4',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
		register_sidebar(array(
        'name' => 'Footer text',
				'id' => 'footer_text',
        'description' => 'Footer text',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
   
}

/* Add class on body for ajax */

if (!function_exists('ajax_classes')) {
function ajax_classes($classes) {
	global $qode_options_infographer;
	$qode_animation="";
	if (isset($_SESSION['qode_animation'])) $qode_animation = $_SESSION['qode_animation'];
	if(($qode_options_infographer['page_transitions'] === "0") && ($qode_animation == "no")) :
		$classes[] = '';
	elseif($qode_options_infographer['page_transitions'] === "1" && (empty($qode_animation) || ($qode_animation != "no"))) :
		$classes[] = 'ajax_updown';
		$classes[] = 'page_not_loaded';
	elseif($qode_options_infographer['page_transitions'] === "2" && (empty($qode_animation) || ($qode_animation != "no"))) :
		$classes[] = 'ajax_fade';
		$classes[] = 'page_not_loaded';
	elseif($qode_options_infographer['page_transitions'] === "3" && (empty($qode_animation) || ($qode_animation != "no"))) :
		$classes[] = 'ajax_updown_fade';
		$classes[] = 'page_not_loaded';
	elseif($qode_options_infographer['page_transitions'] === "4" && (empty($qode_animation) || ($qode_animation != "no"))) :
		$classes[] = 'ajax_leftright';
		$classes[] = 'page_not_loaded';
	elseif(!empty($qode_animation) && $qode_animation != "no") :
		$classes[] = 'page_not_loaded';
	else:
	$classes[] ="";
	endif;

	return $classes;
}
}
add_filter('body_class','ajax_classes');

/* Add class on body boxed layout */

if (!function_exists('boxed_class')) {
function boxed_class($classes) {
	global $qode_options_infographer;
	
	if($qode_options_infographer['boxed'] == "yes") :
		$classes[] = 'boxed';
	else:
	$classes[] ="";
	endif;

	return $classes;
}
}
add_filter('body_class','boxed_class');


/* Add class for no delay on body */

if (!function_exists('delay_class')) {
function delay_class($classes) {
	global $qode_options_infographer;
	
	$delay = false;
	if(isset($qode_options_infographer['delay'])){
		if ($qode_options_infographer['delay'] == "yes") $delay = true;
	}
	if($delay) :
		$classes[] = 'no_delay';
	else:
	$classes[] ="";
	endif;

	return $classes;
}
}
add_filter('body_class','delay_class');

/* Excerpt more */

if (!function_exists('qode_excerpt_more')) {
function qode_excerpt_more( $more ) {
    return '...';
}
}
add_filter('excerpt_more', 'qode_excerpt_more');

/* Excerpt lenght */

if (!function_exists('qode_excerpt_length')) {
function qode_excerpt_length( $length ) {
	global $qode_options_infographer;
	if($qode_options_infographer['number_of_chars']){
		 return $qode_options_infographer['number_of_chars'];
	} else {
		return 45;
	}
}
}
add_filter( 'excerpt_length', 'qode_excerpt_length', 999 );


/* Social excerpt lenght */

if (!function_exists('the_excerpt_max_charlength')) {
function the_excerpt_max_charlength($charlength) {
	global $qode_options_infographer;
	$via = $qode_options_infographer['twitter_via'];
	$excerpt = get_the_excerpt();
	$charlength = 136 - (mb_strlen($via) + $charlength);

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			return mb_substr( $subex, 0, $excut );
		} else {
			return $subex;
		}
	} else {
		return $excerpt;
	}
}
}

/* Create Portfolio post type */

if (!function_exists('create_post_type')) {
function create_post_type() {
	register_post_type( 'portfolio_page',
		array(
			'labels' => array(
				'name' => __( 'Portfolio','qode' ),
				'singular_name' => __( 'Portfolio Item','qode' ),
				'add_item' => __('New Portfolio Item','qode'),
                'add_new_item' => __('Add New Portfolio Item','qode'),
                'edit_item' => __('Edit Portfolio Item','qode')
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'portfolio_page'),
			'menu_position' => 4,
			'show_ui' => true,
            'supports' => array('author', 'title', 'editor', 'thumbnail', 'excerpt', 'post-formats', 'page-attributes')
		)
	);
	  flush_rewrite_rules();
}
}
add_action( 'init', 'create_post_type' );

/* Create Portfolio Categories */

add_action( 'init', 'create_portfolio_taxonomies', 0 );
if (!function_exists('create_portfolio_taxonomies')) {
function create_portfolio_taxonomies() 
{
   $labels = array(
    'name' => __( 'Portfolio Categories', 'taxonomy general name' ),
    'singular_name' => __( 'Portfolio Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Portfolio Categories','qode' ),
    'all_items' => __( 'All Portfolio Categories','qode' ),
    'parent_item' => __( 'Parent Portfolio Category','qode' ),
    'parent_item_colon' => __( 'Parent Portfolio Category:','qode' ),
    'edit_item' => __( 'Edit Portfolio Category','qode' ), 
    'update_item' => __( 'Update Portfolio Category','qode' ),
    'add_new_item' => __( 'Add New Portfolio Category','qode' ),
    'new_item_name' => __( 'New Portfolio Category Name','qode' ),
    'menu_name' => __( 'Portfolio Categories','qode' ),
  );     

  register_taxonomy('portfolio_category',array('portfolio_page'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'portfolio-category' ),
  ));

}
}

/* Pagination */

if (!function_exists('pagination')) {
function pagination($pages = '', $range = 4, $paged = 1)
{  
	global $qode_options_infographer;
     $showitems = $range+1;  
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
		
         echo "<div class='pagination'><ul>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
		echo "<li class='prev'><a href='".get_pagenum_link($paged - 1)."'></a></li>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class='active'><span>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class='inactive'>".$i."</a></li>";
             }
         }
		
         echo "<li class='next'><a href=\"";
		 if($pages > $paged) {
		 echo get_pagenum_link($paged + 1);
		 }
		 else {
			 echo get_pagenum_link($paged);
		 }
		 echo "\"></a></li>";  
		 
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</ul></div>\n";
     }
}
}
add_filter('the_content', 'shortcode_empty_paragraph_fix');

/* Empty paragraph fix in shortcode */

if (!function_exists('shortcode_empty_paragraph_fix')) {
function shortcode_empty_paragraph_fix($content)
{   
    $array = array (
        '<p>[' => '[', 
        ']</p>' => ']', 
        ']<br />' => ']'
    );

    $content = strtr($content, $array);

    return $content;
}
}

/* Use slider instead of image for post */

if (!function_exists('slider_blog')) {
function slider_blog($post_id) {
	$sliders = get_post_meta($post_id, "qode_sliders", true);		
	$slider = $sliders[1];
	if($slider) {
		$html .= '<div class="flexslider"><ul class="slides">';
		$i=0;
		while ($slider[$i])
		{
			$slide = $slider[$i];
			
			$href = $slide[link];
			$baseurl = home_url();
			$baseurl = str_replace('http://', '', $baseurl);
			$baseurl = str_replace('www', '', $baseurl);
			$host = parse_url($href, PHP_URL_HOST);
			if($host != $baseurl) {
				$target = 'target="_blank"';
			}
			else {
				$target = 'target="_self"';
			}
			
			$html .= '<li class="slide ' . $slide[imgsize] . '">';
			$html .= '<div class="image"><img src="' . $slide[img] . '" alt="' . $slide[title] . '" /></div>';
			
			$html .= '</li>';
			$i++; 
		}
		$html .= '</ul></div>';
	}
	return $html;
}
}

if (!function_exists('compareSlides')) {
function compareSlides($a, $b)
{
	if (isset($a['ordernumber']) && isset($b['ordernumber'])) {
    if ($a['ordernumber'] == $b['ordernumber']) {
        return 0;
    }
    return ($a['ordernumber'] < $b['ordernumber']) ? -1 : 1;
  }
  return 0;
}
}

if (!function_exists('comparePortfolioImages')) {
function comparePortfolioImages($a, $b)
{
	if (isset($a['portfolioimgordernumber']) && isset($b['portfolioimgordernumber'])) {
    if ($a['portfolioimgordernumber'] == $b['portfolioimgordernumber']) {
        return 0;
    }
    return ($a['portfolioimgordernumber'] < $b['portfolioimgordernumber']) ? -1 : 1;
  }
  return 0;
}
}

if (!function_exists('comparePortfolioOptions')) {
function comparePortfolioOptions($a, $b)
{
	if (isset($a['optionlabelordernumber']) && isset($b['optionlabelordernumber'])) {
    if ($a['optionlabelordernumber'] == $b['optionlabelordernumber']) {
        return 0;
    }
    return ($a['optionlabelordernumber'] < $b['optionlabelordernumber']) ? -1 : 1;
  }
  return 0;
}
}


/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/includes/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
if (!function_exists('my_theme_register_required_plugins')) {
function my_theme_register_required_plugins() {

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin pre-packaged with a theme
		array(
			'name'     				=> 'Revolution Slider Plugin', // The plugin name
			'slug'     				=> 'revslider', // The plugin slug (typically the folder name)
			'source'   				=> get_stylesheet_directory() . '/plugins/revslider.zip', // The plugin source
			'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),

		// This is an example of how to include a plugin from the WordPress Plugin Repository
		// array(
			// 'name' 		=> 'BuddyPress',
			// 'slug' 		=> 'buddypress',
			// 'required' 	=> false,
		// ),

	);

	// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'infographer';

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'       		=> $theme_text_domain,         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
		'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> false,					   	// Automatically activate plugins after installation or not
		'message' 			=> '',							// Message to output right before the plugins table
		'strings'      		=> array(
			'page_title'                       			=> __( 'Install Required Plugins', $theme_text_domain ),
			'menu_title'                       			=> __( 'Install Plugins', $theme_text_domain ),
			'installing'                       			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
			'oops'                             			=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
			'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
			'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
			'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
			'return'                           			=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
			'plugin_activated'                 			=> __( 'Plugin activated successfully.', $theme_text_domain ),
			'complete' 									=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
			'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa( $plugins, $config );

}
}


if (!function_exists('hex2rgb')) {
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}
}


if (!function_exists('getImageAltFromURL')) {
function getImageAltFromURL($url) {
	global $wpdb;
	$alt_text = "";
	if (!empty($url)) {
		$thepost = $wpdb->get_row( $wpdb->prepare( "SELECT *
		FROM $wpdb->posts WHERE guid = '%s'",$url ) );
		if ($thepost != null) {
			$theID = $thepost->ID;
			$theTitle = $thepost->post_title;
			
			$alt_text = get_post_meta($theID, '_wp_attachment_image_alt', true);
			if ($alt_text == NULL)
			{
				$alt_text = "";
			}
		}
	}
	return ($alt_text);
}
}

define('PMPRO_IPN_DEBUG', 'Tips@footballadvisor.net');

//******************************************

function showSelections(){
	wp_enqueue_style( 'datatablecss', '//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css' );
	wp_enqueue_script( 'datatablejs', '//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js');

	$content = '';

	require(__DIR__."/lib/PHPExcel.php");

	$file = get_field('upload_the_spreadsheet', 'option');
	$ofile = $file;
	if (!$file)
		continue;

	$file = str_replace(get_bloginfo('url'), '/home/systemla/public_html/footballadvisor.net/', $file);

	$sheet = PHPExcel_IOFactory::load($file);
	$worksheet = $sheet->getSheet(0);

	$content .= '<div class="table-holder"><table id="selectionstable" with="100%" style="width:100%;opacity:0" cellpadding="0" border="0" class="table table-striped table-bordered">';
	$lastColumn = 'O';
	$lastRow = $worksheet->getHighestDataRow();
	/*if ($lastRow > 101) {
		$lastRow = 101;
	}*/
	$lastColumn++;
	$lastRow++;
	$k = 0;
	for ($row = 1; $row != $lastRow; $row++){
		$worksheet->getStyle("J".$row)->getNumberFormat()->setFormatCode('0.00'); 
		$worksheet->getStyle("K".$row)->getNumberFormat()->setFormatCode('0.00'); 
		if ($row == 1){
    		$content .= '<thead>';
    	} else {
    		if ($k == 1) {
    			$content .= '<tbody>';
    			$k++;
    		}
    	}
		$content .= '<tr>';
		for ($column = 'A'; $column != $lastColumn; $column++) {
		    $cell = $worksheet->getCell($column.$row)->getFormattedValue();
	    	if ($row == 1){
	    		$content .= '<th>'.$cell.'</th>';
	    	} else {
	    		$content .= '<td>'.$cell.'</td>';
	    	}
		}
		$content .= '</tr>';
		if ($row == 1){
    		$content .= '</thead>';
    	}
	}
	$content .= '</tbody></table></div>';
	$content .= '<script type="text/javascript">jQuery(document).ready(function(){
    	jQuery(".table").DataTable( {
	        "scrollX": true,
	        "fnInitComplete": function (){
		        jQuery(".table").animate({opacity: 1}, 500);
		    },
		    "iDisplayLength": 100
	    } );
	});</script>

	<center><a href="'.$ofile.'" target="" class="button medium" style="color: #ffffff; background-color: #e91b23; ">Download Full Results</a></center>';

	return $content;
}
add_shortcode('showselections','showSelections');

function showPerfbymarket(){
	$file = get_field('upload_the_spreadsheet', 'option');
	$content = "[button size='medium' color='#FFF' background_color='#e91b23' font_size='' line_height='' font_style='' font_weight='' text='Performance By Market' link='".$file."' target='']";

	return do_shortcode($content);
}
add_shortcode('showperformancebymarket','showPerfbymarket');

function showPerfbyleague(){
	$file = get_field('upload_the_spreadsheet', 'option');
	$content = "[button size='medium' color='#FFF' background_color='#e91b23' font_size='' line_height='' font_style='' font_weight='' text='Performance By League' link='".$file."' target='']";
	
	return do_shortcode($content);
}
add_shortcode('showperformancebyleague','showPerfbyleague');


function showPerfbyteam(){
	$file = get_field('upload_the_spreadsheet', 'option');
	$content = "[button size='medium' color='#FFF' background_color='#e91b23' font_size='' line_height='' font_style='' font_weight='' text='Performance By Team' link='".$file."' target='']";
	
	return do_shortcode($content);
}
add_shortcode('showperformancebyteam','showPerfbyteam');

function showPerfbymth(){
	wp_enqueue_style( 'datatablecss', '//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css' );
	wp_enqueue_script( 'datatablejs', '//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js');

	$content = '';

	require_once(__DIR__."/lib/PHPExcel.php");


	$content .= '<div class="table-holder"><table id="perfbyteamtable" with="100%" style="width:100%;opacity:0" cellpadding="0" border="0" class="table stripe table-striped table-bordered">';
	$content .= '<thead>
		<td>Month</td>
		<td>Bets</td>
		<td>Wins</td>
		<td>Lost</td>
		<td>Strike Rate</td>
		<td>Profit</td>
		<td>Points P&L</td>
		<td>ROI</td>
		<td>Points Staked</td>
		<td>Ongoing P&L</td>
		<td>Bank</td>
		<td>Risk</td>
		<td>Ongoing ROI</td>
	</thead>
	<tbody>';

	if(get_field('performance_by_month')):
		while(has_sub_field('performance_by_month')):
			$content .= '
			<tr>
				<td>'.get_sub_field('month').'</td>
				<td>'.get_sub_field('bets').'</td>
				<td>'.get_sub_field('wins').'</td>
				<td>'.get_sub_field('lost').'</td>
				<td>'.get_sub_field('strike_rate').'%</td>
				<td>$'.get_sub_field('profit').'</td>
				<td>'.get_sub_field('points_pl').'</td>
				<td>'.get_sub_field('roi').'%</td>
				<td>'.get_sub_field('points_staked').'</td>
				<td>$'.get_sub_field('ongoing_pl').'</td>
				<td>$'.get_sub_field('bank').'</td>
				<td>$'.get_sub_field('risk').'</td>
				<td>'.get_sub_field('ongoing_roi').'%</td>
			</tr>
			';
		endwhile;
	endif;
	
	$content .= '</tbody></table></div>';
	$content .= '<script type="text/javascript">jQuery(document).ready(function(){
    	jQuery(".table").DataTable( {
	        "scrollX": true,
	        "order": [[ 0, "asc" ]],
	        "fnInitComplete": function (){
		        jQuery(".table").animate({opacity: 1}, 500);
		    },
		    "iDisplayLength": 100
	    } );
	});</script>
	<style type="text/css">
	#perfbyteamtable thead{opacity:0}
	</style>';

	return $content;
}
add_shortcode('showperformancebymonth','showPerfbymth');

function showPLchart(){
	$content = '';

	$content .= "[line_graph scale_steps='15' scale_step_width='10' width='1100' height='350' type='' labels='";

	$sep = ',';
	$months = '';
	$vals = '';
	if(get_field('performance_by_month')):
		while(has_sub_field('performance_by_month')):
			$months .= get_sub_field('month') . $sep;
			$vals .= get_sub_field('points_pl') . $sep;
			$i++;
		endwhile;
	endif;
	$months = substr($months, 0, -1);
	$vals = substr($vals, 0, -1);

	$content .= $months . "']#e32930,P&L," . $vals;
	$content .= "[/line_graph]";

	return do_shortcode($content);
}

add_shortcode('showplchart','showPLchart');

function showROI(){
	$ofile = get_field('upload_the_spreadsheet', 'option');
	$content = '';

	$tROI = get_field('all_time_roi');
    $tSTR = get_field('all_time_strike_rate');

	if(get_field('performance_by_month')):
		while(has_sub_field('performance_by_month')):
			if (date('M/y') == date('M/y', strtotime(get_sub_field('month')))) {
				$mROI = get_sub_field('roi');
				$mSTR = get_sub_field('strike_rate');
				$month = date('F', strtotime(get_sub_field('month')));
			}
		endwhile;
	endif;

	// model [pie_chart type='transparent' delay='' title='' percent='20' percentage_color='#FFF' active_color='#FFF' noactive_color='']


	$content .= "[four_col_col1]
	<h2 style='color:#FFF;text-align:center;'>All Time ROI</h2>
	[separator type='transparent' color='' thickness='1' up='24' down='25']
	[pie_chart type='transparent' delay='' title='' percent='".$tROI."' percentage_color='#FFF' active_color='#FFF' noactive_color='']
	[/four_col_col1]
	[four_col_col2]
	<h2 style='color:#FFF;text-align:center;'>".$month." ROI</h2>
	[separator type='transparent' color='' thickness='1' up='24' down='25']
	[pie_chart type='transparent' delay='' title='' percent='".$mROI."' percentage_color='#FFF' active_color='#FFF' noactive_color='']
	[/four_col_col2]
	[four_col_col3]
	<h2 style='color:#FFF;text-align:center;'>All Time STRIKE</h2>
	[separator type='transparent' color='' thickness='1' up='24' down='25']
	[pie_chart type='transparent' delay='' title='' percent='".$tSTR."' percentage_color='#FFF' active_color='#FFF' noactive_color='']
	[/four_col_col3]
	[four_col_col4]
	<h2 style='color:#FFF;text-align:center;'>".$month." STRIKE</h2>
	[separator type='transparent' color='' thickness='1' up='24' down='25']
	[pie_chart type='transparent' delay='' title='' percent='".$mSTR."' percentage_color='#FFF' active_color='#FFF' noactive_color='']
	[/four_col_col4]
	[separator type='transparent' color='' thickness='1' up='60' down='0']
	<center>[button size='medium' color='#FFFFFF' background_color='#e91b23' font_size='' line_height='' font_style='' font_weight='' text='Download Full Results' link='".$ofile."' target='_blank']</center>";

	return do_shortcode($content);
}

add_shortcode('showroistr','showROI');

function my_pmpro_levels_array($levels)
{
	$newlevels = array();
 
	foreach($levels as $level)
	{
		if(!pmpro_isLevelFree($level) && (!pmpro_hasMembershipLevel(array('13','14','15')) && !in_array($level->id, array('13','14','15')) ) )
		//if(!pmpro_isLevelFree($level))
			$newlevels[] = $level;
	}
 
	return $newlevels;
}
add_filter("pmpro_levels_array", "my_pmpro_levels_array");

function performance_easier()
{
	$labels = '';
	$bets = '';
	$wins = '';

	if (get_field('bets_wins', $post->ID)){
		while (has_sub_field('bets_wins', $post->ID)) {
			$labels .= get_sub_field('graph_label', $post->ID) . ',';
			$bets .= get_sub_field('bets_value', $post->ID) . ',';
			$wins .= get_sub_field('wins_value', $post->ID) . ',';
		}
	}

	$labels = substr($labels, 0, -1);
	$bets = substr($bets, 0, -1);
	$wins = substr($wins, 0, -1);

	return do_shortcode('[line_graph scale_steps="5" scale_step_width="30" width="710" height="350" type="" labels="'.$labels.'"]#e32930,BETS,'.$bets.';#c00000,WINS,'.$wins.' [/line_graph]');
}

add_shortcode('easierperformance','performance_easier');

function performance_alltime_roi()
{
	return do_shortcode("[pie_chart type='normal' delay='' title='ALL TIME ROI' percent='".get_field('all_time_roi', $post->ID)."' percentage_color='#333' active_color='#e91b23' noactive_color='#e3e3e3'][/pie_chart]");
}

add_shortcode('easieralltimeroi','performance_alltime_roi');

function easier_strike_rate()
{
	$shortcode = '[progress_bars_pattern]';

	if (get_field('strike_rate', $post->ID)){
		while (has_sub_field('strike_rate', $post->ID)) {
			$shortcode .= '[progress_bar_pattern title="'.get_sub_field('label', $post->ID).'" percent="'.get_sub_field('percentage', $post->ID).'" border_width="" border_color="#ffffff" percentage_color="#ffffff" text_color="#ffffff"][/progress_bar_pattern]';
		}
	}
	$shortcode .= '[/progress_bars_pattern]';

	return do_shortcode($shortcode);
}

add_shortcode('easierstrikerate', 'easier_strike_rate');

function easier_roi()
{
	$shortcode = '[progress_bars_pattern]';

	if (get_field('the_roi', $post->ID)){
		while (has_sub_field('the_roi', $post->ID)) {
			$shortcode .= '[progress_bar_pattern title="'.get_sub_field('label', $post->ID).'" percent="'.get_sub_field('percentage', $post->ID).'" border_width="" border_color="#ffffff" percentage_color="#ffffff" text_color="#ffffff"][/progress_bar_pattern]';
		}
	}
	$shortcode .= '[/progress_bars_pattern]';

	return do_shortcode($shortcode);
}

add_shortcode('easierroi', 'easier_roi');

function easier_profit()
{
	$shortcode = '';

	if (get_field('the_profit', $post->ID)){
		$x = 0;
		while (has_sub_field('the_profit', $post->ID)) {
			$x++;
			$shortcode .= "
			[four_col_col".$x."]
			[counter type='type2' position='center' delay='500' digit='".get_sub_field('value')."' font_size='90' font_color='#129FFB']
			<h4>".get_sub_field('label')."</h4>
			[/counter]
			[/four_col_col".$x."]
			";

			if ($x == 4) {
				$shortcode .= "[separator type='transparent' color='' thickness='1' up='24' down='25']";
				$x = 0;
			}
		}
	}

	return do_shortcode($shortcode);
}

add_shortcode('easierprofit', 'easier_profit');


//redirect to custom confirmation page - INSTAPAGE
function my_pmpro_confirmation_redirect()
{
	$instapage = 'http://www.footballadvisor.net/trialxsd';	//change this use your membership level ids and page ids
	
	global $pmpro_pages;
	
	if(is_page($pmpro_pages['confirmation']))
	{
		wp_redirect($instapage);
		exit;
	}
}
add_action("wp", "my_pmpro_confirmation_redirect");