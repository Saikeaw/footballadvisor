<?php
/**
 * class-groups-subscriptions-log.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.1.0
 */

define( 'GS_INFO', 'INFO' );
define( 'GS_WARNING', 'WARNING' );
define( 'GS_ERROR', 'ERROR' );

/**
 * Logger
 */
class Groups_Subscriptions_Log {

	private static $logfile = null;
	
	private static $log = false;
	
	public static function init() {
		self::$logfile = GROUPS_SUBSCRIPTIONS_LOG_LIB . '/log.txt';
	}

	public static function enable( $enable ) {
		if ( $enable ) {
			self::$log = true;
		} else {
			self::$log = false;
		}
	}

	/**
	 * Log message.
	 * @param string $message
	 * @param string $level (optional) GS_INFO (default), GS_WARNING or GS_ERROR
	 */
	public static function log( $message, $level = GS_INFO ) {
		if ( self::$log ) {
			$now = date( 'Y-m-d H:i:s O', time() );
			switch( $level ) {
				case GS_WARNING :
				case GS_ERROR :
					break;
				default :
					$level = GS_INFO;
			}
			$log = sprintf( "%s - %s : %s\r\n", $now, $level, $message );
			if ( !@error_log( $log, 3, self::$logfile ) ) {
				error_log(  $log );
			}
		}
	}
}
Groups_Subscriptions_Log::init();
