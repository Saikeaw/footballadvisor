<?php
/**
 * class-groups-subscriptions-debug.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.1.0
 */

require_once( GROUPS_SUBSCRIPTIONS_LOG_LIB . '/class-groups-subscriptions-log.php' );

/**
 * Debug
 */
class Groups_Subscriptions_Debug {

	public static function init() {

		add_action( 'groups_created_subscription', array( __CLASS__, 'groups_created_subscription' ), 100 );
		add_action( 'groups_updated_subscription', array( __CLASS__, 'groups_updated_subscription' ), 100 );
		add_action( 'groups_delete_subscription', array( __CLASS__, 'groups_delete_subscription' ), 100 );
		add_action( 'groups_deleted_subscription', array( __CLASS__, 'groups_deleted_subscription' ), 100 );

		add_action( 'groups_created_group_subscription', array( __CLASS__, 'groups_created_group_subscription' ), 100, 2 );
		add_action( 'groups_updated_group_subscription', array( __CLASS__, 'groups_updated_group_subscription' ), 100, 2 );
		add_action( 'groups_deleted_group_subscription', array( __CLASS__, 'groups_deleted_group_subscription' ), 100, 2 );

		add_action( 'groups_created_subscription_order_item', array( __CLASS__, 'groups_created_subscription_order_item' ), 100, 3 );
		add_action( 'groups_updated_subscription_order_item', array( __CLASS__, 'groups_updated_subscription_order_item' ), 100, 3 );
		add_action( 'groups_deleted_subscription_order_item', array( __CLASS__, 'groups_deleted_subscription_order_item' ), 100, 3 );

	}
	
	public static function groups_created_subscription( $subscription_id ) {
		$s = new Groups_Subscription( $subscription_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	public static function groups_updated_subscription( $subscription_id ) {
		$s = new Groups_Subscription( $subscription_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	public static function groups_delete_subscription( $subscription_id ) {
		$s = new Groups_Subscription( $subscription_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	public static function groups_deleted_subscription( $subscription_id ) {
		$s = new Groups_Subscription( $subscription_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	//
	
	public static function groups_created_group_subscription( $group_id, $subscription_id ) {
		$s = new Groups_Group_Subscription( $group_id, $subscription_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	public static function groups_updated_group_subscription( $group_id, $subscription_id ) {
		$s = new Groups_Group_Subscription( $group_id, $subscription_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	public static function groups_deleted_group_subscription( $group_id, $subscription_id ) {
		$s = new Groups_Group_Subscription( $group_id, $subscription_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	//
	
	public static function groups_created_subscription_order_item( $subscription_id, $order_id, $item_id ) {
		$s = new Groups_Subscription_Order_Item( $subscription_id, $order_id, $item_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	public static function groups_updated_subscription_order_item( $subscription_id, $order_id, $item_id ) {
		$s = new Groups_Subscription_Order_Item( $subscription_id, $order_id, $item_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}
	
	public static function groups_deleted_subscription_order_item( $subscription_id, $order_id, $item_id ) {
		$s = new Groups_Subscription_Order_Item( $subscription_id, $order_id, $item_id );
		Groups_Subscriptions_Log::log( sprintf( '%s : %s', __METHOD__, var_export( $s, true ) ) );
	}

}
Groups_Subscriptions_Debug::init();
