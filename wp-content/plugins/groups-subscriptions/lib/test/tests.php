<?php
/**
 * tests.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 */
// bootstrap WordPress
if ( !defined( 'ABSPATH' ) ) {
	$wp_load = 'wp-load.php';
	$max_depth = 100; // prevent death by depth
	while ( !file_exists( $wp_load ) && ( $max_depth > 0 ) ) {
		$wp_load = '../' . $wp_load;
		$max_depth--;
	}
	if ( file_exists( $wp_load ) ) {
		require_once $wp_load;
	}
}
if ( defined( 'ABSPATH' ) ) {

	/**
	 * Tests:
	 * - Groups_Subscription CR
	 * - Groups_Group_Subscription CRD
	 * - groups_deleted_group() is triggered when a group is deleted
	 *   and the subscription must have been deleted thereafter
	 */
	function groups_subscriptions_tests_groups_deleted_group() {

		// create a test group
		$subscribers_group_id = Groups_Group::create( array( 'name' => 'Subscribers' . rand( 0, 1000 ) ) );
		assert( '$subscribers_group_id !== false' );
		
		// create a test user
		$fooname = 'foo' . rand(0, 100);
		$foo_user_id = wp_create_user( $fooname, 'foo', $fooname . '@example.com' );
		assert( '$foo_user_id instanceof WP_Error === false');
		
		$processor = 'processor' . rand( 0, 100 );
		$reference = 'reference' . rand( 0, 100 );
		
		// create a subscription
		$subscription_id = Groups_Subscription::create( array(
				'user_id'       => $foo_user_id,
				'processor'     => $processor,
				'reference'     => $reference,
				'frequency'     => 1,
				'frequency_uom' => Groups_Subscription::DAY
		) );
		assert( '$subscription_id !== false' );

		// retrieve subscription
		assert( '$subscription = Groups_Subscription::read( $subscription_id )' );
		
		// relate subscription and group
		$map = array(
			'subscription_id' => $subscription_id,
			'group_id'        => $subscribers_group_id
		);
		assert( 'Groups_Group_Subscription::create( $map )' );

		// delete the test group
		assert( 'Groups_Group::delete( $subscribers_group_id )' );

		// the relation between group and subscription must have been resolved
		assert( 'Groups_Group_Subscription::read( $subscribers_group_id, $subscription_id ) === false' );

		// delete test user
		include_once( ABSPATH . '/wp-admin/includes/user.php' );
		if ( $foo_user_id && !( $foo_user_id instanceof WP_Error ) ) {
			assert( 'wp_delete_user( $foo_user_id ) === true' );
		}
	}

	/**
	 * Tests:
	 * - Groups_Subscription CRUD
	 * - Groups_Group_Subscription CR
	 * - Groups_Subscription_Order_Item CR
	 * - triggered D for Groups_Subscription_Order_Item
	 * - triggered D for Groups_Group_Subscription
	 */
	function groups_subscriptions_tests_combined() {

		// create a test group
		$subscribers_group_id = Groups_Group::create( array( 'name' => 'Subscribers' . rand( 0, 1000 ) ) );	
		assert( '$subscribers_group_id !== false' );

		// create a test user
		$fooname = 'foo' . rand(0, 100);
		$foo_user_id = wp_create_user( $fooname, 'foo', $fooname . '@example.com' );
		assert( '$foo_user_id instanceof WP_Error === false');

		$processor = 'processor' . rand( 0, 100 );
		$reference = 'reference' . rand( 0, 100 );

		// create a subscription
		$subscription_id = Groups_Subscription::create( array(
			'user_id'       => $foo_user_id,
			'processor'     => $processor,
			'reference'     => $reference,
			'frequency'     => 1,
			'frequency_uom' => Groups_Subscription::DAY
		) );
		assert( '$subscription_id !== false' );

		// retrieve subscription
		assert( '$subscription = Groups_Subscription::read( $subscription_id )' );
		
		echo '<pre>subscription : ' . var_export( $subscription, true) . '</pre>';

		
		$s = new Groups_Subscription( $subscription_id );
		$current_cycle = $s->current_cycle;
		echo '<p>Current cycle at creation: ' . $current_cycle . '</p>';
		assert( '$current_cycle == 1' );
		sleep(1);
		$current_cycle = $s->current_cycle;
		assert( '$current_cycle == 1' );
		echo '<p>Current cycle 1s after: ' . $current_cycle . '</p>';

		$next_payment_timestamp = Groups_Subscription_Handler::get_next_payment_timestamp( $subscription_id );
		$next_payment_datetime = date( 'Y-m-d H:i:s', $next_payment_timestamp );
		echo '<pre>next payment : ' . $next_payment_datetime . '</pre>';
		
		
		// fail to retrieve subscription with bad id
		assert( 'Groups_Subscription::read( -1 ) === false' );
		
		// retrieve subscription by processor and reference
		assert( 'Groups_Subscription::read_by_reference( $processor, $reference )' );

		// fail to retrieve subscription with bad reference
		assert( 'Groups_Subscription::read_by_reference( "bogus' . rand( 0,100 ) . '", "bogus' . rand( 0,100 ) . '" ) === false' );
		
		// relate subscription and group
		$map = array(
			'subscription_id' => $subscription_id,
			'group_id'        => $subscribers_group_id
		);
		assert( 'Groups_Group_Subscription::create( $map )' );
		
		// test retrieving group-subscription relation
		assert( 'Groups_Group_Subscription::read($subscribers_group_id, $subscription_id )' );
		
		// relate subscription with an order item
		$map = array(
			'subscription_id' => $subscription_id,
			'order_id'        => 1,
			'item_id'         => 1
		);
		assert( 'Groups_Subscription_Order_Item::create( $map )' );
		
		// test updating a subscription
		$map = array(
			'subscription_id' => $subscription_id,
			'status'          => Groups_Subscription::STATUS_ACTIVE
		);
		assert( 'Groups_Subscription::update( $map )' );
		
		// delete a subscription
		assert( 'Groups_Subscription::delete( $subscription_id )' );
		
		// the relation between group and subscription must have been resolved
		assert( 'Groups_Group_Subscription::read( $subscribers_group_id, $subscription_id ) === false' );
		
		// the relation between order item and subscription must have been resolved
		assert( 'Groups_Subscription_Order_Item::read( $subscription_id, 1, 1 ) === false' );
		
		// delete the test group
		assert( 'Groups_Group::delete( $subscribers_group_id )' );

		// delete test users
		include_once( ABSPATH . '/wp-admin/includes/user.php' );
		if ( $foo_user_id && !( $foo_user_id instanceof WP_Error ) ) {
			assert( 'wp_delete_user( $foo_user_id ) === true' );
		}

	}
	
	/**
	 * Tests:
	 * - multiple deletions triggered when a user is deleted
	 */
	function groups_subscriptions_tests_deleted_user() {
		
		// create a test group
		$subscribers_group_id = Groups_Group::create( array( 'name' => 'Subscribers' . rand( 0, 1000 ) ) );
		assert( '$subscribers_group_id !== false' );
		
		// create a test user
		$fooname = 'foo' . rand(0, 100);
		$foo_user_id = wp_create_user( $fooname, 'foo', $fooname . '@example.com' );
		assert( '$foo_user_id instanceof WP_Error === false');
		
		$processor = 'processor' . rand( 0, 100 );
		$reference = 'reference' . rand( 0, 100 );
		
		// create a subscription
		$subscription_id = Groups_Subscription::create( array(
				'user_id'       => $foo_user_id,
				'processor'     => $processor,
				'reference'     => $reference,
				'frequency'     => 1,
				'frequency_uom' => Groups_Subscription::DAY
		) );
		assert( '$subscription_id !== false' );

		// relate subscription and group
		$map = array(
			'subscription_id' => $subscription_id,
			'group_id'        => $subscribers_group_id
		);
		assert( 'Groups_Group_Subscription::create( $map )' );

		// relate subscription with an order item
		$map = array(
				'subscription_id' => $subscription_id,
				'order_id'        => 1,
				'item_id'         => 1
		);
		assert( 'Groups_Subscription_Order_Item::create( $map )' );
		
		// delete the test user => trigger several deletions checked below
		include_once( ABSPATH . '/wp-admin/includes/user.php' );
		if ( $foo_user_id && !( $foo_user_id instanceof WP_Error ) ) {
			assert( 'wp_delete_user( $foo_user_id ) === true' );
		}
		
		// the subscription must have been eliminated
		assert( 'Groups_Subscription::read_by_reference( $processor, $reference ) === false' );

		// the relation between group and subscription must have been resolved
		assert( 'Groups_Group_Subscription::read( $subscribers_group_id, $subscription_id ) === false' );
		
		// the relation between order item and subscription must have been resolved
		assert( 'Groups_Subscription_Order_Item::read( $subscription_id, 1, 1 ) === false' );
		
		// delete the test group
		assert( 'Groups_Group::delete( $subscribers_group_id )' );
	}
	
	/**
	 * Tests:
	 * - user belonging to a group through two memberships stays in
	 *   group as long as valid subscriptions exist
	 */
	function groups_subscriptions_tests_multiple_membership() {
	
		// create a test group
		$subscribers_group_id = Groups_Group::create( array( 'name' => 'Subscribers' . rand( 0, 1000 ) ) );
		assert( '$subscribers_group_id !== false' );
	
		// create a test user
		$fooname = 'foo' . rand(0, 100);
		$foo_user_id = wp_create_user( $fooname, 'foo', $fooname . '@example.com' );
		assert( '$foo_user_id instanceof WP_Error === false');
	
		$processor  = 'processor' . rand( 0, 100 );
		$reference  = 'reference' . rand( 0, 100 );
		$reference2 = 'reference2' . rand( 0, 100 );
	
		// create first subscription
		$subscription_id = Groups_Subscription::create( array(
				'user_id'       => $foo_user_id,
				'processor'     => $processor,
				'reference'     => $reference,
				'frequency'     => 1,
				'frequency_uom' => Groups_Subscription::DAY
		) );
		assert( '$subscription_id !== false' );
		
		// create second subscription
		$subscription_id2 = Groups_Subscription::create( array(
				'user_id'       => $foo_user_id,
				'processor'     => $processor,
				'reference'     => $reference2,
				'frequency'     => 1,
				'frequency_uom' => Groups_Subscription::DAY
		) );
		assert( '$subscription_id2 !== false' );

		// relate first subscription and group
		$map = array(
			'subscription_id' => $subscription_id,
			'group_id'        => $subscribers_group_id
		);
		assert( 'Groups_Group_Subscription::create( $map )' );
		
		// relate second subscription and group
		$map = array(
			'subscription_id' => $subscription_id2,
			'group_id'        => $subscribers_group_id
		);
		assert( 'Groups_Group_Subscription::create( $map )' );
		
		// activate subscriptions (adds user to group)
		$map = array(
			'subscription_id' => $subscription_id,
			'status'          => Groups_Subscription::STATUS_ACTIVE
		);
		assert( 'Groups_Subscription::update( $map )' );
		$map = array(
			'subscription_id' => $subscription_id2,
			'status'          => Groups_Subscription::STATUS_ACTIVE
		);
		assert( 'Groups_Subscription::update( $map )' );
		
		// test user belongs to group
		assert( 'Groups_User_Group::read( $foo_user_id, $subscribers_group_id )' );
	
		// delete the first subscription
		assert( 'Groups_Subscription::delete( $subscription_id )' );
		
		// user must still belong to group
		assert( 'Groups_User_Group::read( $foo_user_id, $subscribers_group_id )' );
		
		// delete the second subscription
		assert( 'Groups_Subscription::delete( $subscription_id2 )' );
		
		// user must not belong to group
		assert( 'Groups_User_Group::read( $foo_user_id, $subscribers_group_id ) === false' );
		
		// delete the test group
		assert( 'Groups_Group::delete( $subscribers_group_id )' );
	
		// delete the test user
		include_once( ABSPATH . '/wp-admin/includes/user.php' );
		if ( $foo_user_id && !( $foo_user_id instanceof WP_Error ) ) {
			assert( 'wp_delete_user( $foo_user_id ) === true' );
		}
	}
	
	function groups_subscriptions_tests_simulate_unpaid_subscription() {
		// create a test group
		$subscribers_group_id = Groups_Group::create( array( 'name' => 'Subscribers' . rand( 0, 1000 ) ) );
		assert( '$subscribers_group_id !== false' );
		
		// create a test user
		$fooname = 'foo' . rand(0, 100);
		$foo_user_id = wp_create_user( $fooname, 'foo', $fooname . '@example.com' );
		assert( '$foo_user_id instanceof WP_Error === false');
		
		$processor = 'processor' . rand( 0, 100 );
		$reference = 'reference' . rand( 0, 100 );
		
		// create a subscription
		$subscription_id = Groups_Subscription::create( array(
			'user_id'       => $foo_user_id,
			'processor'     => $processor,
			'reference'     => $reference,
			'frequency'     => 1,
			'frequency_uom' => Groups_Subscription::DAY,
			'from_datetime' => date( 'Y-m-d H:i:s', strtotime( "-1 week" ) ),
			'status'        => Groups_Subscription::STATUS_ACTIVE // so that it can get suspended
		) );
		assert( '$subscription_id !== false' );
		
		$s = new Groups_Subscription( $subscription_id );
		$current_cycle = $s->current_cycle;
		echo '<p>Current cycle: ' . $current_cycle . '</p>';
		assert( '$current_cycle == 8' );

		// this must suspend the subscription
		Groups_Subscription_Handler::check_subscription( $subscription_id );

		$t = Groups_Subscription_Handler::get_next_payment_timestamp( $subscription_id );
		echo '<pre>payment due: ' . date( 'Y-m-d H:i:s', $t ) . '</pre>';
		// verify suspension
		$subscription = new Groups_Subscription( $subscription_id );
		echo '<pre>'.var_export( $subscription, true).'</pre>';
		assert( '$subscription->status == Groups_Subscription::STATUS_SUSPENDED' );

		// delete the test group
		assert( 'Groups_Group::delete( $subscribers_group_id )' );

		// delete test user
		include_once( ABSPATH . '/wp-admin/includes/user.php' );
		if ( $foo_user_id && !( $foo_user_id instanceof WP_Error ) ) {
			assert( 'wp_delete_user( $foo_user_id ) === true' );
		}
	}
	
	function group_subscriptions_tests_status_updates() {
		// create a test group
		$subscribers_group_id = Groups_Group::create( array( 'name' => 'Subscribers' . rand( 0, 1000 ) ) );
		assert( '$subscribers_group_id !== false' );
		
		// create a test user
		$fooname = 'foo' . rand(0, 100);
		$foo_user_id = wp_create_user( $fooname, 'foo', $fooname . '@example.com' );
		assert( '$foo_user_id instanceof WP_Error === false');
		
		$processor = 'processor' . rand( 0, 100 );
		$reference = 'reference' . rand( 0, 100 );
		
		// create an open subscription, activate, cancel
		// paid two times
		$subscription_id = Groups_Subscription::create( array(
			'user_id'       => $foo_user_id,
			'processor'     => $processor,
			'reference'     => $reference,
			'frequency'     => 1,
			'frequency_uom' => Groups_Subscription::DAY,
			'from_datetime' => date( 'Y-m-d H:i:s', strtotime( '-50 hours' ) ),
			'last_payment'  => date( 'Y-m-d H:i:s', strtotime( '-2 hours' ) ),
			'current_count' => 2
		) );
		assert( '$subscription_id !== false' );

		// relate subscription and group
		$map = array(
			'subscription_id' => $subscription_id,
			'group_id'        => $subscribers_group_id
		);
		assert( 'Groups_Group_Subscription::create( $map )' );

		$map = array(
			'subscription_id' => $subscription_id,
			'status'          => Groups_Subscription::STATUS_ACTIVE
		);
		assert( 'Groups_Subscription::update( $map )' );
		
		// status must have been updated
		$s = new Groups_Subscription( $subscription_id );
		assert( '$s->status == Groups_Subscription::STATUS_ACTIVE' );
		
		// user must belong to group
		assert( 'Groups_User_Group::read( $foo_user_id, $subscribers_group_id )' );

		$map = array(
			'subscription_id' => $subscription_id,
			'status'          => Groups_Subscription::STATUS_CANCELLED
		);
		assert( 'Groups_Subscription::update( $map )' );
		
// 		$crons = _get_cron_array();
// 		echo '<h2>Cron</h2>';
// 		echo '<pre>';
// 		echo var_export( $crons, true );
// 		echo '</pre>';
		
		// termination for this subscription must be scheduled
		assert( 'wp_next_scheduled( "Groups_Subscription_Handler_safely_terminate", array( $subscription_id ) ) !== false' );

		// create a subscription with fixed end date, activate
		$subscription_id2 = Groups_Subscription::create( array(
			'user_id'       => $foo_user_id,
			'processor'     => $processor,
			'reference'     => $reference,
			'frequency'     => 1,
			'frequency_uom' => Groups_Subscription::DAY,
			'from_datetime' => date( 'Y-m-d H:i:s', strtotime( '-25 hours' ) ),
			//'thru_datetime' => date( 'Y-m-d H:i:s', time() )
			'total_count'   => 10
		) );
		assert( '$subscription_id !== false' );
		
		$map = array(
			'subscription_id' => $subscription_id2,
			'status'          => Groups_Subscription::STATUS_ACTIVE
		);
		assert( 'Groups_Subscription::update( $map )' );
		
		// completion for this subscription must be scheduled
		assert( 'wp_next_scheduled( "Groups_Subscription_Handler_complete_subscription", array( $subscription_id2 ) ) !== false' );

		// can't count on wp_cron() here, do it manually:
		Groups_Subscription_Handler::complete_subscription( $subscription_id2 );

		// now status must be set to completed
		$subscription = new Groups_Subscription( $subscription_id2 );
		assert( '$subscription->status == Groups_Subscription::STATUS_COMPLETED' );

		// delete the test group
		assert( 'Groups_Group::delete( $subscribers_group_id )' );
		
		// delete test user
		include_once( ABSPATH . '/wp-admin/includes/user.php' );
		if ( $foo_user_id && !( $foo_user_id instanceof WP_Error ) ) {
			assert( 'wp_delete_user( $foo_user_id ) === true' );
		}
	}

	function groups_subscriptions_tests() {
		
		assert_options( ASSERT_ACTIVE, true );
		assert_options( ASSERT_WARNING, true );
		assert_options( ASSERT_BAIL, false );
		
		echo '<p>Running group deletion test block</p>';
		groups_subscriptions_tests_groups_deleted_group();
		
		echo '<p>Running combined test block</p>';
		groups_subscriptions_tests_combined();
		
		echo '<p>Running user deletion tests</p>';
		groups_subscriptions_tests_deleted_user();
		
		echo '<p>Running multiple membership tests</p>';
		groups_subscriptions_tests_multiple_membership();
		
		echo '<p>Running simulated unpaid subscription test</p>';
		groups_subscriptions_tests_simulate_unpaid_subscription();
		
		echo '<p>Running subscription status update checks</p>';
		group_subscriptions_tests_status_updates();
	}

	$active_plugins = get_option( 'active_plugins', array() );
	if ( is_multisite() ) {
		$active_sitewide_plugins = get_site_option( 'active_sitewide_plugins', array() );
		$active_sitewide_plugins = array_keys( $active_sitewide_plugins );
		$active_plugins          = array_merge( $active_plugins, $active_sitewide_plugins );
	}
	$groups_is_active               = in_array( 'groups/groups.php', $active_plugins );
	$groups_subscriptions_is_active = in_array( 'groups-subscriptions/groups-subscriptions.php', $active_plugins );

	if ( $groups_is_active && $groups_subscriptions_is_active ) {
		if ( !current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			wp_die( __( 'Access denied.', GROUPS_PLUGIN_DOMAIN ) );
		} else {
			$run = isset( $_POST['run'] ) ? $_POST['run'] : null;
			switch( $run ) {
				case 'run' :
					if ( !isset( $_POST['groups-test-nonce'] ) || !wp_verify_nonce( $_POST['groups-test-nonce'], 'run-tests' ) ) {
						wp_die( __( 'Access denied.', GROUPS_PLUGIN_DOMAIN ) );
					}
					if ( isset( $_POST['clear-scheduled-hooks'] ) ) {
						wp_clear_scheduled_hook( 'Groups_Subscription_Handler_check_subscription' );
						wp_clear_scheduled_hook( 'Groups_Subscription_Handler_complete_subscription' );
						wp_clear_scheduled_hook( 'Groups_Subscription_Handler_safely_terminate' );
					}
					echo '<h1>Running tests for <i>Groups Subscriptions</i> plugin ...</h1>';
					groups_subscriptions_tests();
					echo '<h2>Finished.</h2>';
					echo '<form action="" method="get">';
					echo '<input type="submit" value="Start over" />';
					echo '</form>';
					break;
				default :
					$url = get_bloginfo( 'url' );
					echo '<p style="color:#f00; font-weight:bold;">';
					echo 'DO NOT CONTINUE UNLESS YOU KNOW WHAT YOU ARE DOING.';
					echo '</p>';
					echo '<ul>';
					echo '<li>This will run tests for the Groups Subscriptions plugin on this site.</li>';
					echo '<li>Unless you are a developer who knows what she or he is doing, you do not need to do this and you do not want to proceed.</li>';
					echo '<li>It may <strong>completely destroy your site</strong>.</li>';
					echo '<li>Run only at your own risk, do not blame anyone if something goes wrong.</li>';
					echo '<li>You <strong>agree to be solely responsible for any damage</strong> this may cause to the site.';
					echo '<li>Make a full backup of your site and database before you continue.</li>';
					echo '<li>If in doubt, <strong><a href="' . $url . '">do not continue</a></strong>.</li>';
					echo '</ul>';
					echo '<form action="" method="post">';
					echo '<input name="clear-scheduled-hooks" type="checkbox" /> Clear scheduled hooks<br/><br/>';
					echo '<input name="run" value="run" type="hidden" />';
					echo '<input type="submit" value="Go" />';
					wp_nonce_field( 'run-tests', 'groups-test-nonce', true, true );
					echo '</form>';

					$crons = _get_cron_array();
					echo '<h2>Cron</h2>';
					echo '<pre>';
					echo var_export( $crons, true );
					echo '</pre>';
			}
			
		}
	} else {
		if ( !$groups_is_active ) {
			echo '<p>The <i>Groups</i> plugin is not active, not running tests.</p>';
		}
		if ( !$groups_subscriptions_is_active ) {
			echo '<p>The <i>Groups Subscriptions</i> plugin is not active, not running tests.</p>';
		}
	}
	
} // ABSPATH defined