<?php
/**
 * class-groups-subscription-post-type.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 2.0.0
 */

/**
 * Subscription post type.
 * Generic subscription product base, providing a common view and control over
 * subscription attributes for extending components. 
 */
class Groups_Subscription_Post_Type {

	const DEFAULT_CURRENCY = 'USD';
	const DEFAULT_FREQUENCY_UOM = Groups_Subscription::MONTH;
	const PRICE_DECIMALS = 2;

	/**
	 * Hooks.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'post_type' ) );
		add_action( 'add_meta_boxes', array( __CLASS__, 'add_meta_boxes' ), 10, 2 );
		add_action( 'save_post', array( __CLASS__, 'save_post' ), 10, 2 );
	}

	/**
	 * Register subscription post type.
	 */
	public static function post_type() {
		register_post_type(
			'subscription',
			array(
				'labels' => array(
					'name'               => __( 'Subscriptions', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'singular_name'      => __( 'Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'all_items'          => __( 'All Subscriptions', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'add_new'            => __( 'Add New', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'add_new_item'       => __( 'Add New Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'edit'               => __( 'Edit', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'edit_item'          => __( 'Edit Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'new_item'           => __( 'New Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'not_found'          => __( 'No Subscriptions found', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'not_found_in_trash' => __( 'No Subscriptions found in trash', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'parent'             => __( 'Parent Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'search_items'       => __( 'Search Subscriptions', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'view'               => __( 'View Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
					'view_item'          => __( 'View Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN )
				),
				'capability_type'     => 'subscription',
				'description'         => __( 'Subscriptions grant group memberships on the site.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'exclude_from_search' => false,
				'has_archive'         => true,
				'hierarchical'        => false,
				'map_meta_cap'        => true,
// 				'menu_position'       => 10,
				'menu_icon'           => GROUPS_SUBSCRIPTIONS_PLUGIN_URL . '/images/groups-subscriptions.png',
				'public'              => true,
				'publicly_queryable'  => true,
				'query_var'           => true,
				'rewrite'             => true,
				'show_in_nav_menus'   => false,
				'show_ui'             => true,
				'supports'            => array(
					'author',
					'comments',
					'editor',
					'excerpt',
					'thumbnail',
					'title'
				)
			)
		);
		self::create_capabilities();
	}

	/**
	 * Returns an array of relevant capabilities for the subscription post type.
	 *
	 * @return array
	 */
	public static function get_capabilities() {
		return array(
				//'edit_post'              => 'edit_subscription',
				//'read_post'              => 'read_subscription',
				//'delete_post'            => 'delete_subscription',
				'edit_subscriptions'     => 'edit_subscriptions',
				'edit_posts'             => 'edit_posts', // access to comments
				'edit_others_posts'      => 'edit_others_subscriptions',
				'publish_posts'          => 'publish_subscriptions',
				'read_private_posts'     => 'read_private_subscriptions',
				//'read'                   => 'read',
				'delete_posts'           => 'delete_subscriptions',
				'delete_private_posts'   => 'delete_private_subscriptions',
				'delete_published_posts' => 'delete_published_subscriptions',
				'delete_others_posts'    => 'delete_others_subscriptions',
				'edit_private_posts'     => 'edit_private_subscriptions',
				'edit_published_posts'   => 'edit_published_subscriptions',
				//'create_posts'           => 'edit_subscriptions'
		);
	}

	/**
	 * Creates needed capabilities to handle subscriptions.
	 * This makes sure that these capabilities are present (will recreate them
	 * if deleted).
	 */
	public static function create_capabilities() {
		$caps = self::get_capabilities();
		foreach( $caps as $key => $capability ) {
			if ( !Groups_Capability::read_by_capability( $capability ) ) {
				Groups_Capability::create( array( 'capability' => $capability ) );
			}
		}
		self::set_administrator_capabilities();
	}

	/**
	 * Assure administrator capabilities.
	 */
	public static function set_administrator_capabilities() {
		global $wp_roles;
		if ( $admin = $wp_roles->get_role( 'administrator' ) ) {
			foreach( self::get_administrator_capabilities() as $capability ) {
				if ( !$admin->has_cap( $capability ) ) {
					$admin->add_cap( $capability );
				}
			}
		}
	}

	/**
	 * Additional administrator capabilities needed to manage subscriptions.
	 *
	 * @return array
	 */
	public static function get_administrator_capabilities() {
		return array(
			// subscriptions
			'edit_subscriptions',
			'edit_others_subscriptions',
			'publish_subscriptions',
			'read_private_subscriptions',
			'delete_subscriptions',
			'delete_private_subscriptions',
			'delete_published_subscriptions',
			'delete_others_subscriptions',
			'edit_private_subscriptions',
			'edit_published_subscriptions'
		);
	}

	/**
	 * Hook adding price and groups meta boxes to subscriptions.
	 * @param string $post_type
	 * @param object $post
	 */
	public static function add_meta_boxes( $post_type, $post ) {
		if ( $post_type == 'subscription' ) {

			add_meta_box(
				'groups-subscriptions-price',
				__( 'Price', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				array( __CLASS__, 'price_panel' ),
				'subscription',
				'normal',
				'high'
			);

			add_meta_box(
				'groups-subscriptions-groups',
				__( 'Groups', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				array( __CLASS__, 'groups_panel' ),
				'subscription',
				'normal',
				'high'
			);

		}
	}
	
	/**
	 * Renders the price meta box.
	 */
	public static function price_panel() {
		global $post, $wpdb;

		if ( isset( $post->post_type ) && ( $post->post_type == 'subscription' ) ) {

			echo '<div id="price_panel" class="price_panel" style="padding: 1em;">';

			echo '<p class="description">' . __( 'Subscription pricing is based on ...', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</p>';

			$setup_price        = get_post_meta( $post->ID, '_setup_price', true );
			$subscription_price = get_post_meta( $post->ID, '_subscription_price', true );
			$currency           = get_post_meta( $post->ID, '_currency', true );
			if ( empty( $currency ) ) {
				$currency = self::DEFAULT_CURRENCY;
			}
			$frequency          = get_post_meta( $post->ID, '_frequency', true );
			$frequency_uom      = get_post_meta( $post->ID, '_frequency_uom', true );
			if ( empty( $frequency_uom ) ) {
				$frequency_uom = self::DEFAULT_FREQUENCY_UOM;
			}
			$total_count        = get_post_meta( $post->ID, '_total_count', true );

			echo '<p>';
			echo '<label>';
			echo __( 'Setup price', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo ' ';
			echo sprintf(
				'<input type="text" name="setup_price" value="%s" />',
				 esc_attr( $setup_price )
			);
			echo '</label>';
			echo '</p>';
			echo '<p class="description">';
			echo __( 'This is the setup fee to be charged once. If no subscription price is set, the subscription will be activated against a one-time payment.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo '</p>';

			echo '<p>';
			echo '<label>';
			echo __( 'Subscription price', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo ' ';
			echo sprintf(
				'<input type="text" name="subscription_price" value="%s" />',
				esc_attr( $subscription_price )
			);
			echo '</label>';
			echo '</p>';
			echo '<p class="description">';
			echo __( 'This is the price to be charged every time as determined by the frequency.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo '</p>';

			echo '<p>';
			echo '<label>';
			echo __( 'Currency', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo ' ';
			echo '<select name="currency">';
			foreach( self::get_supported_currencies() as $cid => $cur ) {
				$selected = ( $currency == $cid ) ? ' selected="selected" ' : '';
				echo sprintf(
					'<option %s value="%s">%s%s</option>',
					$selected,
					esc_attr( $cid ),
					$cur['label'],
					!empty( $cur['symbol'] ) ? ' ('. $cur['symbol'] . ')' : ''
				);
			}
			echo '</select>';
			echo '</label>';
			echo '</p>';

			echo '<p>';
			echo '<label>';
			echo __( 'Frequency, every ...', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo ' ';
			echo sprintf(
					'<input type="text" name="frequency" value="%s" />',
					esc_attr( $frequency )
			);
			echo '</label>';
			echo '</p>';
			echo '<p class="description">';
			echo __( 'For monthly payments, this should be set to 1 and Month(s) should be selected.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo '</p>';

			echo '<p>';
			echo '<label>';
			echo __( 'Frequency unit', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo ' ';
			echo '<select name="frequency_uom">';
			$uoms = array(
				Groups_Subscription::DAY   => __( 'Day(s)', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				Groups_Subscription::WEEK  => __( 'Week(s)', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				Groups_Subscription::MONTH => __( 'Month(s)', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				Groups_Subscription::YEAR  => __( 'Year(s)', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN )
			);
			foreach ( $uoms as $uom => $label ) {
				$selected = ( $frequency_uom == $uom ) ? ' selected="selected" ' : '';
				echo sprintf( '<option %s value="%s">%s</option>', $selected, $uom, $label );
			}
			echo '</select>';
			echo '</label>';
			echo '</p>';
			echo '<p class="description">';
			echo __( 'Select the payment recurrence frequency unit here.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo '</p>';
			
			echo '<p>';
			echo '<label>';
			echo __( 'How many times', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo ' ';
			echo sprintf(
					'<input type="text" name="total_count" value="%s" />',
					esc_attr( $total_count )
			);
			echo '</label>';
			echo '</p>';
			echo '<p class="description">';
			echo __( 'The number of billing cycles for this subscription, if set, this determines the duration of the subscription. Otherwise the subscription will be maintained until cancelled.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
			echo '</p>';

			echo '</div>'; // .price-panel
				
		}
	}

	/**
	 * Renders the groups meta box.
	 */
	public static function groups_panel() {
	
		global $post, $wpdb;
		
		if ( isset( $post->post_type ) && ( $post->post_type == 'subscription' ) ) {
	
			echo '<div id="groups_panel" class="groups_panel" style="padding: 1em;">';
	
			echo '<p>' . __( 'The customer will be a member of the selected groups as long as the subscription is active.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</p>';
	
			$product_groups        = get_post_meta( $post->ID, '_groups_groups', false );
			$product_groups_remove = get_post_meta( $post->ID, '_groups_groups_remove', false );
		
			$group_table = _groups_get_tablename( "group" );
			$groups = $wpdb->get_results( "SELECT * FROM $group_table ORDER BY name" );
		
			$n = 0;
			if ( count( $groups ) > 0 ) {
				echo '<table class="widefat" style="width:50%">';
				echo '<thead>';
				echo '<tr>';
				echo '<th style="width:50%">' . __( 'Group', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</th>';
				echo '<th style="width:25%">' . __( 'Add', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</th>';
				echo '<th style="width:25%">' . __( 'Remove', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</th>';
				echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
				foreach( $groups as $group ) {
		
					if ( $group->name !== Groups_Registered::REGISTERED_GROUP_NAME ) {
						echo '<tr>';
						echo '<th>' . wp_filter_nohtml_kses( $group->name ) . '</th>';
						echo '<td>';
						echo sprintf(
							'<input type="checkbox" name="%s" %s />',
							'_groups_groups-' . esc_attr( $group->group_id ),
							in_array( $group->group_id, $product_groups ) ? ' checked="checked" ' : ''
						);
						echo '</td>';
						echo '<td>';
						echo sprintf(
							'<input type="checkbox" name="%s" %s />',
							'_groups_groups_remove-' . esc_attr( $group->group_id ),
							in_array( $group->group_id, $product_groups_remove ) ? ' checked="checked" ' : ''
						);
						echo '</td>';
						echo '</tr>';
						$n++;
					}
				}
				echo '</tbody>';
				echo '</table>';
			}
			if ( $n == 0 ) {
				echo '<p>' . __( 'There are no groups available to select. At least one group (other than <em>Registered</em>) must be created.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</p>';
			}
			echo '<p>' . __( 'Note that all users belong to the <em>Registered</em> group automatically.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</p>';
		
			echo '<p>' . __( 'Visitors must create an account or log in to purchase a subscription.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</p>';
		
			echo '<br/>';
			echo '</div>';
			
		}
	}

	/**
	 * Process data from added meta boxes:
	 * - register subscription price and term data 
	 * - register groups for a subscription
	 * @param int $post_id product ID
	 * @param object $post product
	 */
	public static function save_post( $post_id = null, $post = null) {

		global $wpdb;

		if ( ! ( ( defined( "DOING_AUTOSAVE" ) && DOING_AUTOSAVE ) ) ) {

			if ( $post->post_type == 'subscription' ) {

				//
				// subscription prices and term
				//

				$setup_price = isset( $_POST['setup_price'] ) ? self::bcadd( '0', $_POST['setup_price'] ) : null;

				if ( self::bccomp( $setup_price, 0 ) <= 0 ) {
					$setup_price = null;
				}
				update_post_meta( $post_id, '_setup_price', $setup_price );

				$subscription_price = !empty( $_POST['subscription_price'] ) ? self::bcadd( '0', $_POST['subscription_price'] ) : null;
				if ( self::bccomp( $subscription_price, 0 ) <= 0 ) {
					$subscription_price = null;
				}
				update_post_meta( $post_id, '_subscription_price', $subscription_price );

				$currency = !empty( $_POST['currency'] ) ? $_POST['currency'] : self::DEFAULT_CURRENCY;
				if ( !key_exists( $currency, self::get_supported_currencies() ) ) {
					$currency = self::DEFAULT_CURRENCY;
				}
				update_post_meta( $post_id, '_currency', $currency );

				$frequency = !empty( $_POST['frequency'] ) ? intval( $_POST['frequency'] ) : null;
				if ( $frequency < 1 ) {
					$frequency = 1;
				}
				update_post_meta( $post_id, '_frequency', $frequency );

				$frequency_uom = !empty( $_POST['frequency_uom'] ) ? $_POST['frequency_uom'] : null;
				switch( $frequency_uom ) {
					case Groups_Subscription::DAY :
					case Groups_Subscription::WEEK :
					case Groups_Subscription::MONTH :
					case Groups_Subscription::YEAR :
						break;
					default :
						$frequency_uom = self::DEFAULT_FREQUENCY_UOM;
				}
				update_post_meta( $post_id, '_frequency_uom', $frequency_uom );

				$total_count = !empty( $_POST['total_count'] ) ? intval( $_POST['total_count'] ) : null;
				if ( $total_count < 1 ) {
					$total_count = null;
				}
				update_post_meta( $post_id, '_total_count', $total_count );

				//
				// add/remove groups
				//

				// refresh groups, clear all, then assign checked
				delete_post_meta( $post_id, '_groups_groups' );
				delete_post_meta( $post_id, '_groups_groups_remove' );
				
				// iterate over groups, could also try to find these in $_POST
				// but would normally be more costly
				$group_table = _groups_get_tablename( 'group' );
				$groups = $wpdb->get_results( "SELECT group_id FROM $group_table" );
				if ( count( $groups ) > 0 ) {
					foreach( $groups as $group ) {
						if ( !empty( $_POST['_groups_groups-'.$group->group_id] ) ) {
							add_post_meta( $post_id, '_groups_groups', $group->group_id );
						} else {
							if ( !empty( $_POST['_groups_groups_remove-'.$group->group_id] ) ) {
								add_post_meta( $post_id, '_groups_groups_remove', $group->group_id );
							}
						}
					}
				}

			}
		}

	}

	/**
	 * Returns the supported currencies with labels.
	 * Provides the groups_subscriptions_supported_currencies filter to modifiy the list.
	 * @return array
	 */
	public static function get_supported_currencies() {
		return apply_filters(
			'groups_subscriptions_supported_currencies', 
			array(
				'AUD' => array( 'label' => __( 'Australian Dollar', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ),
				'BRL' => array( 'label' => __( 'Brazilian Real', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'CAD' => array( 'label' => __( 'Canadian Dollar', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ),
				'CZK' => array( 'label' => __( 'Czech Koruna', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ), 
				'DKK' => array( 'label' => __( 'Danish Krone', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'EUR' => array( 'label' => __( 'Euro', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#8364;' ),
				'HKD' => array( 'label' => __( 'Hong Kong Dollar', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ), 
				'HUF' => array( 'label' => __( 'Hungarian Forint', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'ILS' => array( 'label' => __( 'Israeli New Sheqel', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'JPY' => array( 'label' => __( 'Japanese Yen', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#165;' ),
				'MYR' => array( 'label' => __( 'Malaysian Ringgit', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'MXN' => array( 'label' => __( 'Mexican Peso', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'NOK' => array( 'label' => __( 'Norwegian Krone', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'NZD' => array( 'label' => __( 'New Zealand Dollar', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ),
				'PHP' => array( 'label' => __( 'Philippine Peso', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'PLN' => array( 'label' => __( 'Polish Zloty', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'GBP' => array( 'label' => __( 'Pound Sterling', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#163;' ),
				'SGD' => array( 'label' => __( 'Singapore Dollar', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'SEK' => array( 'label' => __( 'Swedish Krona', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'CHF' => array( 'label' => __( 'Swiss Franc', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'TWD' => array( 'label' => __( 'Taiwan New Dollar', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'THB' => array( 'label' => __( 'Thai Baht', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'TRY' => array( 'label' => __( 'Turkish Lira', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '' ),
				'USD' => array( 'label' => __( 'U.S. Dollar', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), 'symbol' => '&#36;' )
			)
		);
	}
	
	/**
	 * Renders subscription price information.
	 * @param int $post_id
	 * @return string
	 */
	public static function get_price_html( $post_id ) {

		$html = '';
		
		if ( $subscription = get_post( $post_id ) ) {
			if ( $subscription->post_type == 'subscription' ) {

				$setup_price        = get_post_meta( $post_id, '_setup_price', true );
				$subscription_price = get_post_meta( $post_id, '_subscription_price', true );
				$currency           = get_post_meta( $post_id, '_currency', true );
				$frequency          = get_post_meta( $post_id, '_frequency', true );
				$frequency_uom      = get_post_meta( $post_id, '_frequency_uom', true );
				$total_count        = get_post_meta( $post_id, '_total_count', true );

				$symbol = $currency;
				$currencies = self::get_supported_currencies();
				if ( key_exists( $currency, $currencies ) ) {
					$c = $currencies[$currency];
					$label = !empty( $c['label'] ) ? $c['label'] : $currency;
					if ( !empty( $c['symbol'] ) ) {
						$symbol = sprintf( '<span class="subscription currency symbol" title="%s">%s</span>', $label, $c['symbol'] );
					}
				}

				if ( $subscription_price ) {

					if ( !$total_count ) {
						switch( $frequency_uom ) {
							case Groups_Subscription::DAY :
								$t = _n( '%s %s per day', '%s %s every %d days', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
								break;
							case Groups_Subscription::WEEK :
								$t = _n( '%s %s per week', '%s %s every %d weeks', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
								break;
							case Groups_Subscription::YEAR :
								$t = _n( '%s %s per year', '%s %s every %d years', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
								break;
							default : // including Groups_Subscription::MONTH
								$t = _n( '%s %s per month', '%s %s every %d months', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
						}
						$html .= '<span class="recurring-fee">' . sprintf( $t, $subscription_price, $symbol, $frequency ) . '</span>';
					} else {
						switch( $frequency_uom ) {
							case Groups_Subscription::DAY :
								$t = _n( '%1$s %2$s per day for %4$d days', '%s %s every %d days for %d days', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
								break;
							case Groups_Subscription::WEEK :
								$t = _n( '%1$s %2$s per week for %4$d weeks', '%s %s every %d weeks for %d weeks', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
								break;
							case Groups_Subscription::YEAR :
								$t = _n( '%1$s %2$s per year for %4$d years', '%s %s every %d years for %d years', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
								break;
							default : // including Groups_Subscription::MONTH
								$t = _n( '%1$s %2$s per month for %4$d months', '%s %s every %d months for %d months', $frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
						}
						$html .= '<span class="recurring-fee">' . sprintf( $t, $subscription_price, $symbol, $frequency, $frequency * $total_count ) . '</span>';
					}
					if ( $setup_price > 0 ) {
						$html .= '<span class="setup-fee">' . sprintf( __( ' (Setup fee: %s %s)', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $setup_price, $symbol ) . '</span>';
					}

				} else if ( $setup_price ) {
					$html .= '<span class="one-time-fee">' . sprintf( __( '%s %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $setup_price, $symbol ) . '</span>';
				} else {
					// @todo render something when no price is given?
				}

			}
		}
		return $html;
	}

	/**
	 * bcadd or fake it using float
	 * @param string $left_operand
	 * @param string $right_operand
	 * @return string
	 */
	private static function bcadd( $left_operand, $right_operand ) {
		$result = null;
		if ( function_exists( 'bcadd' ) ) {
			$result = bcadd( $left_operand, $right_operand, self::PRICE_DECIMALS ); // fixed to two decimals
		} else {
			$result = (string) ( floatval( $left_operand ) + floatval( $right_operand ) );
		}
		return $result;
	}

	/**
	 * bccomp or fake it using float
	 * @param string $left_operand
	 * @param string $right_operand
	 * @return int
	 */
	private static function bccomp( $left_operand, $right_operand ) {
		$result = null;
		if ( function_exists( 'bccomp' ) ) {
			$result = bccomp( $left_operand, $right_operand, self::PRICE_DECIMALS );
		} else {
			$tmp = floatval( $left_operand ) - floatval( $right_operand );
			if ( $tmp == 0 ) {
				$result = 0;
			} else if ( $tmp > 0 ) {
				$result = 1;
			} else {
				$result = -1;
			}
		}
		return $result;
	}

}
Groups_Subscription_Post_Type::init();
