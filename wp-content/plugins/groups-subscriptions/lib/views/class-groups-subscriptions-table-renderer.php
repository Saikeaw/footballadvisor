<?php
/**
 * class-groups-subscriptions-table-renderer.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 2.2.0
 */

/**
 * Subscription table renderer.
 */
class Groups_Subscriptions_Table_Renderer {

	/**
	 * Renders a table with subscription information.
	 * @param array $options
	 * @param int $n will be set to the number of subscriptions found
	 */
	public static function render( $options, &$n ) {

		global $wpdb;

		$output = '';

		if ( isset( $options['user_id'] ) ) {
			$user = new WP_User( $options['user_id'] );
		} else {
			return $output;
		}

		$statuses = array( Groups_Subscription::STATUS_ACTIVE );
		$show_all = false;
		if ( isset( $options['status'] ) ) {
			$status = $options['status'];
			if ( is_string( $status ) ) {
				if ( trim( $status ) === '*' ) {
					$show_all = true;
				} else {
					$statuses = array();
					$_statuses = explode( ',', $status );
					foreach( $_statuses as $status ) {
						$status = strtolower( trim( $status ) );
						switch( $status ) {
							case Groups_Subscription::STATUS_ACTIVE :
							case Groups_Subscription::STATUS_CANCELLED :
							case Groups_Subscription::STATUS_COMPLETED :
							case Groups_Subscription::STATUS_EXPIRED :
							case Groups_Subscription::STATUS_INACTIVE :
							case Groups_Subscription::STATUS_SUSPENDED :
								$placeholders[] = '%s';
								$statuses[] = $status;
								break;
						}
					}
				}
			}
		}

		$subscription_table = _groups_get_tablename( 'subscription' );

		if ( $show_all ) {
			$query = $wpdb->prepare(
					"SELECT * FROM $subscription_table
					LEFT JOIN $wpdb->users ON $subscription_table.user_id = $wpdb->users.ID
					WHERE $subscription_table.user_id = %d
					ORDER BY subscription_id DESC",
					Groups_Utility::id( $user->ID )
			);
		} else {
			$params = array_merge( array( Groups_Utility::id( $user->ID ) ), $statuses );
			$query = $wpdb->prepare(
				"SELECT * FROM $subscription_table
				LEFT JOIN $wpdb->users ON $subscription_table.user_id = $wpdb->users.ID
				WHERE $subscription_table.user_id = %d AND $subscription_table.status IN (" . implode( ',', $placeholders ) . ")
				ORDER BY subscription_id DESC",
				$params
			);
		}

		$results = $wpdb->get_results( $query, OBJECT );

		$n = count( $results );

		if ( $n > 0 ) {

			$column_display_names = array(
				'subscription_id'  => __( 'Id', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'processor'        => __( 'Processor', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'reference'        => __( 'Reference', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'status'           => __( 'Status', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'dates'            => __( 'Dates', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'cycle'            => __( 'Cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'description'      => __( 'Description', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'groups'           => __( 'Groups', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'order'            => __( 'Order', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			);
			if ( isset( $options['columns'] ) && $options['columns'] !== null ) {
				$new_columns = array();
				foreach( $column_display_names as $key => $value ) {
					if ( strpos( $options['columns'], $key ) !== false ) {
						$new_columns[$key] = $value;
					}
				}
				$column_display_names = $new_columns;
			}

			if ( count( $column_display_names )  > 0 ) {
				$output .= '<table class="subscriptions">';
				$output .= '<thead>';
				$output .= '<tr>';

				foreach ( $column_display_names as $key => $column_display_name ) {
					$output .= "<th scope='col' class='$key'>$column_display_name</th>";
				}

				$output .= '</tr>';
				$output .= '</thead>';
				$output .= '<tbody>';

				for ( $i = 0; $i < count( $results ); $i++ ) {
					$result = $results[$i];
					$subscription = new Groups_Subscription( $result->subscription_id );
					$output .= '<tr class="' . ( $i % 2 == 0 ? 'even' : 'odd' ) . '">';

					if ( isset( $column_display_names['subscription_id'] ) ) {
						$output .= "<td class='subscription-id'>" . $result->subscription_id . "</td>";
					}
					if ( isset( $column_display_names['processor'] ) ) {
						$output .= "<td class='processor'>" . stripslashes( wp_filter_nohtml_kses( $result->processor ) ) . "</td>";
					}
					if ( isset( $column_display_names['reference'] ) ) {
						$output .= "<td class='reference'>" . stripslashes( wp_filter_nohtml_kses( $result->reference ) ) . "</td>";
					}
					if ( isset( $column_display_names['status'] ) ) {
						$output .= "<td class='status'>" . stripslashes( wp_filter_nohtml_kses( $result->status ) ) . "</td>";
					}

					if ( isset( $column_display_names['dates'] ) ) {
						$output .= "<td class='dates'>";
						$output .= '<ul>';
						$output .= '<li>' . sprintf( __( 'From %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->from_datetime ) . '</li>';
						if ( intval( $result->frequency ) < Groups_Subscription::WAY_AHEAD ) { // only if it's not a one-time payment
							if ( $thru_timestamp = Groups_Subscription_Handler::get_end_of_subscription_timestamp( $result->subscription_id ) ) {
								$output .= '<li>' . sprintf( __( 'Until %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), date( 'Y-m-d H:i:s', $thru_timestamp ) ) . '</li>';
							}
						}
						if ( $result->last_payment ) {
							$output .= '<li>' . sprintf( __( 'Last payment %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->last_payment ) . '</li>';
						}
						$output .= '</ul>';
						$output .= "</td>";
					}
					
					if ( isset( $column_display_names['cycle'] ) ) {
						$output .= "<td class='cycle'>";
						$cycle = '';
						if ( $result->status == Groups_Subscription::STATUS_ACTIVE ) {
							$current_cycle = $subscription->current_cycle;
							$current_cycle_tooltip_label = __( 'Current cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
						} else if ( $result->status == Groups_Subscription::STATUS_COMPLETED ) {
							$current_cycle = min( array( $result->current_count, $subscription->current_cycle ) );
							$current_cycle_tooltip_label = __( 'Last cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
						} else {
							$current_cycle = min( array( $result->current_count + 1, $subscription->current_cycle ) );
							$current_cycle_tooltip_label = __( 'Last cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
						}
						if ( !$result->total_count ) {
							switch( $result->frequency_uom ) {
								case Groups_Subscription::DAY :
									$t = _n( 'every day', 'every %d days', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
									break;
								case Groups_Subscription::WEEK :
									$t = _n( 'every week', 'every %d weeks', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
									break;
								case Groups_Subscription::YEAR :
									$t = _n( 'every year', 'every %d years', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
									break;
								default : // including Groups_Subscription::MONTH
									$t = _n( 'every month', 'every %d months', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							}
							// far away in the future ... ?
							if ( intval( $result->frequency ) >= Groups_Subscription::WAY_AHEAD ) {
								$t = '&infin;';
							}
							$cycle = sprintf( $t, $result->frequency );
							$cycle .= '<br/>';
							$cycle .= sprintf( '<span class="tooltip" title="%s">#%d</span> / <span class="tooltip" title="%s">#%d</span>', __( 'Payments received', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->current_count, $current_cycle_tooltip_label, $current_cycle );
						} else {
							switch( $result->frequency_uom ) {
								case Groups_Subscription::DAY :
									$t = _n( 'every day for %2$d days', 'every %1$d days for %2$d days', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
									break;
								case Groups_Subscription::WEEK :
									$t = _n( 'every week for %2$d weeks', 'every %1$d weeks for %2$d weeks', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
									break;
								case Groups_Subscription::YEAR :
									$t = _n( 'every year for %2$d years', 'every %1$d years for %2$d years', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
									break;
								default : // including Groups_Subscription::MONTH
									$t = _n( 'every month for %2$d months', 'every %1$d months for %2$d months', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							}
							$cycle = sprintf( $t, $result->frequency, $result->frequency * $result->total_count );
							$cycle .= '<br/>';
							$cycle .= sprintf( '<span class="tooltip" title="%s">#%d</span> / <span class="tooltip" title="%s">#%d</span>', __( 'Payments received', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->current_count, $current_cycle_tooltip_label, $current_cycle );
						}
						$output .= apply_filters( 'groups_subscriptions_render_subscription_cycle', $cycle, $result->subscription_id );
						$output .= "</td>";
					}

					if ( isset( $column_display_names['description'] ) ) {
						$output .= "<td class='description'>" . stripslashes( wp_filter_nohtml_kses( $result->description ) ) . "</td>";
					}

					if ( isset( $column_display_names['groups'] ) ) {
						$output .= "<td class='groups'>";
						$output .= '<ul>';
						foreach( $subscription->groups as $group ) {
							$output .= '<li>' . wp_filter_nohtml_kses( $group->name ) . '</li>';
						}
						$output .= '</ul>';
						$output .= "</td>";
					}

					if ( isset( $column_display_names['order'] ) ) {
						if ( $order_item = $subscription->order_item ) {
							$output .= "<td class='order'>";
							$output .= apply_filters( 'groups_subscriptions_render_subscription_order_item', sprintf( __( 'Order %d Item %d', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $order_item->order_id, $order_item->item_id ), $result->subscription_id );
							$output .= "</td>";
						} else {
							$output .= "<td class='order'>&nbsp;</td>";
						}
					}
					$output .= '</tr>';
				}
				$output .= '</tbody>';
				$output .= '</table>';
			}
		}

		return $output;
	}
}
