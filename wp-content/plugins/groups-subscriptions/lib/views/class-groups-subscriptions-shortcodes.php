<?php
/**
 * class-groups-subscriptions-shortcodes.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 2.2.0
 */

/**
 * Subscription shortcode handlers
 */
class Groups_Subscriptions_Shortcodes {

	/**
	 * Adds subscription shortcodes.
	 */
	public static function init() {
		//add_shortcode( 'groups_subscription_info', array( __CLASS__, 'groups_subscription_info' ) );
		add_shortcode( 'groups_subscriptions_table', array( __CLASS__, 'groups_subscriptions_table' ) );
	}

	/**
	 * Renders a table showing a user's subscriptions.
	 * 
	 * Attributes:
	 * 
	 * - "status"   : subscription status, defaults to active. Can be a comma-separated list or * for all subscriptions.
	 * - "user_id"  : defaults to the current user's id, accepts user id, email or login
	 * - "show_count" : If info about how many subscriptions are listed should be shown. Defaults to "true".
	 * - "count_0" : Message for 0 subscriptions listed.
	 * - "count_1" : Message for 1 subscription listed.
	 * - "count_2" : Message for n subscriptions listed, use %d as placeholder in a customized message.
	 * - "show_table" : If the table should be shown. Defaults to "true".
	 * - "columns" : Which columns should be included. Defaults to all columns. Specify one or more of subscription_id, processor, reference, status, dates, cycle, description, groups, order.
	 * @param array $atts attributes
	 * @param string $content content to render
	 * @return rendered information
	 */
	public static function groups_subscriptions_table( $atts, $content = null ) {
		global $wpdb;
		$output = "";

		// for translation
		__( 'No active subscriptions.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
		_n( 'One active subscription.', '%d active subscriptions.', 0, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );

		$options = shortcode_atts(
			array(
				'status' => Groups_Subscription::STATUS_ACTIVE,
				'user_id' => get_current_user_id(),
				'show_count' => true,
				'count_0' => 'No subscriptions.',
				'count_1' => 'One subscription.',
				'count_n' => '%d subscriptions.',
				'show_table' => true,
				'columns' => null
			),
			$atts
		);

		extract( $options );

		if (
			( $user = get_user_by( 'id', $user_id ) ) ||
			( $user = get_user_by( 'login', $user_id ) ) ||
			( $user = get_user_by( 'email', $user_id ) )
		) {
			$user_id = $user->ID;
			if ( ( $user_id == get_current_user_id() )  || current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
				require_once( GROUPS_SUBSCRIPTIONS_VIEWS_LIB . '/class-groups-subscriptions-table-renderer.php' );
				$table = Groups_Subscriptions_Table_Renderer::render( $options, $n );
				if ( $show_count ) {
					$output .= '<div class="subscriptions-count">';
					if ( $n > 0 ) {
						$output .= sprintf( _n( $count_1, $count_n, $n, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $n );
					} else {
						$output .= $count_0;
					}
					$output .= '</div>';
				}
				if ( $show_table ) {
					$output .= '<div class="subscriptions-table">';
					$output .= $table;
					$output .= '</div>';
				}
			}
		}
		return $output;
	}

}
Groups_Subscriptions_Shortcodes::init();
