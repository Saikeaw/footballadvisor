<?php
/**
 * class-groups-group-subscription.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 */

/**
 * Group Subscription OPM
 */
class Groups_Group_Subscription {
	
	var $group_subscription = null;
	
	/**
	 * Create by group and subscription id.
	 * Must have been persisted.
	 * @param int $group_id
	 * @param int $subscription_id
	 */
	public function __construct( $group_id, $subscription_id ) {
		$this->group_subscription = self::read( $group_id, $subscription_id );
	}
	
	/**
	 * Adds action hooks.
	 */
	public static function init() {
		add_action( 'groups_deleted_group', array( __CLASS__, 'groups_deleted_group' ) );
		add_action( 'groups_deleted_subscription', array( __CLASS__, 'groups_deleted_subscription' ) );
	}

	/**
	 * Delete all group-subscription entries for a given group.
	 * Hooked on groups_deleted_group.
	 * @param int $group_id
	 */
	public static function groups_deleted_group( $group_id ) {
		global $wpdb;
		if ( !empty( $group_id ) ) {
			$group_subscription_table = _groups_get_tablename( 'group_subscription' );
			if ( $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT subscription_id FROM $group_subscription_table WHERE group_id = %d", Groups_Utility::id( $group_id ) ) ) ) {
				// do it individually, action must be triggered for each through delete()
				foreach( $subscriptions as $subscription ) {
					self::delete( $group_id, $subscription->subscription_id );
				}
			}
		}
	}

	/**
	 * Delete all group-subscription entries for a given subscription.
	 * Hooked on groups_deleted_subscription.
	 * @param int $subscription_id
	 */
	public static function groups_deleted_subscription( $subscription_id ) {
		global $wpdb;
		if ( !empty( $subscription_id ) ) {
			$group_subscription_table = _groups_get_tablename( 'group_subscription' );
			if ( $groups = $wpdb->get_results( $wpdb->prepare( "SELECT group_id FROM $group_subscription_table WHERE subscription_id = %d", Groups_Utility::id( $subscription_id ) ) ) ) {
				// do it individually, action must be triggered for each through delete()
				foreach( $groups as $group ) {
					self::delete( $group->group_id, $subscription_id );
				}
			}
		}
	}
	
	/**
	 * Persist a group-subscription relation.
	 * 
	 * @param array $map attributes - must provide group_id and subscription_id
	 * @return true on success, otherwise false
	 */
	public static function create( $map ) {
		
		global $wpdb;
		extract( $map );
		$result = false;

		// avoid nonsense requests
		if ( !empty( $group_id ) && !empty( $subscription_id) ) {
			// make sure group and subscription exist
			if ( Groups_Group::read( $group_id ) && Groups_Subscription::read( $subscription_id ) ) {
				$group_subscription_table = _groups_get_tablename( 'group_subscription' );
				// don't try to create duplicate entries (would raise an error for duplicate PK)
				if ( 0 === intval( $wpdb->get_var( $wpdb->prepare(
					"SELECT COUNT(*) FROM $group_subscription_table WHERE group_id = %d AND subscription_id = %d",
					Groups_Utility::id( $group_id ),
					Groups_Utility::id( $subscription_id )
				) ) ) ) {
					$data = array(
						'group_id' => Groups_Utility::id( $group_id ),
						'subscription_id' => Groups_Utility::id( $subscription_id )
					);
					$formats = array( '%d', '%d' );
					if ( $wpdb->insert( $group_subscription_table, $data, $formats ) ) {
						$result = true;
						do_action( "groups_created_group_subscription", $group_id, $subscription_id );
					}
				}
			}
		}
		return $result;
	}
	
	/**
	 * Retrieve a group-subscription relation.
	 * 
	 * @param int $group_id group's id
	 * @param int $subscription_id subscription's id
	 * @return object upon success, otherwise false
	 */
	public static function read( $group_id, $subscription_id ) {
		global $wpdb;
		$result = false;
		
		$group_subscription_table = _groups_get_tablename( 'group_subscription' );
		$group_subscription = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $group_subscription_table WHERE group_id = %d AND subscription_id = %d",
			Groups_Utility::id( $group_id ),
			Groups_Utility::id( $subscription_id )
		) );
		if ( $group_subscription !== null ) {
			$result = $group_subscription;
		}
		return $result;
	}
	
	/**
	 * Update group-subscription relation.
	 * 
	 * This changes nothing so as of now it's pointless to even call this.
	 * 
	 * @param array $map
	 * @return true if successful, false otherwise
	 */
	public static function update( $map ) {
		$result = false;
		extract( $map );
		if ( !empty( $group_id ) && !empty( $subscription_id) ) {
			// make sure group and subscription exist
			if ( Groups_Group::read( $group_id ) && Groups_Subscription::read( $subscription_id ) ) {
				$result = true;
				do_action( "groups_updated_group_subscription", $group_id, $subscription_id );
			}
		}
		return $result;
	}
	
	/**
	 * Remove group-subscription relation.
	 * 
	 * @param int $group_id
	 * @param int $subscription_id
	 * @return true if successful, false otherwise
	 */
	public static function delete( $group_id, $subscription_id ) {

		global $wpdb;
		$result = false;
		
		// avoid nonsense requests
		if ( !empty( $group_id ) && !empty( $subscription_id) ) {
			// we can omit checking if the group and subscription exist, to
			// allow resolving the relationship after they have been deleted
			$group_subscription_table = _groups_get_tablename( 'group_subscription' );
			// get rid of it
			$rows = $wpdb->query( $wpdb->prepare(
				"DELETE FROM $group_subscription_table WHERE group_id = %d AND subscription_id = %d",
				Groups_Utility::id( $group_id ),
				Groups_Utility::id( $subscription_id )
			) );
			// must have affected a row, otherwise no great success
			$result = ( $rows !== false ) && ( $rows > 0 );
			if ( $result ) {
				do_action( "groups_deleted_group_subscription", $group_id, $subscription_id );
			}
		}
		return $result;
	}
}
Groups_Group_Subscription::init();
