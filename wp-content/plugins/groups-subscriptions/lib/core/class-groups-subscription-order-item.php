<?php
/**
 * class-groups-subscription-order-item.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 */

/**
 * Group Subscription Order Item OPM
 */
class Groups_Subscription_Order_Item {
	
	/**
	 * @var Object Persisted item.
	 */
	var $subscription_order_item = null;
	
	/**
	 * Create by PK.
	 * Must have been persisted.
	 * @param int $subscription_id
	 * @param int $order_id
	 * @param int $item_id
	 */
	public function __construct( $subscription_id, $order_id, $item_id ) {
		$this->subscription_order_item = self::read( $subscription_id, $order_id, $item_id );
	}
	
	/**
	 * Retrieve a property by name.
	 *
	 * Possible properties:
	 * - subscription_id
	 * - order_id
	 * - item_id
	 * 
	 * @param string $name property's name
	 * @return property value, will return null if property does not exist
	 */
	public function __get( $name ) {
		$result = null;
		if ( $this->subscription_order_item !== null ) {
			switch( $name ) {
				case "subscription_id" :
				case "order_id" :
				case "item_id" :
					$result = $this->subscription_order_item->$name;
					break;
			}
		}
		return $result;
	}
	
	/**
	 * Adds action hooks.
	 */
	public static function init() {
		add_action( 'groups_deleted_subscription', array( __CLASS__, 'groups_deleted_subscription' ) );
		// Note: extensions should add their own hooks to act upon order deleted etc.
	}

	/**
	 * Delete all entries for a given subscription.
	 * Hooked on groups_deleted_subscription.
	 * @param int $subscription_id
	 */
	public static function groups_deleted_subscription( $subscription_id ) {
		global $wpdb;
		if ( !empty( $subscription_id ) ) {
			$subscription_order_item_table = _groups_get_tablename( 'subscription_order_item' );
			if ( $pks = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $subscription_order_item_table WHERE subscription_id = %d", Groups_Utility::id( $subscription_id ) ) ) ) {
				// do it individually, action must be triggered for each through delete()
				foreach( $pks as $pk ) {
					self::delete( $pk->subscription_id, $pk->order_id, $pk->item_id );
				}
			}
		}
	}
	
	/**
	 * Persist a subscription-order-item relation.
	 * 
	 * @param array $map attributes - must provide subscription_id, order_id and item_id
	 * @return true on success, otherwise false
	 */
	public static function create( $map ) {
		
		global $wpdb;
		extract( $map );
		$result = false;

		// avoid nonsense requests
		if ( !empty( $subscription_id ) && !empty( $order_id) && !empty( $item_id) ) {
			// make sure the subscription exists (don't check others as we can't control these here)
			if ( Groups_Subscription::read( $subscription_id ) ) {
				$subscription_order_item_table = _groups_get_tablename( 'subscription_order_item' );
				// don't try to create duplicate entries (would raise an error for duplicate PK)
				if ( 0 === intval( $wpdb->get_var( $wpdb->prepare(
					"SELECT COUNT(*) FROM $subscription_order_item_table WHERE subscription_id = %d AND order_id = %d AND item_id = %d",
					Groups_Utility::id( $subscription_id ),
					Groups_Utility::id( $order_id ),
					Groups_Utility::id( $item_id )
				) ) ) ) {
					$data = array(
						'subscription_id' => Groups_Utility::id( $subscription_id ),
						'order_id'        => Groups_Utility::id( $order_id ),
						'item_id'         => Groups_Utility::id( $item_id )
					);
					$formats = array( '%d', '%d', '%d' );
					if ( $wpdb->insert( $subscription_order_item_table, $data, $formats ) ) {
						$result = true;
						do_action( "groups_created_subscription_order_item", $subscription_id, $order_id, $item_id );
					}
				}
			}
		}
		return $result;
	}
	
	/**
	 * Retrieve a group-subscription relation.
	 * 
	 * @param int $subscription_id subscription's id, can be null to retrieve just by (order_id, item_id)
	 * @return object upon success, otherwise false
	 */
	public static function read( $subscription_id, $order_id, $item_id ) {
		global $wpdb;
		$result = false;
		$subscription_order_item_table = _groups_get_tablename( 'subscription_order_item' );
		if ( $subscription_id === null ) {
			$entry = $wpdb->get_row( $wpdb->prepare(
				"SELECT * FROM $subscription_order_item_table WHERE order_id = %d AND item_id = %d",
				Groups_Utility::id( $order_id ),
				Groups_Utility::id( $item_id )
			) );
		} else {
			$entry = $wpdb->get_row( $wpdb->prepare(
				"SELECT * FROM $subscription_order_item_table WHERE subscription_id = %d AND order_id = %d AND item_id = %d",
				Groups_Utility::id( $subscription_id ),
				Groups_Utility::id( $order_id ),
				Groups_Utility::id( $item_id )
			) );
		}
		
		if ( $entry !== null ) {
			$result = $entry;
		}
		return $result;
	}
	
	/**
	 * Update subscription-order-item relation.
	 * 
	 * Results in no changes as of now.
	 * 
	 * @param array $map
	 * @return true if successful, false otherwise
	 */
	public static function update( $map ) {
		$result = false;
		extract( $map );
		if ( !empty( $subscription_id ) && !empty( $order_id) && !empty( $item_id) ) {
			// make sure entry exists
			if ( Groups_Subscription_Order_Item::read( $subscription_id, $order_id, $item_id ) ) {
				$result = true;
				do_action( "groups_updated_subscription_order_item", $subscription_id, $order_id, $item_id );
			}
		}
		return $result;
	}
	
	/**
	 * Remove subscription-order-item relation.
	 * 
	 * @param int $subscription_id
	 * @param int $order_id
	 * @param int $item_id
	 * @return true if successful, false otherwise
	 */
	public static function delete( $subscription_id, $order_id, $item_id ) {

		global $wpdb;
		$result = false;
		
		// avoid nonsense requests
		if ( !empty( $subscription_id ) && !empty( $order_id) && !empty( $item_id) ) {
			// we can omit checking if the group and subscription exist, to
			// allow resolving the relationship after they have been deleted
			$subscription_order_item_table = _groups_get_tablename( 'subscription_order_item' );
			// get rid of it
			$rows = $wpdb->query( $wpdb->prepare(
				"DELETE FROM $subscription_order_item_table WHERE subscription_id = %d AND order_id = %d AND item_id = %d",
				Groups_Utility::id( $subscription_id ),
				Groups_Utility::id( $order_id ),
				Groups_Utility::id( $item_id )
			) );
			// must have affected a row, otherwise no great success
			$result = ( $rows !== false ) && ( $rows > 0 );
			if ( $result ) {
				do_action( "groups_deleted_subscription_order_item", $subscription_id, $order_id, $item_id );
			}
		}
		return $result;
	}
}
Groups_Subscription_Order_Item::init();
