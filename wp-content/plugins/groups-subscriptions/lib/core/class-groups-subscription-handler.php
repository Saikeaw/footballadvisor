<?php
/**
 * class-groups-subscription-handler.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 */

/**
 * Subscription Handler in charge of coordinating group membership with subscriptions.
 */
class Groups_Subscription_Handler {

	const GRACE = 86400;

	public static $grace = self::GRACE;

	/**
	 * Adds action hooks that control addition and removal of users to and from groups.
	 * While a subscription is active, the user should be a member of the groups that are
     * related to the subscription.
	 */
	public static function init() {

		self::$grace = get_option( 'groups_subscriptions_grace', self::GRACE );

		// when a user is deleted, delete subscriptions for that user
		add_action( 'deleted_user', array( __CLASS__, 'deleted_user' ) );
		
		// Schedule expiration checks.
		add_action( 'groups_created_subscription', array( __CLASS__, 'groups_created_subscription' ) );
		
		// add or remove user from groups depending on subscription status
		add_action( 'groups_updated_subscription', array( __CLASS__, 'groups_updated_subscription' ) );
		
		// remove user from groups & unschedule check
		add_action( 'groups_delete_subscription', array( __CLASS__, 'groups_delete_subscription' ) );
		
		// add user to group if subscription is active
		add_action( 'groups_created_group_subscription', array( __CLASS__, 'groups_created_group_subscription' ), 10, 2 );

		// actions for schedule hooks

		// these receive the $subscription_id
		add_action( 'Groups_Subscription_Handler_check_subscription', array( __CLASS__, 'check_subscription' ), 10, 1 );		
		add_action( 'Groups_Subscription_Handler_complete_subscription', array( __CLASS__, 'complete_subscription' ), 10, 1 );
		add_action( 'Groups_Subscription_Handler_safely_terminate', array( __CLASS__, 'safely_terminate' ), 10, 1 );

	}

	/**
	 * Returns the timestamp for the date the next payment is due. If no payment is due, null is returned.
	 * @param int $subscription_id
	 * @return int timestamp or null 
	 */
	public static function get_next_payment_timestamp( $subscription_id ) {
		$timestamp = null;
		if ( $subscription = Groups_Subscription::read( $subscription_id ) ) {
			if ( !$subscription->total_count || ( $subscription->current_count < $subscription->total_count ) ) {
				$n = $subscription->frequency * $subscription->current_count;
				$suffix = $n > 1 ? 's' : '';
				$timestamp = strtotime( $subscription->from_datetime . ' +' . $n . ' ' . $subscription->frequency_uom . $suffix );

				// if it ends before next calculated payment date, no payment is due
// 				if ( $subscription->thru_datetime !== null ) {
// 					if ( strtotime( $subscription->thru_datetime ) < $timestamp ) {
				if ( $thru_timestamp = self::get_end_of_subscription_timestamp( $subscription_id ) ) {
					if ( $thru_timestamp < $timestamp ) {
						$timestamp = null;
					}
				}
			}
		}
		return $timestamp;
	}
	
	/**
	 * Returns the timestamp for the date the subscription ends, if it's open, null is returned.
	 * @param int $subscription_id
	 * @return int timestamp or null
	 */
	public static function get_end_of_subscription_timestamp( $subscription_id ) {
		$timestamp = null;
		if ( $subscription = Groups_Subscription::read( $subscription_id ) ) {
			if ( $subscription->total_count ) {
				$n = $subscription->frequency * $subscription->total_count;
				$suffix = $n > 1 ? 's' : '';
				$timestamp = strtotime( $subscription->from_datetime . ' +' . $n . ' ' . $subscription->frequency_uom . $suffix );
			}
		}
		return $timestamp;
	}
	
	/**
	 * Returns the timestamp for the date the current paid subscription cycle ends.
	 * @param int $subscription_id
	 * @return int timestamp or null for invalid subscription id
	 */
	public static function get_end_of_current_cycle_timestamp( $subscription_id ) {
		$timestamp = null;
		if ( $subscription = Groups_Subscription::read( $subscription_id ) ) {
			// as current_count is the last paid iteration, 0 means that the end of cycle is the very start of the subscription,
			// 1 is at the end of the first cycle, etc.
			$n = $subscription->frequency * $subscription->current_count;
			$suffix = $n > 1 ? 's' : '';
			$timestamp = strtotime( $subscription->from_datetime . ' +' . $n . ' ' . $subscription->frequency_uom . $suffix );

			// if there is a given end that is earlier than the calculated end, use that
// 			if ( $subscription->thru_datetime !== null ) {
// 				if ( strtotime( $subscription->thru_datetime ) < $timestamp ) {
// 					$timestamp = strtotime( $subscription->thru_datetime );
// 				}
// 			}
			if ( $thru_timestamp = self::get_end_of_subscription_timestamp( $subscription_id ) ) {
				if ( $thru_timestamp < $timestamp ) {
					$timestamp = $thru_timestamp;
				}
			}
		}
		return $timestamp;
	}

	/**
	 * Delete subscriptions for that user.
	 * @param int $user_id
	 */
	public static function deleted_user( $user_id ) {
		global $wpdb;
		$subscription_table = _groups_get_tablename( 'subscription' );
		$rows = $wpdb->get_results( $wpdb->prepare(
			"SELECT * FROM $subscription_table WHERE user_id = %d",
			Groups_Utility::id( $user_id )
		) );
		if ( $rows ) {
			foreach( $rows as $row ) {
				// do it one by one, we need this to trigger actions 
				Groups_Subscription::delete( $row->subscription_id );
			}
		}
	}
	
	/**
	 * Schedules checks for the subscription.
	 * Add user to groups here? Not really, handled through groups_created_group_subscription.
	 * @param unknown_type $subscription_id
	 */
	public static function groups_created_subscription( $subscription_id ) {
		self::schedule_check_subscription( $subscription_id );
	}
	
	/**
	 * Safely schedule the next check for the subscription.
	 * Replaces checks that already have been scheduled.
	 * @param int $subscription_id
	 */
	public static function schedule_check_subscription( $subscription_id ) {

		$check_subscription_timestamps = Groups_Options::get_option( 'check_subscription_timestamps', array() );
		if ( isset( $check_subscription_timestamps[$subscription_id] ) ) {
			wp_unschedule_event( $check_subscription_timestamps[$subscription_id], 'Groups_Subscription_Handler_check_subscription' );
		}

		$timestamp = self::get_next_payment_timestamp( $subscription_id );
		if ( $timestamp !== null ) {
			wp_schedule_single_event( $timestamp, 'Groups_Subscription_Handler_check_subscription', array( $subscription_id ) );
			$check_subscription_timestamps[$subscription_id] = $timestamp;
			Groups_Options::update_option( 'check_subscription_timestamps', $check_subscription_timestamps );
		}
	}

	/**
	 * Remove scheduled check for the given subscription.
	 * @param int $subscription_id
	 */
	public static function unschedule_check_subscription( $subscription_id ) {
		$check_subscription_timestamps = Groups_Options::get_option( 'check_subscription_timestamps', array() );
		if ( isset( $check_subscription_timestamps[$subscription_id] ) ) {
			wp_unschedule_event( $check_subscription_timestamps[$subscription_id], 'Groups_Subscription_Handler_check_subscription' );
		}
		Groups_Options::update_option( 'check_subscription_timestamps', $check_subscription_timestamps );
	}
	
	/**
	 * Suspends subscriptions if payment is pending (only when the subscription is already active).
	 * Reschedules itself for active and suspended subscriptions.
	 * @param int $subscription_id
	 */
	public static function check_subscription( $subscription_id ) {

		Groups_Subscriptions_Log::log( __METHOD__ . ' : checking subscription with id ' . $subscription_id );

		if ( $subscription = Groups_Subscription::read( $subscription_id ) ) {

			if ( $subscription->status == Groups_Subscription::STATUS_ACTIVE ) {

				$now = time();
				$npt = self::get_next_payment_timestamp( $subscription_id );
				if ( $npt !== null ) {
					$delta =  $npt + self::$grace;
					// suspend subscription if last due payment has not been received
					if ( ( $now - strtotime( $subscription->last_payment ) ) > $delta ) {

						Groups_Subscriptions_Log::log( __METHOD__ . ' : suspending subscription with id = ' . $subscription->subscription_id );

						Groups_Subscription::update(
							array(
								'subscription_id' => $subscription->subscription_id,
								'status'          => Groups_Subscription::STATUS_SUSPENDED
							)
						);
						// Note that we don't reschedule checks in this case.
					} else {
						self::schedule_check_subscription( $subscription_id );
					}
				}
			}
		}
	}

	/**
	 * Add or remove user from groups depending on subscription status.
	 * @param int $subscription_id
	 */
	public static function groups_updated_subscription( $subscription_id ) {

		global $wpdb;

		if ( $subscription = Groups_Subscription::read( $subscription_id ) ) {
			switch( $subscription->status ) {
				case Groups_Subscription::STATUS_INACTIVE :
					// all subscriptions are created in the inactive state,
					// there's nothing to do here and this would not be a normal
					// or expecetd event to occur (updated subscription with
					// status inactive => that's dumb).
					break;
					
				case Groups_Subscription::STATUS_ACTIVE :
					// if the subscription status is 'active' :
					// - schedule check
					// - add the user to the subscription's groups
					self::schedule_check_subscription( $subscription_id );
					$group_subscription_table = _groups_get_tablename( 'group_subscription' );
					if ( $rows = $wpdb->get_results( $wpdb->prepare( "SELECT group_id FROM $group_subscription_table WHERE subscription_id = %d", Groups_Utility::id( $subscription_id ) ) ) ) {
						foreach( $rows as $row ) {
							if ( !Groups_User_Group::read( $subscription->user_id, $row->group_id ) ) {
								Groups_User_Group::create( array( 'user_id' => $subscription->user_id, 'group_id' => $row->group_id ) );
							}
						}
					}
					// status is active but thru_datetime !== null, so schedule to complete the subscription
					//if ( $subscription->thru_datetime !== null ) {
					if ( $timestamp = self::get_end_of_subscription_timestamp( $subscription_id ) ) { // * using derived instead of fixed thru_datetime
						wp_schedule_single_event( $timestamp, 'Groups_Subscription_Handler_complete_subscription', array( $subscription_id ) );
					}
					break;

				case Groups_Subscription::STATUS_CANCELLED :
					// schedule to remove the user from the subscription groups at the end of the current cycle
					$timestamp = self::get_end_of_current_cycle_timestamp( $subscription_id );
					if ( $timestamp !== null ) {
						// if it has a thru_datetime, the Groups_Subscription_Handler_complete_subscription is already scheduled
// 						if ( !$subscription->thru_datetime || ( strtotime( $subscription->thru_datetime ) != $timestamp ) ) {
						if ( !self::get_end_of_subscription_timestamp( $subscription_id ) ) { // *
							wp_schedule_single_event( $timestamp, 'Groups_Subscription_Handler_safely_terminate', array( $subscription_id ) );
						}
					}
					break;
					
				case Groups_Subscription::STATUS_COMPLETED :
				case Groups_Subscription::STATUS_EXPIRED :
				case Groups_Subscription::STATUS_SUSPENDED :
					// immediately remove the user from the subscription groups, excluding for those where another subscription grants access
					self::safely_terminate( $subscription_id );
					break;
			}
			if ( $subscription->status != Groups_Subscription::STATUS_ACTIVE ) {
				// - remove scheduled check for any other status
				self::unschedule_check_subscription( $subscription_id );
			}
		}
	}

	/**
	 * Removes the user from all of the subscription's groups and unschedules subscription check.
	 * Hooked on before actual deletion to be able to access user group data.
	 * @param int $subscription_id
	 */
	public static function groups_delete_subscription( $subscription_id ) {
		$subscription = new Groups_Subscription( $subscription_id );
		foreach( $subscription->groups as $group ) {
			if ( !self::count_other_active_subscriptions( $subscription->user_id, $subscription_id, $group->group_id ) ) {
				if ( Groups_User_Group::read( $subscription->user_id, $group->group_id ) ) {
					Groups_User_Group::delete( $subscription->user_id, $group->group_id );
				}
			}
		}
		// - remove scheduled check
		self::unschedule_check_subscription( $subscription_id );
	}

	/**
	 * Add user to group if subscription is active.
	 * @param int $group_id
	 * @param int $subscription_id
	 */
	public static function groups_created_group_subscription( $group_id, $subscription_id ) {
		// if the subscription status is 'active', add the user to the subscription's groups
		if ( $subscription = Groups_Subscription::read( $subscription_id ) ) {
			if ( $subscription->status == Groups_Subscription::STATUS_ACTIVE ) {
				if ( !Groups_User_Group::read( $subscription->user_id, $group_id ) ) {
					Groups_User_Group::create( array( 'user_id' => $subscription->user_id, 'group_id' => $group_id ) );
				}
			}
		}
	}

	/**
	 * Count active subscriptions for the given user and group.
	 *
	 * @param int $user_id
	 * @param int $group_id
	 * @return number of active subscriptions
	 */
	private static function count_active_subscriptions( $user_id, $group_id ) {
		global $wpdb;
		$subscription_table       = _groups_get_tablename( 'subscription' );
		$group_subscription_table = _groups_get_tablename( 'group_subscription' );
		$c = $wpdb->get_var( $wpdb->prepare (
			"SELECT COUNT(*) FROM $subscription_table s LEFT JOIN $group_subscription_table gs ON s.subscription_id = gs.subscription_id WHERE s.user_id = %d AND s.status = %s AND gs.group_id = %d",
			Groups_Utility::id( $user_id ),
			Groups_Subscription::STATUS_ACTIVE,
			Groups_Utility::id( $group_id )
		) );
		return $c;
	}

	/**
	 * Count other active subscriptions for the given user and group, not including
	 * the given subscription.
	 * 
	 * Used to determine if a user can safely be deleted from a group or not.
	 * 
	 * @param int $user_id
	 * @param int $subscription_id
	 * @param int $group_id
	 * @return number of other active subscriptions
	 */
	private static function count_other_active_subscriptions( $user_id, $subscription_id, $group_id ) {
		global $wpdb;
		$subscription_table       = _groups_get_tablename( 'subscription' );
		$group_subscription_table = _groups_get_tablename( 'group_subscription' );
		$c = $wpdb->get_var( $wpdb->prepare (
			"SELECT COUNT(*) FROM $subscription_table s LEFT JOIN $group_subscription_table gs ON s.subscription_id = gs.subscription_id WHERE s.user_id = %d AND s.subscription_id != %d AND s.status = %s AND gs.group_id = %d",
			Groups_Utility::id( $user_id ),
			Groups_Utility::id( $subscription_id ),
			Groups_Subscription::STATUS_ACTIVE,
			Groups_Utility::id( $group_id )
		) );
		return $c;
	}
	
	/**
	 * Removes the user from all of the subscription groups, unless other subscriptions grant access.
	 * The subscription must not be active.
	 * @param int $subscription_id
	 */
	public static function safely_terminate( $subscription_id ) {
		if ( Groups_Subscription::read( $subscription_id ) ) {
			$subscription = new Groups_Subscription( $subscription_id );
			if ( $subscription->status != Groups_Subscription::STATUS_ACTIVE ) {
				foreach( $subscription->groups as $group ) {
					if ( !self::count_other_active_subscriptions( $subscription->user_id, $subscription_id, $group->group_id ) ) {
						if ( Groups_User_Group::read( $subscription->user_id, $group->group_id ) ) {
							Groups_User_Group::delete( $subscription->user_id, $group->group_id );
						}
					}
				}
			}
		}
	}

	public static function cancel_subscription( $subscription_id ) {
		// @todo cancel Groups_Subscription
		// @todo gateway request
	}

	/**
	 * Called through scheduler when a subscription should be completed because it has a determined lifetime.
	 * The call is schedule through groups_updated_subscription(...).
	 * 
	 * @todo completed is the same as expired from the assumed semantics ... drop one of these statuses: expired, completed?
	 * 
	 * @param int $subscription_id
	 */
	public static function complete_subscription( $subscription_id ) {

		Groups_Subscriptions_Log::log( __METHOD__ . ' : checking subscription with id ' . $subscription_id );

		if ( Groups_Subscription::read( $subscription_id ) ) {
			$subscription = new Groups_Subscription( $subscription_id );
			if ( $subscription->status == Groups_Subscription::STATUS_ACTIVE ) {

				Groups_Subscriptions_Log::log( __METHOD__ . ' : completing subscription with id = ' . $subscription->subscription_id );

				Groups_Subscription::update(
					array(
						'subscription_id' => $subscription->subscription_id,
						'status'          => Groups_Subscription::STATUS_COMPLETED
					)
				);
			}
		}
	}

}
Groups_Subscription_Handler::init();
