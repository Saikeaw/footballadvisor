<?php
/**
 * class-groups-subscription.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 */

/**
 * Subscription OPM.
 */
class Groups_Subscription {

	const STATUS_INACTIVE  = 'inactive';
	const STATUS_ACTIVE    = 'active';
	const STATUS_EXPIRED   = 'expired'; // this status is currently never assigned
	const STATUS_CANCELLED = 'cancelled';
	const STATUS_COMPLETED = 'completed';
	const STATUS_SUSPENDED = 'suspended';

	const PAYMENT_STATUS_PENDING = 'pending';
	const PAYMENT_STATUS_COMPLETED = 'completed';

	const DAY   = 'day';
	const WEEK  = 'week';
	const MONTH = 'month';
	const YEAR  = 'year';

	const WAY_AHEAD = 1000000;

	public static function init() {
	}

	/**
	 * @var Object Persisted group.
	 */
	var $subscription = null;

	/**
	 * Create by subscription id.
	 * Must have been persisted.
	 * @param int $subscription_id
	 */
	public function __construct( $subscription_id ) {
		$this->subscription = self::read( $subscription_id );
	}
	
	/**
	 * Retrieve a property by name.
	 *
	 * Possible properties:
	 * - subscription_id
	 * - user_id
	 * - processor
	 * - reference
	 * - status
	 * - from_datetime
	 * - thru_datetime
	 * - last_payment
	 * - frequency
	 * - frequency_uom
	 * - total_count
	 * - current_count
	 * - description
	 * - data
	 * 
	 * - groups
	 * - order_item
	 * - current_cycle
	 * 
	 * @param string $name property's name
	 * @return property value, will return null if property does not exist
	 */
	public function __get( $name ) {

		global $wpdb;

		$result = null;
		if ( $this->subscription !== null ) {
			switch( $name ) {
				case 'subscription_id' :
				case 'user_id' :
				case 'processor' :
				case 'reference' :
				case 'status' :
				case 'from_datetime' :
				case 'thru_datetime' :
				case "last_payment" :
				case 'frequency' :
				case 'frequency_uom' :
				case "total_count" :
				case 'current_count' :
				case 'description' :
				case 'data' :
					$result = $this->subscription->$name;
					break;
				case 'groups' :
					$group_subscription_table = _groups_get_tablename( 'group_subscription' );
					$rows = $wpdb->get_results( $wpdb->prepare (
						"SELECT * FROM $group_subscription_table WHERE subscription_id = %d",
						Groups_Utility::id( $this->subscription->subscription_id )
					) );
					$result = array();
					foreach( $rows as $row ) {
						$result[] = new Groups_Group( $row->group_id );
					}
					break;
				case 'order_item' :
					$subscription_order_item_table = _groups_get_tablename( 'subscription_order_item' );
					$rows = $wpdb->get_results( $wpdb->prepare (
						"SELECT * FROM $subscription_order_item_table WHERE subscription_id = %d",
						Groups_Utility::id( $this->subscription->subscription_id )
					) );
					$result = null;
					foreach( $rows as $row ) {
						$result = new Groups_Subscription_Order_Item( $row->subscription_id, $row->order_id, $row->item_id );
						break;
					}
					break;
				case 'current_cycle' :
					$now = time();
					$start = strtotime( $this->from_datetime );
					$delta =  strtotime( $this->from_datetime . ' +' . $this->frequency . ' ' . $this->frequency_uom . ( $this->frequency > 1 ? 's' : '' ) ) - $start;
					$result = 1 + floor( ( $now - $start ) / $delta );
					break;
			}
		}
		return $result;
	}

	/**
	 * Persist a subscription.
	 * 
	 * Parameters:
	 * 
	 * - user_id (required) - subscriber's user id
	 * - processor (required to retrieve by processor and reference) - processor's name
	 * - reference (required to retrieve by processor and reference) - processor's reference / subscription id
	 * - status (optional) 
	 * - from_datetime (optional) - datetime subscription start, defaults to now
	 * - thru_datetime (optional) - datetime subscription end, defaults to null
	 * - frequency (?) - int fulfillment frequency
	 * - frequency_uom (?) - string unit of measurement of the fulfillment frequency (days, weeks, months, years)
	 * - total_count (?) - number of fulfillments, if null, subscription will not expire unless cancelled
	 * - current_count (?) - fulfillment counter reflecting current fulfillment cycle
	 * - description (optional)
	 * - data (optional)
	 * @param array $map attributes
	 * @return subscription_id on success, otherwise false
	 */
	public static function create( $map ) {
		global $wpdb;
		extract( $map );
		$result = false;
		$error = false;		
		
		if ( !empty( $user_id ) ) {
			
			$subscription_table = _groups_get_tablename( "subscription" );

			$values = array( 'user_id' => Groups_Utility::id( $user_id ) );
			$formats = array( '%d' );

			if ( isset( $processor ) ) {
				$values['processor'] = $processor;
				$formats[] = '%s';
			}
			if ( isset( $reference ) ) {
				$values['reference'] = $reference;
				$formats[] = '%s';
			}
			if ( isset( $status ) ) {
				switch( $status ) {
					case self::STATUS_ACTIVE :
					case self::STATUS_CANCELLED :
					case self::STATUS_COMPLETED :
					case self::STATUS_EXPIRED :
					case self::STATUS_INACTIVE :
					case self::STATUS_SUSPENDED :
						break;
					default :
						$status = self::STATUS_INACTIVE;
				}
				$values['status'] = $status;
				$formats[] = '%s';
			}
			if ( !isset( $from_datetime ) ) {
				$from_datetime = date( 'Y-m-d H:i:s', time() );
			}
			if ( isset( $from_datetime ) ) {
				$values['from_datetime'] = date( 'Y-m-d H:i:s', strtotime( $from_datetime ) );
				$formats[] = '%s';
			}
			if ( isset( $thru_datetime ) ) {
				$values['thru_datetime'] = date( 'Y-m-d H:i:s', strtotime( $thru_datetime ) );
				$formats[] = '%s';
			}
			if ( isset( $last_payment ) ) {
				$values['last_payment'] = date( 'Y-m-d H:i:s', strtotime( $last_payment ) );
				$formats[] = '%s';
			}
			if ( isset( $frequency ) ) {
				$values['frequency'] = intval( $frequency );
				$formats[] = '%d';
			}
			if ( isset( $frequency_uom ) ) {
				switch( $frequency_uom ) {
					case self::DAY :
					case self::WEEK :
					case self::MONTH :
					case self::YEAR :
						break;
					default :
						$frequency_uom = self::MONTH;
				}
				$values['frequency_uom'] = $frequency_uom;
				$formats[] = '%s';
			}
			if ( isset( $total_count ) ) {
				$values['total_count'] = intval( $total_count );
				$formats[] = '%d';
			}
			if ( isset( $current_count ) ) {
				$values['current_count'] = intval( $current_count );
				$formats[] = '%d';
			}
			if ( !empty( $description ) ) {
				$values['description'] = $description;
				$formats[] = '%s';
			}
			if ( isset( $data ) ) {
				$values['data'] = $data;
				$formats[] = '%s';
			}

			if ( $wpdb->insert( $subscription_table, $values, $formats ) ) {
				if ( $result = $wpdb->get_var( "SELECT LAST_INSERT_ID()" ) ) {
					do_action( "groups_created_subscription", $result );
				}
			}

		}
		return $result;
	}
	
	/**
	 * Retrieve a subscription.
	 * 
	 * @param int $subscription_id subscription's id
	 * @return object upon success, otherwise false
	 */
	public static function read( $subscription_id ) {
		global $wpdb;
		$result = false;
		
		$subscription_table = _groups_get_tablename( 'subscription' );
		$subscription = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $subscription_table WHERE subscription_id = %d",
			Groups_Utility::id( $subscription_id )
		) );
		if ( isset( $subscription->subscription_id ) ) {
			$result = $subscription;
		}
		return $result;
	}
	
	/**
	 * Retrieve a subscription by processor and reference.
	 *
	 * @param string $processor the payment processor
	 * @param string $reference the subscription's reference
	 * @return object upon success, otherwise false
	 */
	public static function read_by_reference( $processor, $reference ) {
		global $wpdb;
		$result = false;
		$subscription_table = _groups_get_tablename( 'subscription' );
		$subscription = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $subscription_table WHERE processor = %s AND reference = %s",
			$processor,
			$reference
		) );
		if ( isset( $subscription->subscription_id ) ) {
			$result = $subscription;
		}
		return $result;
	}
	
	/**
	 * Update a subscription.
	 * 
	 * @param array $map subscription fields, must contain subscription_id
	 * @return subscription_id on success, otherwise false
	 */
	public static function update( $map ) {
		
		global $wpdb;
		extract( $map );
		$result = false;
		
		if ( isset( $subscription_id ) ) {

			$set = array();
			$values = array();

			if ( isset( $user_id ) ) {
				$set[] = 'user_id = %d';
				$values[] = Groups_Utility::id( $user_id );
			}
			if ( isset( $processor ) ) {
				$set[] = 'processor = %s';
				$values[] = $processor;
			}
			if ( isset( $reference ) ) {
				$set[] = 'reference = %s';
				$values[] = $reference;
			}
			if ( isset( $status ) ) {
				switch( $status ) {
					case self::STATUS_ACTIVE :
					case self::STATUS_CANCELLED :
					case self::STATUS_COMPLETED :
					case self::STATUS_EXPIRED :
					case self::STATUS_INACTIVE :
					case self::STATUS_SUSPENDED :
						break;
					default :
						$status = self::STATUS_INACTIVE;
				}
				$set[] = 'status = %s';
				$values[] = $status;
			}
			if ( isset( $from_datetime ) ) {
				$set[] = 'from_datetime = %s';
				$values[] = date( 'Y-m-d H:i:s', strtotime( $from_datetime ) );
			}
			if ( isset( $thru_datetime ) ) {
				$set[] = 'thru_datetime = %s';
				$values[] = date( 'Y-m-d H:i:s', strtotime( $thru_datetime ) );
			}
			if ( isset( $last_payment ) ) {
				$set[] = 'last_payment = %s';
				$values[] = date( 'Y-m-d H:i:s', strtotime( $last_payment ) );
			}
			if ( isset( $frequency ) ) {
				$set[] = 'frequency = %d';
				$values = intval( $frequency );
			}
			if ( isset( $frequency_uom ) ) {
				switch( $frequency_uom ) {
					case self::DAY :
					case self::WEEK :
					case self::MONTH :
					case self::YEAR :
						break;
					default :
						$frequency_uom = self::MONTH;
				}
				$set[] = 'frequency_uom = %s';
				$values[] = $frequency_uom;
			}
			if ( isset( $total_count ) ) {
				$set[] = 'total_count = %d';
				$values[] = $total_count;
			}
			if ( isset( $current_count ) ) {
				$set[] = 'current_count = %d';
				$values[] = $current_count;
			}
			if ( isset( $description ) ) {
				$set[] = 'description = %s';
				$values[] = $description;
			}
			if ( isset( $data ) ) {
				$set[] = 'data = %s';
				$values[] = $data;
			}
			$values[] = Groups_Utility::id( $subscription_id );

			Groups_Subscriptions_Log::log( __METHOD__ . ' : set = ' . var_export( $set, true ));
			Groups_Subscriptions_Log::log( __METHOD__ . ' : values = ' . var_export( $values, true ));

			if ( ( count( $set ) > 0 ) && ( count( $values ) > 0 ) ) {
				$set = implode( ',', $set );
				$subscription_table = _groups_get_tablename( 'subscription' );
				$wpdb->query( $wpdb->prepare(
					"UPDATE $subscription_table SET $set WHERE subscription_id = %d",
					$values
				) );
				$result = $subscription_id;
				do_action( "groups_updated_subscription", $result );
			}
		}
		return $result;
	}
	
	/**
	 * Remove a subscription.
	 * 
	 * @param int $subscription_id
	 * @return subscription_id if successful, false otherwise
	 */
	public static function delete( $subscription_id ) {

		global $wpdb;
		$result = false;
		
		if ( $subscription = self::read( $subscription_id ) ) {
			do_action( "groups_delete_subscription", $subscription_id);
			$subscription_table = _groups_get_tablename( 'subscription' );
			// delete subscription
			if ( $wpdb->query( $wpdb->prepare(
				"DELETE FROM $subscription_table WHERE subscription_id = %d",
				Groups_Utility::id( $subscription->subscription_id )
			) ) ) {
				$result = $subscription->subscription_id;
				do_action( "groups_deleted_subscription", $result );
			}
		}
		return $result;
	}
}
Groups_Subscription::init();
