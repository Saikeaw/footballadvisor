<?php
/**
 * class-groups-subscription-terminator.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.2.0
 */

/**
 * Terminates activate subscriptions when overdue.
 * Safety-valve for dangling active subscriptions that should not be active.
 */
class Groups_Subscription_Terminator {

	public static function init() {

		add_action( 'groups_subscription_terminator_crunch', array( __CLASS__, 'crunch' ), 10, 0 );
		$crunching = wp_next_scheduled( 'groups_subscription_terminator_crunch' );
		if ( !$crunching ) {
			wp_schedule_event( time(), 'hourly', 'groups_subscription_terminator_crunch' );
		}

		// From the description of register_deactivation_hook:
		// "Set the deactivation hook for a plugin." ... which would imply
		// you can only have one. It's not implemented like that (Wp 3.4.2),
		// but to be safe, rather than do ...
		// register_deactivation_hook( GROUPS_SUBSCRIPTIONS_FILE, array( __CLASS__, 'deactivate' ) );
		// ... we use our own action hook:
		add_action( 'groups_subscriptions_before_cleanup', array( __CLASS__,'deactivate' ) );
	}

	public static function deactivate() {
		wp_clear_scheduled_hook( 'groups_subscription_terminator_crunch' );
	}

	/**
	 * Terminator action on active overdue subscriptions: complete or suspend.
	 * This is a 'security-valve' that protects from failed scheduled calls to
	 * Groups_Subscription_Handler::check_subscription() and
	 * Groups_Subscription_Handler::complete_subscription() so that subscriptions
	 * are eventually suspended or completed even if these normal scheduled
	 * handlers have failed.
	 * @see Groups_Subscription_Handler::check_subscription()
	 * @see Groups_Subscription_Handler::complete_subscription()
	 */
	public static function crunch() {
		
		Groups_Subscriptions_Log::log( __METHOD__ . ' @ ' . date( 'Y-m-d H:i:s', time() ) );

		global $wpdb;

		// get active subscriptions
		$subscription_table = _groups_get_tablename( 'subscription' );

		// fast & dumb, resource-hungry on huge sets: heavy on PHP (but light on DB)
// 		$subscriptions = $wpdb->get_results( $wpdb->prepare(
// 			"SELECT * FROM $subscription_table WHERE status = %s",
// 			Groups_Subscription::STATUS_ACTIVE
// 		) );

		// smart & slow, resource-saving on huge sets: light on PHP (math on DB)
		$subscriptions = $wpdb->get_results( $wpdb->prepare(
			"SELECT * FROM $subscription_table
			WHERE status = %s AND
			%s >= CASE frequency_uom
				WHEN 'day' THEN from_datetime + INTERVAL frequency*current_count DAY
				WHEN 'week' THEN from_datetime + INTERVAL frequency*current_count WEEK
				WHEN 'month' THEN from_datetime + INTERVAL frequency*current_count MONTH
				WHEN 'year' THEN from_datetime + INTERVAL frequency*current_count YEAR END",
			Groups_Subscription::STATUS_ACTIVE,
			date( 'Y-m-d H:i:s', time() )
		) );

		// complete or suspend overdue subscriptions that have not been handled
		// by their normal scheduled tasks
		$now = time();
		foreach ( $subscriptions as $subscription ) {

			Groups_Subscriptions_Log::log( __METHOD__ . ' : checking subscription with id = ' . $subscription->subscription_id );

			// complete subscription ?
			$end = Groups_Subscription_Handler::get_end_of_subscription_timestamp( $subscription->subscription_id );
			if ( ( $end !== null ) && ( $now > $end ) ) {

					Groups_Subscriptions_Log::log( __METHOD__ . ' : completing subscription with id = ' . $subscription->subscription_id );

					Groups_Subscription::update(
						array(
							'subscription_id' => $subscription->subscription_id,
							'status'          => Groups_Subscription::STATUS_COMPLETED
						)
					);
			} else {
				// unpaid subscription?
				$npt = Groups_Subscription_Handler::get_next_payment_timestamp( $subscription->subscription_id );
				if ( $npt !== null ) {
					$delta =  $npt + Groups_Subscription_Handler::$grace;
					// suspend subscription if last due payment has not been received
					if ( ( $now - strtotime( $subscription->last_payment ) ) > $delta ) {

						Groups_Subscriptions_Log::log( __METHOD__ . ' : suspending subscription with id = ' . $subscription->subscription_id );

						Groups_Subscription::update(
							array(
								'subscription_id' => $subscription->subscription_id,
								'status'          => Groups_Subscription::STATUS_SUSPENDED
							)
						);
					}
				}
			}

		}
	}
	
}
Groups_Subscription_Terminator::init();
