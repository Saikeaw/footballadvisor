<?php
/**
 * class-groups-subscriptions-admin-user-profile.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 2.2.0
 */

/**
 * Renders subscription info on user profile pages.
 */
class Groups_Subscriptions_Admin_User_Profile {

	/**
	 * Hook into profile.
	 */
	public static function init() {
		add_action( 'show_user_profile', array( __CLASS__, 'show_user_profile' ) );
		add_action( 'edit_user_profile', array( __CLASS__, 'edit_user_profile' ) );
	}

	/**
	 * Displays subscription info when a user is viewing his own profile page -
	 * hooked on show_user_profile.
	 * @param WP_User $user
	 */
	public static function show_user_profile( $user ) {
		self::edit_user_profile( $user );
	}

	/**
	 * Display subscription info when viewing a user's profile page -
	 * hooked on edit_user_profile.
	 * @param WP_User $user
	 */
	public static function edit_user_profile( $user ) {
		
		if ( $user->ID != get_current_user_id() && !current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			return;
		}

		global $wpdb;

		$subscription_table = _groups_get_tablename( 'subscription' );

		$show_all = false;

		if ( !$show_all ) {
			$query = $wpdb->prepare(
				"SELECT * FROM $subscription_table
				LEFT JOIN $wpdb->users ON $subscription_table.user_id = $wpdb->users.ID
				WHERE $subscription_table.user_id = %d AND $subscription_table.status = %s
				ORDER BY subscription_id DESC",
				Groups_Utility::id( $user->ID ),
				Groups_Subscription::STATUS_ACTIVE
			);
		} else {
			$query = $wpdb->prepare(
				"SELECT * FROM $subscription_table
				LEFT JOIN $wpdb->users ON $subscription_table.user_id = $wpdb->users.ID
				WHERE $subscription_table.user_id = %d
				ORDER BY subscription_id DESC",
				Groups_Utility::id( $user->ID )
			);
		}

		$results = $wpdb->get_results( $query, OBJECT );

		$output = '<h3>' . __( 'Subscriptions', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</h3>';

		$n = count( $results );

		$output .= '<p>';
		if ( $n > 0 ) {
			$output .= sprintf( _n( 'One active subscription.', '%d active subscriptions', $n, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $n );
		} else {
			$output .= __( 'No active subscriptions.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
		}
		$output .= '</p>';

		if ( $n > 0 ) {
			$column_display_names = array(
				'subscription_id'  => __( 'Id', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'processor'        => __( 'Processor', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'reference'        => __( 'Reference', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'status'           => __( 'Status', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'dates'            => __( 'Dates', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'cycle'            => __( 'Cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'description'      => __( 'Description', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'groups'           => __( 'Groups', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
				'order'            => __( 'Order', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			);
			$output .= '<table id="" class="wp-list-table widefat fixed" cellspacing="0">';
			$output .= '<thead>';
			$output .= '<tr>';

			foreach ( $column_display_names as $key => $column_display_name ) {
				$output .= "<th scope='col' class='$key'>$column_display_name</th>";
			}

			$output .= '</tr>';
			$output .= '</thead>';
			$output .= '<tbody>';

			for ( $i = 0; $i < count( $results ); $i++ ) {
				$result = $results[$i];
				$subscription = new Groups_Subscription( $result->subscription_id );
				$output .= '<tr class="' . ( $i % 2 == 0 ? 'even' : 'odd' ) . '">';

				$output .= "<td class='subscription-id'>" . $result->subscription_id . "</td>";
				$output .= "<td class='processor'>" . stripslashes( wp_filter_nohtml_kses( $result->processor ) ) . "</td>";
				$output .= "<td class='reference'>" . stripslashes( wp_filter_nohtml_kses( $result->reference ) ) . "</td>";
				$output .= "<td class='status'>" . stripslashes( wp_filter_nohtml_kses( $result->status ) ) . "</td>";

				$output .= "<td class='dates'>";
				$output .= '<ul>';
				$output .= '<li>' . sprintf( __( 'From %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->from_datetime ) . '</li>';
				if ( intval( $result->frequency ) < Groups_Subscription::WAY_AHEAD ) { // only if it's not a one-time payment
					if ( $thru_timestamp = Groups_Subscription_Handler::get_end_of_subscription_timestamp( $result->subscription_id ) ) {
						$output .= '<li>' . sprintf( __( 'Until %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), date( 'Y-m-d H:i:s', $thru_timestamp ) ) . '</li>';
					}
				}
				if ( $result->last_payment ) {
					$output .= '<li>' . sprintf( __( 'Last payment %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->last_payment ) . '</li>';
				}
				$output .= '</ul>';
				$output .= "</td>";

				$output .= "<td class='cycle'>";
				$cycle = '';
				if ( $result->status == Groups_Subscription::STATUS_ACTIVE ) {
					$current_cycle = $subscription->current_cycle;
					$current_cycle_tooltip_label = __( 'Current cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
				} else if ( $result->status == Groups_Subscription::STATUS_COMPLETED ) {
					$current_cycle = min( array( $result->current_count, $subscription->current_cycle ) );
					$current_cycle_tooltip_label = __( 'Last cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
				} else {
					$current_cycle = min( array( $result->current_count + 1, $subscription->current_cycle ) );
					$current_cycle_tooltip_label = __( 'Last cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
				}
				if ( !$result->total_count ) {
					switch( $result->frequency_uom ) {
						case Groups_Subscription::DAY :
							$t = _n( 'every day', 'every %d days', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::WEEK :
							$t = _n( 'every week', 'every %d weeks', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::YEAR :
							$t = _n( 'every year', 'every %d years', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						default : // including Groups_Subscription::MONTH
							$t = _n( 'every month', 'every %d months', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
					}
					// far away in the future ... ?
					if ( intval( $result->frequency ) >= Groups_Subscription::WAY_AHEAD ) {
						$t = '&infin;';
					}
					$cycle = sprintf( $t, $result->frequency );
					$cycle .= '<br/>';
					$cycle .= sprintf( '<span class="tooltip" title="%s">#%d</span> / <span class="tooltip" title="%s">#%d</span>', __( 'Payments received', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->current_count, $current_cycle_tooltip_label, $current_cycle );
				} else {
					switch( $result->frequency_uom ) {
						case Groups_Subscription::DAY :
							$t = _n( 'every day for %2$d days', 'every %1$d days for %2$d days', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::WEEK :
							$t = _n( 'every week for %2$d weeks', 'every %1$d weeks for %2$d weeks', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::YEAR :
							$t = _n( 'every year for %2$d years', 'every %1$d years for %2$d years', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						default : // including Groups_Subscription::MONTH
							$t = _n( 'every month for %2$d months', 'every %1$d months for %2$d months', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
					}
					$cycle = sprintf( $t, $result->frequency, $result->frequency * $result->total_count );
					$cycle .= '<br/>';
					$cycle .= sprintf( '<span class="tooltip" title="%s">#%d</span> / <span class="tooltip" title="%s">#%d</span>', __( 'Payments received', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->current_count, $current_cycle_tooltip_label, $current_cycle );
				}
				$output .= apply_filters( 'groups_subscriptions_admin_render_subscription_cycle', $cycle, $result->subscription_id );
				$output .= "</td>";

				$output .= "<td class='description'>" . stripslashes( wp_filter_nohtml_kses( $result->description ) ) . "</td>";

				$output .= "<td class='groups'>";
				$output .= '<ul>';
				foreach( $subscription->groups as $group ) {
					$output .= '<li>' . wp_filter_nohtml_kses( $group->name ) . '</li>';
				}
				$output .= '</ul>';
				$output .= "</td>";

				if ( $order_item = $subscription->order_item ) {
					$output .= "<td class='order'>";
					$output .= apply_filters( 'groups_subscriptions_admin_render_subscription_order_item', sprintf( __( 'Order %d Item %d', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $order_item->order_id, $order_item->item_id ), $result->subscription_id );
					$output .= "</td>";
				} else {
					$output .= "<td class='order'>&nbsp;</td>";
				}
				$output .= '</tr>';
			}
			$output .= '</tbody>';
			$output .= '</table>';
		}

		echo $output;
	}

}
Groups_Subscriptions_Admin_User_Profile::init();
