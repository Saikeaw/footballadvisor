<?php
/**
 * class-groups-subscriptions-admin-subscriptions.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.3.0
 */

require_once( GROUPS_CORE_LIB . '/class-groups-pagination.php' );

/**
 * Admin section for Groups Subscriptions.
 */
class Groups_Subscriptions_Admin_Settings {

	const NONCE = 'groups-subscriptions-admin-nonce';

	// @todo bulk actions delete selected subscriptions
	// @todo option to purge inactive subscriptions after grace period

	/**
	 * Show subscriptions
	 */
	public static function view() {

		global $wpdb;

		$grace_values = array(
			'0'     => __( 'None', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'3600'  => __( '1 hour', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'7200'  => __( '2 hours', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'10800' => __( '3 hours', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'21600' => __( '6 hours', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'43200' => __( '12 hours', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'86400' => __( '24 hours', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'172800' => __( '48 hours', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'259200' => __( '72 hours', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
		);

		$output = '';

		if ( !current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			wp_die( __( 'Access denied.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
		}

		$log = get_option( 'groups_subscriptions_log', false );
		$grace = get_option( 'groups_subscriptions_grace', Groups_Subscription_Handler::GRACE );
		$show_in_user_profile = get_option( 'groups_subscriptions_show_in_user_profile', true );

		if ( isset( $_POST['submit'] ) ) {
			
			if ( !wp_verify_nonce( $_POST[self::NONCE], 'settings' ) ) {
				wp_die( __( 'I fart in your general direction.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
			}

			$log = isset( $_POST['log'] );
			$grace = isset( $_POST['grace'] ) ? intval( $_POST['grace'] ) : 0;
			$show_in_user_profile = isset( $_POST['show_in_user_profile'] );

			add_option( 'groups_subscriptions_log', $log, null, 'no' );
			add_option( 'groups_subscriptions_grace', $grace, null, 'no' );
			add_option( 'groups_subscriptions_show_in_user_profile', $show_in_user_profile, null, 'no' );

			update_option( 'groups_subscriptions_log', $log );
			update_option( 'groups_subscriptions_grace', $grace );
			update_option( 'groups_subscriptions_show_in_user_profile', $show_in_user_profile );

			// update_option( 'foo', false ); will NOT create the option foo if it doesn't exist (WP 3.5.1)
			// update_option does NOT allow to specify whether an option should be autoloaded
			// add_option does NOT update an option if it already exists
		}

		// options form
		$output .=
			'<h2>' . __( 'Subscription Settings', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</h2>' .
			'<div class="groups-subscriptions-settings manage">' .
			'<form action="" name="settings" method="post">' .		
			'<div>';

		$output .=
			'<h3>' . __( 'Payment grace period', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</h3>' .
			'<p>' .
			'<label>' .
			__( 'Grace period', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) .
			'<select name="grace">';
		foreach( $grace_values as $value => $name ) {
			$output .= sprintf( '<option value="%d" %s>%s</option>', $value, ( $grace == $value ? ' selected="selected" ' : '' ), $name );
		}
		$output .=
			'</select>' .
			'</label>' .
			'</p>' .
			'<p class="description">' .
			__( 'The grace period determines how long after payment due a subscription will be allowed to be active until suspended. This is an approximate value and the actual time a subscription will be active in this case may vary.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) .
			'</p>';

		$output .=
			'<h3>' . __( 'Logging', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</h3>' .

			'<p>' .
			'<label>'.
			'<input name="log" type="checkbox" ' . ( $log ? 'checked="checked"' : '' ) . '/>' .
			__( 'Enable logging', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) .
			'</label>' .
			'</p>' .
			'<p class="description warning">' .
			__( 'CAUTION: Do not use this option on a production site as sensitive data might be exposed.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) .
			'</p>';

		$output .=
		'<h3>' . __( 'User profiles', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</h3>' .
		
		'<p>' .
		'<label>'.
		'<input name="show_in_user_profile" type="checkbox" ' . ( $show_in_user_profile ? 'checked="checked"' : '' ) . ' />' .
		__( 'Show subscription information in user profiles', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) .
		'</label>' .
		'</p>' .
		'<p class="description warning">' .
		__( 'If enabled, shows subscription information to members in their user profile.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) .
		'</p>';

		$output .=
			'<p>' .
			wp_nonce_field( 'settings', self::NONCE, true, false ) .
			'<input type="submit" name="submit" value="' . __( 'Save', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '"/>' .
			'</p>' .
			'</div>' .
			'</form>' .
			'</div>';

		echo $output;
		Groups_Help::footer();
	}

}

