<?php
/**
 * class-groups-subscriptions-admin-subscriptions.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 */

require_once( GROUPS_CORE_LIB . '/class-groups-pagination.php' );

/**
 * Admin section for Groups Subscriptions.
 */
class Groups_Subscriptions_Admin_Subscriptions {

	const NONCE        = 'groups-subscriptions-admin-nonce';
	const NONCE_1      = 'groups-subscriptions-nonce-1';
	const NONCE_2      = 'groups-subscriptions-nonce-2';
	const ACTION_NONCE = 'groups-subscriptions-action-nonce';
	const FILTER_NONCE = 'groups-subscriptions-filter-nonce';

	const PER_PAGE = 10;

	/**
	 * Show subscriptions
	 */
	public static function view() {

		global $wpdb;

		$output = '';

		if ( !current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			wp_die( __( 'Access denied.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
		}

		if (
				isset( $_POST['clear_filters'] ) ||
				isset( $_POST['submitted'] )
		) {
			if ( !wp_verify_nonce( $_POST[self::FILTER_NONCE], 'admin' ) ) {
				wp_die( __( 'Your mother was a hamster and your father smelt of elderberries!', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
			}
		}

		// filters
		$subscription_id  = Groups_Options::get_user_option( 'groups_subscriptions_subscription_id', null );
		$user             = Groups_Options::get_user_option( 'groups_subscriptions_user', null );
		$processor        = Groups_Options::get_user_option( 'groups_subscriptions_processor', null );
		$reference        = Groups_Options::get_user_option( 'groups_subscriptions_reference', null );
		$status           = Groups_Options::get_user_option( 'groups_subscriptions_status', null );
		// @todo from_datetime
		// @todo thru_datetime
		// @todo last_payment
		// @todo frequency
		// @todo frequency_uom
		// @todo total_count
		// @todo current_count
		$description      = Groups_Options::get_user_option( 'groups_subscriptions_description', null );

		if ( isset( $_POST['clear_filters'] ) ) {
			Groups_Options::delete_user_option( 'groups_subscriptions_subscription_id' );
			Groups_Options::delete_user_option( 'groups_subscriptions_user' );
			Groups_Options::delete_user_option( 'groups_subscriptions_processor' );
			Groups_Options::delete_user_option( 'groups_subscriptions_reference' );
			Groups_Options::delete_user_option( 'groups_subscriptions_status' );
			Groups_Options::delete_user_option( 'groups_subscriptions_description' );
			$subscription_id = null;
			$user            = null;
			$processor       = null;
			$reference       = null;
			$status          = null;
			$description     = null;
		} else if ( isset( $_POST['submitted'] ) ) {

			// filter by subscription id
			if ( !empty( $_POST['subscription_id'] ) ) {
				$subscription_id = intval( $_POST['subscription_id'] );
				Groups_Options::update_user_option( 'groups_subscriptions_subscription_id', $subscription_id );
			} else if ( isset( $_POST['subscription_id'] ) ) { // empty && isset => '' => all
				$subscription_id = null;
				Groups_Options::delete_user_option( 'groups_subscriptions_subscription_id' );
			}

			// filter by user
			if ( !empty( $_POST['user'] ) ) {
				$user = trim( $_POST['user'] );
				Groups_Options::update_user_option( 'groups_subscriptions_user', $user );
			} else if ( isset( $_POST['user'] ) ) {
				$user = null;
				Groups_Options::delete_user_option( 'groups_subscriptions_user' );
			}

			if ( !empty( $_POST['processor'] ) ) {
				$processor = $_POST['processor'];
				Groups_Options::update_user_option( 'groups_subscriptions_processor', $processor );
			} else {
				$processor = null;
				Groups_Options::delete_user_option( 'groups_subscriptions_processor' );
			}

			if ( !empty( $_POST['reference'] ) ) {
				$reference = $_POST['reference'];
				Groups_Options::update_user_option( 'groups_subscriptions_reference', $reference );
			} else {
				$reference = null;
				Groups_Options::delete_user_option( 'groups_subscriptions_reference' );
			}

			if ( !empty( $_POST['status'] ) ) {
				$status = $_POST['status'];
				Groups_Options::update_user_option( 'groups_subscriptions_status', $status );
			} else {
				$status = null;
				Groups_Options::delete_user_option( 'groups_subscriptions_status' );
			}

			if ( !empty( $_POST['description'] ) ) {
				$description = $_POST['description'];
				Groups_Options::update_user_option( 'groups_subscriptions_description', $description );
			} else {
				$description = null;
				Groups_Options::delete_user_option( 'groups_subscriptions_description' );
			}
		}

		if ( isset( $_POST['row_count'] ) ) {
			if ( !wp_verify_nonce( $_POST[self::NONCE_1], 'admin' ) ) {
				wp_die( __( 'Access denied.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
			}
		}

		if ( isset( $_POST['paged'] ) ) {
			if ( !wp_verify_nonce( $_POST[self::NONCE_2], 'admin' ) ) {
				wp_die( __( 'Access denied.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
			}
		}

		$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$current_url = remove_query_arg( 'paged', $current_url );
		$current_url = remove_query_arg( 'action', $current_url );
		$current_url = remove_query_arg( 'subscription_id', $current_url );

		$subscription_table            = _groups_get_tablename( 'subscription' );
		$group_table                   = _groups_get_tablename( 'group' );
		$group_subscription_table      = _groups_get_tablename( 'group_subscription' );
		$subscription_order_item_table = _groups_get_tablename( 'subscription_order_item' );

		$output .=
		'<div class="manage-groups-subscriptions">' .
		'<h2>' . __( 'Subscriptions', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</h2>';

// 		$output .=
// 		'<div class="manage">' .
// 		"<a title='" . __( 'Click to add a new subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . "' class='add button' href='" . esc_url( $current_url ) . "&action=add'><img class='icon' alt='" . __( 'Add', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN) . "' src='". GROUPS_SUBSCRIPTIONS_PLUGIN_URL ."images/add.png'/><span class='label'>" . __( 'New Subscription', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN) . "</span></a>" .
// 		'</div>';

		$row_count = isset( $_POST['row_count'] ) ? intval( $_POST['row_count'] ) : 0;

		if ($row_count <= 0) {
			$row_count = Groups_Options::get_user_option( 'groups_subscriptions_per_page', self::PER_PAGE );
		} else {
			Groups_Options::update_user_option('groups_subscriptions_per_page', $row_count );
		}
		$offset = isset( $_GET['offset'] ) ? intval( $_GET['offset'] ) : 0;
		if ( $offset < 0 ) {
			$offset = 0;
		}
		$paged = isset( $_GET['paged'] ) ? intval( $_GET['paged'] ) : 0;
		if ( $paged < 0 ) {
			$paged = 0;
		}

		$orderby = isset( $_GET['orderby'] ) ? $_GET['orderby'] : null;
		switch ( $orderby ) {
			case 'subscription_id' :
			case 'user_login' :
			case 'processor' :
			case 'reference' :
			case 'status' :
			case 'description' :
				break;
			default:
				$orderby = 'subscription_id';
		}

		$order = isset( $_GET['order'] ) ? $_GET['order'] : null;
		switch ( $order ) {
			case 'asc' :
			case 'ASC' :
				$switch_order = 'DESC';
				break;
			case 'desc' :
			case 'DESC' :
				$switch_order = 'ASC';
				break;
			default:
				if ( $orderby != 'subscription_id' ) {
					$order = 'ASC';
					$switch_order = 'DESC';
				} else {
					$order = 'DESC';
					$switch_order = 'ASC';
				}
		}

		$filters = array( ' 1=%d ' );
		$filter_params = array( 1 );
		if ( $subscription_id ) {
			$filters[] = " $subscription_table.subscription_id = %d ";
			$filter_params[] = $subscription_id;
		}
		if ( $user ) {
			$filters[] = " $wpdb->users.user_login LIKE '%%%s%%' ";
			$filter_params[] = $user;
		}
		if ( $processor ) {
			$filters[] = " $subscription_table.processor LIKE '%%%s%%' ";
			$filter_params[] = $processor;
		}
		if ( $reference ) {
			$filters[] = " $subscription_table.reference LIKE '%%%s%%' ";
			$filter_params[] = $reference;
		}
		if ( $status ) {
			$filters[] = " $subscription_table.status LIKE %s ";
			$filter_params[] = $status;
		}
		if ( $description ) {
			$filters[] = " $subscription_table.description LIKE '%%%s%%' ";
			$filter_params[] = $description;
		}

		$filters = " WHERE " . implode( " AND ", $filters );

		$count_query = $wpdb->prepare(
			"SELECT COUNT(*) FROM $subscription_table
			LEFT JOIN $wpdb->users ON $subscription_table.user_id = $wpdb->users.ID
			$filters",
			$filter_params
		);
		$count  = $wpdb->get_var( $count_query );
		if ( $count > $row_count ) {
			$paginate = true;
		} else {
			$paginate = false;
		}
		$pages = ceil ( $count / $row_count );
		if ( $paged > $pages ) {
			$paged = $pages;
		}
		if ( $paged != 0 ) {
			$offset = ( $paged - 1 ) * $row_count;
		}

		$query = $wpdb->prepare(
			"SELECT * FROM $subscription_table
			LEFT JOIN $wpdb->users ON $subscription_table.user_id = $wpdb->users.ID
			$filters
			ORDER BY $orderby $order
			LIMIT $row_count OFFSET $offset",
			$filter_params
		);

		$results = $wpdb->get_results( $query, OBJECT );

		$column_display_names = array(
			'subscription_id'  => __( 'Id', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'user_login'          => __( 'User', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'processor'        => __( 'Processor', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'reference'        => __( 'Reference', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'status'           => __( 'Status', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'from_datetime'    => __( 'From', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'thru_datetime'    => __( 'Until', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'last_payment'     => __( 'Last payment', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'dates'            => __( 'Dates', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'frequency'        => __( 'Frequency', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'frequency_uom'    => __( 'UOM', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'total_count'      => __( 'Total Count', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'current_count'    => __( 'Current Count', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'cycle'            => __( 'Cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'description'      => __( 'Description', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'groups'           => __( 'Groups', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
			'order'            => __( 'Order', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ),
// 			'edit'             => __( '&nbsp;', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), // @todo 'edit' heading
// 			'remove'           => __( '&nbsp;', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) // @todo 'remove' heading
		);

		$output .= '<div class="groups-subscriptions-overview">';

		$output .=
		'<div class="filters">' .
		'<label class="description" for="setfilters">' . __( 'Filters', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<form id="setfilters" action="" method="post">' .
		
		'<p>' .
		
		'<label class="subscription-id filter" for="subscription_id">' . __( 'Subscription Id', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<input class="subscription-id filter" name="subscription_id" type="text" value="' . esc_attr( $subscription_id ) . '"/>' .
		
		'<label class="user-id filter" for="user">' . __( 'User', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<input class="user-id filter" name="user" type="text" value="' . esc_attr( $user ) . '"/>' .
		
		'<label class="processor filter" for="processor">' . __( 'Processor', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<input class="processor filter" name="processor" type="text" value="' . $processor . '"/>' .

		'<label class="reference filter" for="reference">' . __( 'Reference', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<input class="reference filter" name="reference" type="text" value="' . $reference . '"/>' .
		
		'<label class="status filter" for="status">' . __( 'Status', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<input class="status filter" name="status" type="text" value="' . $status . '"/>' .
		
		'<label class="descr filter" for="description">' . __( 'Description', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<input class="descr filter" name="description" type="text" value="' . $description . '"/>' .

		'</p>' .

		'<p>' .
		wp_nonce_field( 'admin', self::FILTER_NONCE, true, false ) .
		'<input class="button" type="submit" value="' . __( 'Apply', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '"/>' .
		'<input class="button" type="submit" name="clear_filters" value="' . __( 'Clear', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '"/>' .
		'<input type="hidden" value="submitted" name="submitted"/>' .
		'</p>' .
		'</form>' .
		'</div>';

		$output .= '
		<div class="page-options">
		<form id="setrowcount" action="" method="post">
		<div>
		<label for="row_count">' . __( 'Results per page', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</label>' .
		'<input name="row_count" type="text" size="2" value="' . esc_attr( $row_count ) .'" />
		' . wp_nonce_field( 'admin', self::NONCE_1, true, false ) . '
		<input class="button" type="submit" value="' . __( 'Apply', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '"/>
		</div>
		</form>
		</div>
		';
	
		if ( $paginate ) {
			require_once( GROUPS_CORE_LIB . '/class-groups-pagination.php' );
			$pagination = new Groups_Pagination( $count, null, $row_count );
			$output .= '<form id="posts-filter" method="post" action="">';
			$output .= '<div>';
			$output .= wp_nonce_field( 'admin', self::NONCE_2, true, false );
			$output .= '</div>';
			$output .= '<div class="tablenav top">';
			$output .= $pagination->pagination( 'top' );
			$output .= '</div>';
			$output .= '</form>';
		}

		$output .= '<form id="subscriptions-action" method="post" action="">';

// 		$output .= '<div class="tablenav top">';
// 		$output .= '<div class="alignleft">';
// 		$output .= __( 'Apply to selected subscriptions:', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
// 		$output .= '<input class="button" type="submit" name="activate" value="' . __( 'Activate', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '"/>';
// 		$output .= '<input class="button" type="submit" name="cancel" value="' . __( 'Cancel', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '"/>';
// 		$output .= wp_nonce_field( 'admin', self::ACTION_NONCE, true, false );
// 		$output .= '<input type="hidden" name="action" value="subscriptions-action"/>';
// 		$output .= '</div>'; // .alignleft
// 		$output .= '</div>'; // .tablenav.top
	
		$output .= '
		<table id="" class="wp-list-table widefat fixed" cellspacing="0">
		<thead>
		<tr>
		';
	
		$output .= '<th id="cb" class="manage-column column-cb check-column" scope="col"><input type="checkbox"></th>';
	
		foreach ( $column_display_names as $key => $column_display_name ) {
			$options = array(
				'orderby' => $key,
				'order' => $switch_order
			);
			$class = $key;
			if ( !in_array($key, array( 'dates', 'cycle', 'groups', 'order', 'edit', 'remove' ) ) ) {
				if ( strcmp( $key, $orderby ) == 0 ) {
					$lorder = strtolower( $order );
					$class = "$key manage-column sorted $lorder";
				} else {
					$class = "$key manage-column sortable";
				}
				$column_display_name = '<a href="' . esc_url( add_query_arg( $options, $current_url ) ) . '"><span>' . $column_display_name . '</span><span class="sorting-indicator"></span></a>';
			}
			$output .= "<th scope='col' class='$class'>$column_display_name</th>";
		}
	
		$output .= '</tr>
		</thead>
		<tbody>
		';
	
		if ( count( $results ) > 0 ) {
			for ( $i = 0; $i < count( $results ); $i++ ) {

				$result = $results[$i];
				
				$subscription = new Groups_Subscription( $result->subscription_id );

				$output .= '<tr class="' . ( $i % 2 == 0 ? 'even' : 'odd' ) . '">';

				$output .= '<th class="check-column">';
				$output .= '<input type="checkbox" value="' . esc_attr( $result->subscription_id ) . '" name="subscription_ids[]"/>';
				$output .= '</th>';

				$output .= "<td class='subscription-id'>" . $result->subscription_id . "</td>";
				$userdata = get_user_by( 'id', $result->user_id );
				$output .= "<td class='user-id'>" . stripslashes( wp_filter_nohtml_kses( isset( $userdata->user_login ) ? $userdata->user_login : '' ) ) . "</td>";
				$output .= "<td class='processor'>" . stripslashes( wp_filter_nohtml_kses( $result->processor ) ) . "</td>";
				$output .= "<td class='reference'>" . stripslashes( wp_filter_nohtml_kses( $result->reference ) ) . "</td>";
				$output .= "<td class='status'>" . stripslashes( wp_filter_nohtml_kses( $result->status ) ) . "</td>";
// 				$output .= "<td class='from_datetime'>" . stripslashes( wp_filter_nohtml_kses( $result->from_datetime ) ) . "</td>";
// 				$output .= "<td class='thru_datetime'>" . stripslashes( wp_filter_nohtml_kses( $result->thru_datetime ) ) . "</td>";
// 				$output .= "<td class='last_payment'>" . stripslashes( wp_filter_nohtml_kses( $result->last_payment ) ) . "</td>";
				$output .= "<td class='dates'>";
				$output .= '<ul>';
				$output .= '<li>' . sprintf( __( 'From %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->from_datetime ) . '</li>';
				//if ( $result->thru_datetime ) {
// 				$output .= '<li>' . sprintf( __( 'Until %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->thru_datetime ) . '</li>';

				if ( intval( $result->frequency ) < Groups_Subscription::WAY_AHEAD ) { // only if it's not a one-time payment
					if ( $thru_timestamp = Groups_Subscription_Handler::get_end_of_subscription_timestamp( $result->subscription_id ) ) {
						// BTW, date() takes *very* long to execute if the frequency is high
						$output .= '<li>' . sprintf( __( 'Until %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), date( 'Y-m-d H:i:s', $thru_timestamp ) ) . '</li>';
					}
				}
				if ( $result->last_payment ) {
					$output .= '<li>' . sprintf( __( 'Last payment %s', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->last_payment ) . '</li>';
				}
				$output .= '</ul>';
				$output .= "</td>";
// 				$output .= "<td class='frequency'>" . stripslashes( wp_filter_nohtml_kses( $result->frequency ) ) . "</td>";
// 				$output .= "<td class='frequency_uom'>" . stripslashes( wp_filter_nohtml_kses( $result->frequency_uom ) ) . "</td>";
// 				$output .= "<td class='total_count'>" . stripslashes( wp_filter_nohtml_kses( $result->total_count ) ) . "</td>";
// 				$output .= "<td class='current_count'>" . stripslashes( wp_filter_nohtml_kses( $result->current_count ) ) . "</td>";

				$output .= "<td class='cycle'>";
				$cycle = '';
				if ( $result->status == Groups_Subscription::STATUS_ACTIVE ) {
					$current_cycle = $subscription->current_cycle;
					$current_cycle_tooltip_label = __( 'Current cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
				} else if ( $result->status == Groups_Subscription::STATUS_COMPLETED ) {
					$current_cycle = min( array( $result->current_count, $subscription->current_cycle ) );
					$current_cycle_tooltip_label = __( 'Last cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
				} else {
					$current_cycle = min( array( $result->current_count + 1, $subscription->current_cycle ) );
					$current_cycle_tooltip_label = __( 'Last cycle', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
				}
				if ( !$result->total_count ) {
					switch( $result->frequency_uom ) {
						case Groups_Subscription::DAY :
							$t = _n( 'every day', 'every %d days', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::WEEK :
							$t = _n( 'every week', 'every %d weeks', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::YEAR :
							$t = _n( 'every year', 'every %d years', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						default : // including Groups_Subscription::MONTH
							$t = _n( 'every month', 'every %d months', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
					}
					// far away in the future ... ?
					if ( intval( $result->frequency ) >= Groups_Subscription::WAY_AHEAD ) {
						$t = '&infin;';
					}
					$cycle = sprintf( $t, $result->frequency );
					$cycle .= '<br/>';
					$cycle .= sprintf( '<span class="tooltip" title="%s">#%d</span> / <span class="tooltip" title="%s">#%d</span>', __( 'Payments received', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->current_count, $current_cycle_tooltip_label, $current_cycle );
				} else {
					switch( $result->frequency_uom ) {
						case Groups_Subscription::DAY :
							$t = _n( 'every day for %2$d days', 'every %1$d days for %2$d days', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::WEEK :
							$t = _n( 'every week for %2$d weeks', 'every %1$d weeks for %2$d weeks', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						case Groups_Subscription::YEAR :
							$t = _n( 'every year for %2$d years', 'every %1$d years for %2$d years', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
							break;
						default : // including Groups_Subscription::MONTH
							$t = _n( 'every month for %2$d months', 'every %1$d months for %2$d months', $result->frequency, GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN );
					}
					$cycle = sprintf( $t, $result->frequency, $result->frequency * $result->total_count );
					$cycle .= '<br/>';
					$cycle .= sprintf( '<span class="tooltip" title="%s">#%d</span> / <span class="tooltip" title="%s">#%d</span>', __( 'Payments received', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $result->current_count, $current_cycle_tooltip_label, $current_cycle );
				}
				$output .= apply_filters( 'groups_subscriptions_admin_render_subscription_cycle', $cycle, $result->subscription_id );
				$output .= "</td>";
				
				$output .= "<td class='description'>" . stripslashes( wp_filter_nohtml_kses( $result->description ) ) . "</td>";
				
				$output .= "<td class='groups'>";
				
				$output .= '<ul>';
				foreach( $subscription->groups as $group ) {
					$output .= '<li>' . wp_filter_nohtml_kses( $group->name ) . '</li>';
				}
				$output .= '</ul>';
				$output .= "</td>";
				
				if ( $order_item = $subscription->order_item ) {
					$output .= "<td class='order'>";
					$output .= apply_filters( 'groups_subscriptions_admin_render_subscription_order_item', sprintf( __( 'Order %d Item %d', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ), $order_item->order_id, $order_item->item_id ), $result->subscription_id );
					$output .= "</td>";
				} else {
					$output .= "<td class='order'>&nbsp;</td>";
				}

				// @todo edit subscription
// 				$output .= "<td class='edit'>";
// 				$output .= "<a href='" . esc_url( add_query_arg( 'paged', $paged, $current_url ) ) . "&action=edit&subscription_id=" . $result->subscription_id . "' title='" . __( 'Edit', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN) . "' alt='" . __( 'Edit', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN) . "'><img src='". GROUPS_SUBSCRIPTIONS_PLUGIN_URL ."images/edit.png'/></a>";
// 				$output .= "</td>";

				// @todo remove subscription
// 				$output .= "<td class='remove'>";
// 				$output .= "<a href='" . esc_url( $current_url ) . "&action=remove&subscription_id=" . $result->subscription_id . "' title='" . __( 'Remove', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN) . "' alt='" . __( 'Remove', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN) . "'><img src='". GROUPS_SUBSCRIPTIONS_PLUGIN_URL ."images/remove.png'/></a>";
// 				$output .= "</td>";

				$output .= '</tr>';
			}
		} else {
			$output .= '<tr><td colspan="10">' . __( 'There are no results.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) . '</td></tr>';
		}
	
		$output .= '</tbody>';
		$output .= '</table>';
	
		$output .= '</form>'; // #subscriptions-action
			
		if ( $paginate ) {
			require_once( GROUPS_CORE_LIB . '/class-groups-pagination.php' );
			$pagination = new Groups_Pagination($count, null, $row_count);
			$output .= '<div class="tablenav bottom">';
			$output .= $pagination->pagination( 'bottom' );
			$output .= '</div>';
		}
	
		$output .= '</div>'; // .groups-subscriptions-overview
		$output .= '</div>'; // .manage-groups-subscriptions
	
		echo $output;
		Groups_Help::footer();
	}
	
}

