<?php
/**
 * class-groups-subscriptions-admin.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 */

/**
 * Admin section for Groups Subscriptions.
 */
class Groups_Subscriptions_Admin {

	const NONCE = 'groups-subscriptions-admin-nonce';

	/**
	 * Admin setup.
	 */
	public static function init() {
		add_action( 'admin_init', array( __CLASS__, 'admin_init' ) );
		add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ), 100 );
		if ( get_option( 'groups_subscriptions_show_in_user_profile', true ) ) {
			include_once( GROUPS_SUBSCRIPTIONS_ADMIN_LIB . '/class-groups-subscriptions-admin-user-profile.php' );
		}
	}

	/**
	 * Admin CSS.
	 */
	public static function admin_init() {
		wp_register_style( 'groups_subscriptions_admin', GROUPS_SUBSCRIPTIONS_PLUGIN_URL . 'css/groups_subscriptions_admin.css', array(), GROUPS_SUBSCRIPTIONS_CORE_VERSION );
	}

	/**
	 * Adds the admin section.
	 */
	public static function admin_menu() {
		$admin_page = add_submenu_page(
			'groups-admin',
			__( 'Subscriptions' ),
			__( 'Subscriptions' ),
			GROUPS_ADMINISTER_OPTIONS,
			'groups_subscriptions',
			array( __CLASS__, 'groups_subscriptions' )
		);
// 		add_action( 'admin_print_scripts-' . $admin_page, array( __CLASS__, 'admin_print_scripts' ) );
		add_action( 'admin_print_styles-' . $admin_page, array( __CLASS__, 'admin_print_styles' ) );
		
		$admin_page = add_submenu_page(
			'groups-admin',
			__( 'Subscription Settings' ),
			__( 'Subscription Settings' ),
			GROUPS_ADMINISTER_OPTIONS,
			'groups_subscriptions_settings',
			array( __CLASS__, 'settings' )
		);
		// 		add_action( 'admin_print_scripts-' . $admin_page, array( __CLASS__, 'admin_print_scripts' ) );
		add_action( 'admin_print_styles-' . $admin_page, array( __CLASS__, 'admin_print_styles' ) );
	}
	
	/**
	 * Admin styles.
	 */
	public static function admin_print_styles() {
		wp_enqueue_style( 'groups_admin' );
		wp_enqueue_style( 'groups_subscriptions_admin' );
	}
	
	/**
	 * Loads scripts.
	 */
	public static function admin_print_scripts() {
		wp_enqueue_script( 'groups-subscriptions', GROUPS_SUBSCRIPTIONS_PLUGIN_URL . 'js/groups-subscriptions.js', array(), GROUPS_SUBSCRIPTIONS_CORE_VERSION );
	}

	/**
	 * Renders the admin section.
	 */
	public static function groups_subscriptions() {
		if ( !current_user_can( GROUPS_ADMINISTER_OPTIONS ) ) {
			wp_die( __( 'Access denied.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
		}
		require_once( GROUPS_SUBSCRIPTIONS_ADMIN_LIB . '/class-groups-subscriptions-admin-subscriptions.php' );
		Groups_Subscriptions_Admin_Subscriptions::view();

	}
	
	public static function settings() {
		if ( !current_user_can( GROUPS_ADMINISTER_OPTIONS ) ) {
			wp_die( __( 'Access denied.', GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN ) );
		}
		require_once( GROUPS_SUBSCRIPTIONS_ADMIN_LIB . '/class-groups-subscriptions-admin-settings.php' );
		Groups_Subscriptions_Admin_Settings::view();
	}
}
Groups_Subscriptions_Admin::init();
