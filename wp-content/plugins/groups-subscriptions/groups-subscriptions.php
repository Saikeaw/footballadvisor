<?php
/**
 * groups-subscriptions.php
 *
 * Copyright (c) 2012 - 2015 "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-subscriptions
 * @since groups-subscriptions 1.0.0
 *
 * Plugin Name: Groups Subscriptions
 * Plugin URI: http://www.itthinx.com/plugins/groups-subscriptions
 * Description: Groups Subscriptions is a subscription framework for Groups.
 * Version: 2.4.0
 * Author: itthinx
 * Author URI: http://www.itthinx.com
 * Donate-Link: http://www.itthinx.com
 * License: GPLv3
 */
define( 'GROUPS_SUBSCRIPTIONS_CORE_VERSION', '2.4.0' );
if ( !function_exists( 'itthinx_plugins' ) ) {
	require_once 'itthinx/itthinx.php';
}
itthinx_plugins( __FILE__ );
define( 'GROUPS_SUBSCRIPTIONS_PLUGIN_DOMAIN', 'groups' );
define( 'GROUPS_SUBSCRIPTIONS_FILE', __FILE__ );

define( 'GROUPS_SUBSCRIPTIONS_PLUGIN_URL', plugin_dir_url( GROUPS_SUBSCRIPTIONS_FILE ) );

if ( !defined( 'GROUPS_SUBSCRIPTIONS_CORE_URL' ) ) {
	define( 'GROUPS_SUBSCRIPTIONS_CORE_URL', plugins_url( 'groups-subscriptions' ) );
}
if ( !defined( 'GROUPS_SUBSCRIPTIONS_CORE_DIR' ) ) {
	define( 'GROUPS_SUBSCRIPTIONS_CORE_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
}
if ( !defined( 'GROUPS_SUBSCRIPTIONS_CORE_LIB' ) ) {
	define( 'GROUPS_SUBSCRIPTIONS_CORE_LIB', GROUPS_SUBSCRIPTIONS_CORE_DIR . '/lib/core' );
}
if ( !defined( 'GROUPS_SUBSCRIPTIONS_ADMIN_LIB' ) ) {
	define( 'GROUPS_SUBSCRIPTIONS_ADMIN_LIB', GROUPS_SUBSCRIPTIONS_CORE_DIR . '/lib/admin' );
}
if ( !defined( 'GROUPS_SUBSCRIPTIONS_EXT_LIB' ) ) {
	define( 'GROUPS_SUBSCRIPTIONS_EXT_LIB', GROUPS_SUBSCRIPTIONS_CORE_DIR . '/lib/ext' );
}
if ( !defined( 'GROUPS_SUBSCRIPTIONS_LOG_LIB' ) ) {
	define( 'GROUPS_SUBSCRIPTIONS_LOG_LIB', GROUPS_SUBSCRIPTIONS_CORE_DIR . '/lib/log' );
}
if ( !defined( 'GROUPS_SUBSCRIPTIONS_VIEWS_LIB' ) ) {
	define( 'GROUPS_SUBSCRIPTIONS_VIEWS_LIB', GROUPS_SUBSCRIPTIONS_CORE_DIR . '/lib/views' );
}
require_once( GROUPS_SUBSCRIPTIONS_CORE_LIB . '/class-groups-subscriptions-controller.php');
