<?php
/**
 * groups-paypal.php
 * 
 * Copyright (c) 2012-2016 "kento" Karim Rahimpur www.itthinx.com
 * 
 * =============================================================================
 * 
 *                             LICENSE RESTRICTIONS
 * 
 *           This plugin is provided subject to the license granted.
 *              Unauthorized use and distribution is prohibited.
 *                     See COPYRIGHT.txt and LICENSE.txt.
 * 
 * This plugin relies on code that is NOT licensed under the GNU General
 * Public License. Files licensed under the GNU General Public License state
 * so explicitly in their header. 
 * 
 * =============================================================================
 * 
 * You MUST be granted a license by the copyright holder for those parts that
 * are not provided under the GPLv3 license.
 * 
 * If you have not been granted a license DO NOT USE this plugin until you have
 * BEEN GRANTED A LICENSE.
 * 
 * Use of this plugin without a granted license constitutes an act of COPYRIGHT
 * INFRINGEMENT and LICENSE VIOLATION and may result in legal action taken
 * against the offending party.
 * 
 * Being granted a license is GOOD because you will get support and contribute
 * to the development of useful free and premium themes and plugins that you
 * will be able to enjoy.
 * 
 * Thank you!
 * 
 * Visit www.itthinx.com for more information.
 * 
 * =============================================================================
 * 
 * This code is released under the GNU General Public License.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * This header and all notices must be kept intact.
 * 
 * @author Karim Rahimpur
 * @package groups-paypal
 * @since groups-paypal 1.0.0
 *
 * Plugin Name: Groups PayPal
 * Plugin URI: http://www.itthinx.com/plugins/groups-paypal
 * Description: Sell memberships and subscriptions with Groups and PayPal. 
 * Author: itthinx
 * Author URI: http://www.itthinx.com
 * Version: 1.3.1
 */

define( 'GROUPS_PAYPAL_VERSION', '1.3.1' );
if ( !function_exists( 'itthinx_plugins' ) ) {
	require_once 'itthinx/itthinx.php';
}
itthinx_plugins( __FILE__ );
define( 'GROUPS_PAYPAL_FILE', __FILE__ );
if ( !defined( 'GROUPS_PAYPAL_DIR' ) ) {
	define( 'GROUPS_PAYPAL_DIR', WP_PLUGIN_DIR . '/groups-paypal' );
}
if ( !defined( 'GROUPS_PAYPAL_LIB' ) ) {
	define( 'GROUPS_PAYPAL_LIB', GROUPS_PAYPAL_DIR . '/lib' );
}
if ( !defined( 'GROUPS_PAYPAL_ADMIN_LIB' ) ) {
	define( 'GROUPS_PAYPAL_ADMIN_LIB', GROUPS_PAYPAL_DIR . '/lib/admin' );
}
if ( !defined( 'GROUPS_PAYPAL_VIEW_LIB' ) ) {
	define( 'GROUPS_PAYPAL_VIEW_LIB', GROUPS_PAYPAL_DIR . '/lib/view' );
}

define( 'GROUPS_PAYPAL_PLUGIN_DOMAIN', 'groups-paypal' );
define( 'GROUPS_PAYPAL_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once( 'lib/class-groups-paypal.php' );
