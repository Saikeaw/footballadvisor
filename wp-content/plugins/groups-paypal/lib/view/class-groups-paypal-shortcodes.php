<?php
/**
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 */

/**
 * Shortcode handler.
 */
class Groups_PayPal_Shortcodes {

	const NONCE_ACTION = 'subscribe';
	const NONCE        = 'nonce-subscribe';

	/**
	 * Add shortcodes.
	 */
	public static function init() {
		add_filter( 'the_posts', array( __CLASS__, 'the_posts' ) );
		add_shortcode( 'groups_paypal_subscription', array( __CLASS__, 'groups_paypal_subscription' ) );
		add_shortcode( 'groups_paypal_return', array( __CLASS__, 'groups_paypal_return' ) );
	}

	/**
	 * Load scripts and styles when shortcodes are present.
	 * @param array $posts
	 * @return array
	 */
	public static function the_posts( $posts ) {

		if (
			//!wp_script_is( 'groups-paypal' ) ||
			!wp_style_is( 'groups-paypal' )
		) {
			foreach( $posts as $post ) {
				if ( strpos( $post->post_content, '[groups_paypal' ) !== false ) {
					//wp_enqueue_script( 'groups-paypal', GROUPS_PAYPAL_PLUGIN_URL . '/js/groups-paypal.js', array( 'jquery' ), GROUPS_PAYPAL_VERSION, true );
					wp_enqueue_style( 'groups-paypal', GROUPS_PAYPAL_PLUGIN_URL . '/css/groups-paypal.css', array(), GROUPS_PAYPAL_VERSION );
					break;
				}
			}
		}

		return $posts;
	}

	/**
	 * Shortcode renderer.
	 * @param array $atts
	 * @param string $content
	 * @return string HTML
	 */
	public static function groups_paypal_subscription( $atts, $content = null ) {

		global $post;

		$output = '';
		extract( shortcode_atts(
			array(
				'id'                   => null,
				'button_label'         => null,
				'payment_button_label' => null,
				'show_title'           => true,
				'show_price'           => true,
				'show_notice'          => true
			),
			$atts
		) );

		$options = get_option( Groups_PayPal::PLUGIN_OPTIONS , array() );

		$sandbox           = isset( $options[Groups_PayPal::SANDBOX] ) ? $options[Groups_PayPal::SANDBOX] : Groups_PayPal::SANDBOX_DEFAULT;
		$receiver_email    = Groups_PayPal::get_receiver_email();

		$secure_urls       = isset( $options[Groups_PayPal::SECURE_URLS] ) ? $options[Groups_PayPal::SECURE_URLS] : Groups_PayPal::SECURE_URLS_DEFAULT;
		$return_url        = isset( $options[Groups_PayPal::RETURN_URL] ) ? esc_attr( wp_filter_nohtml_kses( $options[Groups_PayPal::RETURN_URL] ) ) : '';
		$cancel_url        = isset( $options[Groups_PayPal::CANCEL_URL] ) ? esc_attr( wp_filter_nohtml_kses( $options[Groups_PayPal::CANCEL_URL] ) ) : '';

		$no_note           = isset( $options[Groups_PayPal::NO_NOTE] ) ? $options[Groups_PayPal::NO_NOTE] : Groups_PayPal::NO_NOTE_DEFAULT;
		$no_shipping       = isset( $options[Groups_PayPal::NO_SHIPPING] ) ? $options[Groups_PayPal::NO_SHIPPING] : Groups_PayPal::NO_SHIPPING_DEFAULT;
		$lc                = isset( $options[Groups_PayPal::LC] ) ? esc_attr( wp_filter_nohtml_kses( $options[Groups_PayPal::LC] ) ) : '';

		$charset           = get_bloginfo( 'charset' );

		if ( $id === null )  {
			if ( !empty( $post ) ) {
				$id = $post->ID;
			}

		}

		if ( !empty( $id ) ) {
			if ( $subscription_post = get_post( $id ) ) {
				if ( $subscription_post->post_type == 'subscription' ) {

					$nonce        = self::NONCE . '-' . $subscription_post->ID;
					$nonce_action = self::NONCE_ACTION . '-' . $subscription_post->ID;

					$setup_price        = get_post_meta( $id, '_setup_price', true );
					$subscription_price = get_post_meta( $id, '_subscription_price', true );
					$currency           = get_post_meta( $id, '_currency', true );
					$frequency          = get_post_meta( $id, '_frequency', true );
					$frequency_uom      = get_post_meta( $id, '_frequency_uom', true );
					switch( $frequency_uom ) {
						case Groups_Subscription::DAY :
							$t = 'D';
							break;
						case Groups_Subscription::WEEK :
							$t = 'W';
							break;
						case Groups_Subscription::MONTH :
							$t = 'M';
							break;
						case Groups_Subscription::YEAR :
							$t = 'Y';
							break;
						default :
							$t = 'M';
					}
					$total_count = intval( get_post_meta( $id, '_total_count', true ) );

					if ( empty( $_POST['submit'] ) || ( isset( $_POST['id'] ) && intval( $_POST['id'] ) !== $subscription_post->ID ) ) {

						if ( $button_label === null ) {
							$button_label = __( 'Subscribe', GROUPS_PAYPAL_PLUGIN_DOMAIN );
						}

						$output .= sprintf( '<div class="order-form paypal subscription-%d">', $subscription_post->ID );

						if ( $show_title ) {
							$output .= '<div class="title">';
							$output .= get_the_title( $subscription_post->ID );
							$output .= '</div>';
						}

						if ( $show_price ) {
							$output .= '<div class="price">';
							$output .= Groups_Subscription_Post_Type::get_price_html( $id );
							$output .= '</div>';
						}

						$output .= '<div class="form">';
						$output .= '<form action="" method="post">';
						$output .= '<div class="fields">';
						$output .= sprintf( '<input type="hidden" name="id" value="%s" />', esc_attr( $subscription_post->ID ) );
						$output .= wp_nonce_field( $nonce_action, $nonce, true, false );
						$output .= sprintf( '<input type="submit" name="submit" value="%s" />', esc_attr( $button_label ) );
						$output .= '</div>'; // .fields
						$output .= '</form>';
						$output .= '</div>'; // .form

						$output .= '</div>'; // .order-form

					} else if ( isset( $_POST['id'] ) && intval( $_POST['id'] ) === $subscription_post->ID ) {
						if ( wp_verify_nonce( $_POST[$nonce], $nonce_action ) ) {

							if ( ! is_user_logged_in() ) {

								if ( get_option( 'users_can_register') ) {
									$redirect_to   = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
									$register_link = apply_filters( 'register', site_url( sprintf( 'wp-login.php?action=register&redirect_to=%s', urlencode( $redirect_to ) ), 'login' ) );
									$output .= apply_filters(
										'groups_paypal_request_log_in_or_register',
										'<div class="log-in-or-register">' .
										sprintf(
											__( 'Please log in or <a href="%s">register</a> to subscribe to <span class="subscription-title">%s</span>.' ),
											$register_link,
											get_the_title( $subscription_post->ID )
										) .
										'</div>'
									);
								} else {
									$output .= apply_filters(
										'groups_paypal_request_log_in',
										'<div class="log-in">' .
										sprintf(
											__( 'Please log in to subscribe to <span class="subscription-title">%s</span>.' ),
											get_the_title( $subscription_post->ID )
										) .
										'</div>'
									);
								}

								$output .= '<div class="login-form">';
								$output .= wp_login_form( array( 'echo' => false ) );
								$output .= '</div>';

							} else {

								// create a new subscription
								$map = array(
									'user_id'     => get_current_user_id(),
									'description' => get_the_title( $subscription_post->ID ),
								);
								if ( $subscription_price) {
									$map['frequency']     = $frequency;
									$map['frequency_uom'] = $frequency_uom;
								} else {
									// subscription is not supposed to expire, assure that
									// the next payment cycle is far away
									$map['frequency']     = PHP_INT_MAX;
									$map['frequency_uom'] = Groups_Subscription::YEAR;
								}
								if ( $total_count ) {
									$map['total_count'] = $total_count;
								}
								$subscription_id = Groups_Subscription::create( $map );

								$variables = null;
								if ( $subscription_id ) {

									// Register the subscription order item.
									// The intent is to be able to trace the subscription
									// back to the product, the product in our case is the
									// subscription post and our order id is that of the
									// subscription:
									// order id = subscription id
									// item it = subscription post id
									Groups_Subscription_Order_Item::create( array(
										'subscription_id' => Groups_Utility::id( $subscription_id ),
										'order_id'        => Groups_Utility::id( $subscription_id ),
										'item_id'         => Groups_Utility::id( $subscription_post->ID )
									) );

									// add product groups to subscription
									$product_groups = get_post_meta( $subscription_post->ID, '_groups_groups', false );
									foreach( $product_groups as $group_id ) {
										Groups_Group_Subscription::create( array(
											'subscription_id' => Groups_Utility::id( $subscription_id ),
											'group_id'        => Groups_Utility::id( $group_id )
										) );
									}

									$subscription = new Groups_Subscription( $subscription_id );

									$variables = array(
										'business'      => $receiver_email,
										'item_name'     => get_the_title( $subscription_post->ID ),
										'currency_code' => $currency,
										'custom'        => $subscription->subscription_id,
										'no_note'       => $no_note,
										'no_shipping'   => $no_shipping,
										'notify_url'    => Groups_PayPal::get_notify_url(),
										'rm'            => 2 // return uses POST and includes all payment variables
									);

									if ( $subscription_price ) {

										$variables['cmd'] = '_xclick-subscriptions';

										if ( $total_count === 1 ) {
											$variables['a3'] = $setup_price + $subscription_price;
											$variables['p3'] = $frequency;
											$variables['t3'] = $t;
										} else if ( $total_count === 2 ) {
											$variables['a1'] = $setup_price + $subscription_price;
											$variables['p1'] = 1;
											$variables['t1'] = $t;
											$variables['a3'] = $subscription_price;
											$variables['p3'] = $frequency;
											$variables['t3'] = $t;
										} else {
											if ( $setup_price > 0 ) {
												$variables['a1'] = $setup_price + $subscription_price;
												$variables['p1'] = 1;
												$variables['t1'] = $t;
												if ( $total_count ) {
													$total_count--;
												}
											}
											$variables['a3'] = $subscription_price;
											$variables['p3'] = $frequency;
											$variables['t3'] = $t;
											$variables['src'] = 1;
											if ( $total_count ) { // if 0, that's because it's an open subscription; we don't get here if it was 1 or 2
												$variables['srt'] = $total_count;
											}
										}

									} else {

										$variables['cmd']    = '_xclick';
										$variables['amount'] = $setup_price;

									}

									if ( !empty( $return_url ) ) {
										$variables['return'] = $return_url;
									}

									if ( !empty( $cancel_url ) ) {
										$variables['cancel_return'] = $cancel_url;
									}

									if ( !empty( $lc ) ) {
										$variables['lc'] = $lc;
									}

								}

								if ( $payment_button_label === null ) {
									$payment_button_label = __( 'Checkout', GROUPS_PAYPAL_PLUGIN_DOMAIN );
								}

								if ( $variables ) {

									$output .= sprintf( '<div class="checkout-form paypal subscription-%d">', $subscription_post->ID );

									if ( $show_notice ) {
										$output .= '<div class="notice">';
										$output .= __( 'To activate your subscription please proceed to checkout:', GROUPS_PAYPAL_PLUGIN_DOMAIN );
										$output .= '</div>';
									}

									if ( $show_title ) {
										$output .= '<div class="title">';
										$output .= get_the_title( $subscription_post->ID );
										$output .= '</div>';
									}

									if ( $show_price ) {
										$output .= '<div class="price">';
										$output .= Groups_Subscription_Post_Type::get_price_html( $id );
										$output .= '</div>';
									}

									$output .= '<div class="form">';
									$output .= sprintf( '<form action="%s" method="post">', Groups_PayPal::get_paypal_url() );
									$output .= '<div class="fields">';
									foreach( $variables as $key => $value ) {
										if ( $value !== null ) {
											$output .= sprintf(
												'<input type="hidden" name="%s" value="%s" />',
												esc_attr( $key ),
												esc_attr( $value )
											);
										}
									}
									$output .= sprintf( '<input type="submit" name="submit" value="%s" />', esc_attr( $payment_button_label ) );
									$output .= '</div>'; // .fields
									$output .= '</form>';
									$output .= '</div>'; // .form

									$output .= '</div>'; // .checkout-form
								}
							}

						}
					}

				} else {
					$output .= '<div class="error">' . __( 'ERROR : Not a subscription.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</div>';
				}
			} else {
				$output .= '<div class="error">' . __( 'ERROR : Invalid subscription id.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</div>';
			}
		} else {
			$output .= '<div class="error">' . __( 'ERROR : Missing subscription id.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</div>';
		}
		return $output;
	}

	/**
	 * Shortcode renderer for return URL after payment.
	 * @param array $atts
	 * @param string $content
	 * @return string HTML
	 */
	public static function groups_paypal_return( $atts, $content = null ) {

		global $post;

		$output = '';

		extract( shortcode_atts(
			array(
				'show_message'     => true,
				'show_description' => true,
				'show_price'       => true,
				'show_groups'      => true
			),
			$atts
		) );

		$options = get_option( Groups_PayPal::PLUGIN_OPTIONS , array() );

		// Requires rm to be set to 2 (the buyer’s browser is redirected to the return URL by using the POST method, and all payment variables are included). 
		$subscription_id = isset( $_POST['custom'] ) ? $_POST['custom'] : null;
		if ( $subscription_id && Groups_Subscription::read( $subscription_id ) ) {
			$subscription = new Groups_Subscription( $subscription_id );
			if ( $subscription ) {
				if ( $subscription->user_id == get_current_user_id() ) {

					if ( $show_message ) {
						$output .= apply_filters(
							'groups_paypal_return_message',
							'<div class="message">' .
							sprintf(
								__( 'Thank you, your order for subscription #%d has been successfully processed.', GROUPS_PAYPAL_PLUGIN_DOMAIN ),
								$subscription->subscription_id
							) .
							'</div>',
							$subscription_id
						);
					}

					if ( $show_description ) {
						$output .= '<div class="description">';
						$output .= $subscription->description;
						$output .= '</div>';
					}

					if ( $show_price ) {
						global $wpdb;
						$groups_subscription_order_item_table = _groups_get_tablename( 'subscription_order_item' );
						$post_id = $wpdb->get_var( $wpdb->prepare(
							"SELECT item_id from $groups_subscription_order_item_table WHERE subscription_id = %d AND order_id = %d",
							Groups_Utility::id( $subscription_id ),
							Groups_Utility::id( $subscription_id )
						) );
						$output .= '<div class="price">';
						$output .= Groups_Subscription_Post_Type::get_price_html( $post_id );
						$output .= '</div>';
					}

					if ( $show_groups ) {
						if ( count( $subscription->groups ) > 0 ) {
							$output .= '<div class="groups">';
							$output .= __( 'The subscription grants membership to:', GROUPS_PAYPAL_PLUGIN_DOMAIN );
							$output .= '<ul>';
							foreach( $subscription->groups as $group ) {
								$output .= '<li>' . wp_filter_nohtml_kses( $group->name ) . '</li>';
							}
							$output .= '</ul>';
							$output .= '</div>';
						}
					}
				}
			}
		}

		echo $output;

	}

}
Groups_PayPal_Shortcodes::init();
