<?php
/** 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 */

/**
 * PayPal IPN Event Handler.
 */
class Groups_PayPal_Listener implements I_IX_PayPal_IPN_Event_Listener {

	private $receiver_email = null;

	/**
	 * Creates a new listener.
	 * 
	 * @param array $params must contain receiver_email
	 */
	public function __construct( $params ) {
		$this->receiver_email = isset( $params['receiver_email'] ) ? $params['receiver_email'] : null;
	}

	/**
	 * Handle notification.
	 * @param $event IX_PayPal_IPN_Event
	 * @see I_IX_PayPal_IPN_Event_Listener::notify()
	 */
	public function notify( IX_PayPal_IPN_Event $event ) {

		$post = $event->get_IPN_variables();

		if ( class_exists( 'Groups_Subscriptions_Log' ) ) {
			Groups_Subscriptions_Log::log( sprintf( '%s : event = %s', __METHOD__, var_export( $event, true ) ) );
		}

		$mc_currency     = isset( $post['mc_currency'] )    ? $post['mc_currency']    : null;
		$mc_gross        = isset( $post['mc_gross'] )       ? $post['mc_gross']       : null;
		$payment_status  = isset( $post['payment_status'] ) ? $post['payment_status'] : null;
		$receiver_email  = isset( $post['receiver_email'] ) ? $post['receiver_email'] : null;
		$txn_id          = isset( $post['txn_id'] )         ? $post['txn_id']         : null;

		$txn_type        = isset( $post['txn_type'] )       ? $post['txn_type']       : null;
		$reason_code     = isset( $post['reason_code'] )    ? $post['reason_code']    : null;

		// for refund, reversal, canceled reversal: parent_txn_id is the txn_id of the original transaction
		$parent_txn_id   = isset( $post['parent_txn_id'] )  ? $post['parent_txn_id']  : null;

		$subscr_id       = isset( $post['subscr_id'] )      ? $post['subscr_id']      : null;
		$payer_email     = isset( $post['payer_email'] )    ? $post['payer_email']    : null;

		$subscription_id = isset( $post['custom'] )         ? (int) $post['custom']   : null;

		if ( $subscription = Groups_Subscription::read( $subscription_id ) ) {

			switch ( $txn_type ) {

				//
				// One-time payments
				//

				case null : // Payment refund/reversal with parent_txn_id; credit card chargeback if the case_type variable contains chargeback
				case ''   :
					switch( $payment_status ) {
						case 'Refunded' :
						case 'Reversed' :
							// parent_txn_id & reason_code should be present
							if ( $parent_txn_id && $reason_code ) {
								$map = array(
									'subscription_id' => $subscription->subscription_id,
								);
								$map['status'] = Groups_Subscription::STATUS_CANCELLED; // @todo must use another status to distinguish from merely cancelled to actually refunded or reversed/chargeback
								Groups_Subscription::update( $map );
							}
							break;
					}
					break;

				case 'cart' : // Payment received for multiple items; source is Express Checkout or the PayPal Shopping Cart.
					// doesn't make much sense in our context  
					break;

				case 'express_checkout' : // Payment received for a single item; source is Express Checkout
					// @todo express checkout
					break;


				case 'web_accept' : // Payment received; source is a Buy Now, Donation, or Auction Smart Logos button
					if (
						$payment_status == 'Completed' &&
						strcasecmp( $receiver_email, $this->receiver_email ) === 0 // PayPal normalizes $receiver_email to lower case
					) {
						// register the processor and its subscription id
						$map = array(
							'subscription_id' => $subscription_id,
							'processor'       => 'paypal',
							'reference'       => $txn_id,
							'status'          => Groups_Subscription::STATUS_ACTIVE
						);
						Groups_Subscription::update( $map );
					}

					break;

				//
				// Subscriptions
				//

				case 'subscr_cancel' : // Subscription cancelled
					//$subscr_date = isset( $post['subscr_date'] ) ? $post['subscr_date'] : null;
					$map = array(
						'subscription_id' => $subscription->subscription_id,
					);
					$map['status'] = Groups_Subscription::STATUS_CANCELLED;
					Groups_Subscription::update( $map );
					break;

				case 'subscr_eot' : // Subscription expired
					// PayPal's IPN for this is unreliable as it sends this
					// IPN before the subscription actually expires. E.g.
					// on a one day subscription subscr_eot is received
					// before even the subscr_payment IPN is received.
					// We don't need it anyway as we're completing
					// subscriptions ourselves.
// 					Groups_Subscription::update( array(
// 						'subscription_id' => $subscription->subscription_id,
// 						'status' => Groups_Subscription::STATUS_EXPIRED
// 					) );
					break;

				case 'subscr_failed' : // Subscription signup failed
					// nothing to do
					break;

				case 'subscr_modify' : // Subscription modified
					//
					break;

				case 'subscr_payment' : // Subscription payment received
					$new_payment = true;
					// check if this payment transaction has not yet been registered
					$data = unserialize( $subscription->data );
					if ( !$data ) {
						$data = array();
					}
					if ( !isset( $data['payments'] ) ) {
						$data['payments'] = array();
					}
					if ( !isset( $data['payments']['paypal'] ) ) {
						$data['payments']['paypal'] = array();
					}
					if ( !isset( $data['payments']['paypal'][$txn_id] ) ) {
						$data['payments']['paypal'][$txn_id] = $post;
					} else {
						$new_payment = false;
					}
					if ( $new_payment ) {
						// register the processor, subscription id and last payment datetime
						$map = array(
							'subscription_id' => $subscription_id,
							'processor'       => 'paypal',
							'reference'       => $subscr_id,
							'current_count'   => $subscription->current_count + 1,
							'last_payment'    => date( 'Y-m-d H:i:s', time() ), // using incoming time on server, not the processor's payment reception date
							'data'            => serialize( $data )
						);
						Groups_Subscription::update( $map );
					}
					break;

				case 'subscr_signup' : // Subscription started => activate it
					// register the processor and its subscription id
					$map = array(
						'subscription_id' => $subscription_id,
						'processor'       => 'paypal',
						'reference'       => $subscr_id,
						'status'          => Groups_Subscription::STATUS_ACTIVE
					);
					Groups_Subscription::update( $map );
					break;
			} // switch

		}
	}

}
