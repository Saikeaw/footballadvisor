<?php
/** 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 */
// bootstrap WordPress
if ( !defined( 'ABSPATH' ) ) {
	$wp_load = 'wp-load.php';
	$max_depth = 100; // prevent death by depth
	while ( !file_exists( $wp_load ) && ( $max_depth > 0 ) ) {
		$wp_load = '../' . $wp_load;
		$max_depth--;
	}
	if ( file_exists( $wp_load ) ) {
		require_once $wp_load;
	}
}
if ( defined( 'ABSPATH' ) ) {
	require_once( 'ix-paypal-ipn/ix-paypal-ipn.php' );
	require_once( 'class-groups-paypal-listener.php' );

	$options        = get_option( Groups_PayPal::PLUGIN_OPTIONS , array() );
	$sandbox        = isset( $options[Groups_PayPal::SANDBOX] ) ? $options[Groups_PayPal::SANDBOX] : Groups_PayPal::SANDBOX_DEFAULT;
	$receiver_email = Groups_PayPal::get_receiver_email();

	$handler = new IX_PayPal_IPN_Handler();
	if ( $sandbox ) {
		$handler->set_mode( IX_PayPal_IPN_Handler::SANDBOX );
	}
	$listener = new Groups_PayPal_Listener( array( 'receiver_email' => $receiver_email ) );
	$handler->add_listener( $listener );
	$handler->handle_notification();
	$handler->remove_listener( $listener );
}
