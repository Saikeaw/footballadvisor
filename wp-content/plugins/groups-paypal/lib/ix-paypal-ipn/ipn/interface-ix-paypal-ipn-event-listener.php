<?php
/** 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is provided subject to the license granted. 
 * Unauthorized use and distribution is prohibited. 
 * See COPYRIGHT.txt and LICENSE.txt. 
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * This header and all notices must be kept intact.
 */

/** 
 * Event Listener Interface declaration.
 * 
 * Event listeners that need to be notified by the IX_PayPal_IPN_Handler must
 * implement this interface.
 */
interface I_IX_PayPal_IPN_Event_Listener {
	
	/**
	 * Handles the event.
	 * 
	 * @param IX_PayPal_IPN_Event $event
	 */
	public function notify( IX_PayPal_IPN_Event $event );
}