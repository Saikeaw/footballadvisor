<?php
/** 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is provided subject to the license granted. 
 * Unauthorized use and distribution is prohibited. 
 * See COPYRIGHT.txt and LICENSE.txt. 
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * This header and all notices must be kept intact.
 */

/** 
 * Itthinx PayPal IPN handler; IX_PayPal_IPN_Event factory.
 */
class IX_PayPal_IPN_Handler {
	
	const LIVE             = 'live';
	const SANDBOX          = 'sandbox';
	
	const LIVE_HOSTNAME    = 'www.paypal.com';
	const SANDBOX_HOSTNAME = 'www.sandbox.paypal.com';

	// result values
	const VERIFIED = 'VERIFIED';
	const INVALID  = 'INVALID';
	const UNKNOWN  = 'UNKNOWN';
	
	/**
	 * Holds attached listeners.
	 * 
	 * @var array of I_IX_PayPal_IPN_Listener objects
	 */
	public $listeners = array();
	
	private $mode = self::LIVE;
	private $ssl = true; // non-ssl won't work with sandbox
	private $curl = true;
	
	private $followlocation = false;
	private $max_redirects = 0; // not used with socket
	
	/**
	 * Hostname to connect to
	 * 
	 * @var string hostname to connect to
	 */
	private $hostname = self::LIVE_HOSTNAME;
	
	/**
	 * Path to verification on host.
	 * 
	 * @var string path
	 */
	private $path = '/cgi-bin/webscr'; 
	
	/**
	 * Port to connect to.
	 * 
	 * @var int port number
	 */
	private $port = 443;
	
	/**
	 * Connection timeout in seconds.
	 * 
	 * @var int seconds
	 */
	private $timeout = 30;
	
	/**
	 * Constructor.
	 * 
	 */
	public function __construct() {
		$this->set_mode( self::LIVE );
	}
	
	/**
	 * Set live or sandbox mode.
	 * 
	 * @param string $mode
	 * @param $hostname string (optional) URL to use
	 * @return string current mode
	 */
	public function set_mode( $mode, $hostname = null ) {
		switch( $mode ) {
			case self::LIVE :
				$this->mode = self::LIVE;
				if ( $hostname === null ) {
					$this->hostname = self::LIVE_HOSTNAME;
				} else {
					$this->hostname = $hostname;
				}
				break;

			case self::SANDBOX :
				$this->mode = self::SANDBOX;
				if ( $hostname === null ) {
					$this->hostname = self::SANDBOX_HOSTNAME;
				} else {
					$this->hostname = $hostname;
				}
				break;
		}
		return $this->mode;
	}
	
	/**
	 * Use SSL or not.
	 * 
	 * @param boolean $ssl true to use SSL
	 * @return boolean true if SSL will be used, otherwise false
	 */
	public function set_ssl( $ssl ) {
		switch ( $ssl ) {
			case true :
				$this->ssl = true;
				$this->port = 443;
				break;
			case false :
				$this->ssl = false;
				$this->port = 80;
				break;
		}
		return $this->ssl;
	}
	
	/**
	 * Use cURL or not.
	 * 
	 * @param boolean $curl true to use cURL if available
	 * @return true if cURL is available and will be used, otherwise false
	 */
	public function set_curl( $curl ) {
		if ( ( $curl === true ) && function_exists( 'curl_init' ) ) {
			$this->curl = true;
		} else {
			$this->curl = false;
		}
		return $this->curl;
	}
	
	/**
	 * Add an event listener.
	 * The listener is notified upon new events. 
	 * @param I_IX_PayPal_IPN_Listener listener
	 * @return true on success, false otherwise
	 */
	public function add_listener( I_IX_PayPal_IPN_Event_Listener $listener ) {
		$result = false;
		if ( !in_array( $listener, $this->listeners ) ) {
			$this->listeners[] = $listener;
			$result = true;
		}
		return $result;
	}
	
	/**
	 * Remove an event listener.
	 * 
	 * @param I_IX_PayPal_IPN_Listener $listener
	 * @return true if the listener was remove, false otherwise
	 */
	public function remove_listener( I_IX_PayPal_IPN_Event_Listener $listener ) {
		$result = false;
		$i = array_search( $listener, $this->listeners, true );
		if ( $i !== false ) {
			unset( $this->listeners[$i] );
			$result = true;
		}
		return $result;
	}
	
	/**
	 * Invokes notification handler that creates the event and sends it
	 * out to the attached listeners.
	 */
	public function handle_notification() {

		$IPN_variables = array();
		// PayPal IPN command precedes IPN POST data
		$content = 'cmd=_notify-validate';
		foreach ( $_POST as $key => $value ) {
			// Only using urlencode( $value ) when $value contains a single quote, for example first_name='John\\\'s'
			// will end up in INVALID result from IPN, applying stripslashes always resolves this issue:
// 			if( get_magic_quotes_gpc() ) {
// 				$value = urlencode( stripslashes( $value ) );
// 			} else {
// 				$value = urlencode( $value );
// 			}
			$value = urlencode( stripslashes( $value ) );
			$IPN_variables[$key] = $value;
			$content .= "&$key=$value";
		}
		
		if ( $this->curl ) {
			$response = $this->curl_verify_ipn( $content );
		} else {
			$response = $this->socket_verify_ipn( $content );
		}
		
		if ( IX_PAYPAL_DEBUG ) {
			IX_Log::info("response=$response" );
		}
		
		if ( $response === self::VERIFIED ) {
			$event = new IX_PayPal_IPN_Event();
			$event->set_IPN_variables( $IPN_variables );
			foreach ( $this->listeners as $listener ) {
				$listener->notify( $event );
			}
		}
	}
	
	/**
	* Handle notification verification through cURL.
	* POSTs the $content (IPN data) back to PayPal for validation,
	* obtains validation response and returns that.
	* @param $content string IPN data to POST
	* @return string "VERIFIED", "INVALID" or "UNKNOWN"
	*/
	public function curl_verify_ipn( $content ) {
		$result = null;
		$url = ( $this->ssl ? 'https://' : 'http://' ) . $this->hostname . $this->path;
		$h = curl_init( $url );
		if ( $h !== false ) {
			if (
				curl_setopt( $h, CURLOPT_HEADER,         true                  ) &&
				curl_setopt( $h, CURLOPT_POST,           true                  ) &&
				curl_setopt( $h, CURLOPT_POSTFIELDS,     $content              ) &&
				curl_setopt( $h, CURLOPT_TIMEOUT,        $this->timeout        ) &&
				curl_setopt( $h, CURLOPT_RETURNTRANSFER, true                  )
			) {
				if (
					!$this->followlocation ||
					curl_setopt( $h, CURLOPT_FOLLOWLOCATION, $this->followlocation ) &&
					curl_setopt( $h, CURLOPT_MAXREDIRS,      $this->max_redirects  )
				) {
					if ( defined( 'IX_PAYPAL_CURLOPT_SSL_VERIFYPEER' ) && !IX_PAYPAL_CURLOPT_SSL_VERIFYPEER ) {
						curl_setopt( $h, CURLOPT_SSL_VERIFYPEER, false );
					}
					$response = curl_exec( $h );
					if ( IX_PAYPAL_DEBUG ) {
						$curl_error = curl_error( $h );
						if ( !empty( $curl_error ) ) {
							IX_Log::error( $curl_error );
						}
					}
					if ( $response !== false ) {
						$http_code = curl_getinfo( $h, CURLINFO_HTTP_CODE );
						if ( intval( $http_code ) == 200 ) {
							$header_size = intval( curl_getinfo( $h, CURLINFO_HEADER_SIZE ) );
							$header = substr( $response, 0, $header_size );
							$response = substr( $response, $header_size );
							if ( strpos( $response, self::VERIFIED ) !== false ) {
								$result = self::VERIFIED;
							} else if ( strpos( $response, self::INVALID ) !== false ) {
								$result = self::INVALID;
							} else {
								$result = self::UNKNOWN;
							}
						} else {
							if ( IX_PAYPAL_DEBUG ) {
								IX_Log::error( "HTTP code: $http_code" );
							}
						}
					}
				}
			}
			curl_close( $h );
		}
		return $result;
	}
	
	/**
	 * Handle notification verification through socket.
	 * POSTs the $content (IPN data) back to PayPal for validation,
	 * obtains validation response and returns that.
	 * @param $content string IPN data to POST
	 * @return string "VERIFIED", "INVALID" or "UNKNOWN"
	 */
	public function socket_verify_ipn( $content ) {
		$result = null;
		$headers = "POST $this->path HTTP/1.0\r\n";
		$headers .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$headers .= "Content-Length: " . strlen( $content ) . "\r\n";
		$headers .= "Connection: close\r\n";
		$headers .= "\r\n";
		
		$hostname = ( $this->ssl ? 'ssl://' : '' ) . $this->hostname;
		$fp = fsockopen( $hostname, $this->port, $errno, $errstr, $this->timeout );

		if ( $fp ) {
			fputs( $fp, $headers . $content );
			$http_ok = true;
			$http_status = "";
			$response = "";
			while ( !feof( $fp ) ) {
				$response .= fgets( $fp, 1024 );
			}
			$lines = explode( "\n", $response );
			if ( strpos( $lines[0], "200") !== false ) {
				if ( strpos( $response, self::VERIFIED ) !== false ) {
					$result = self::VERIFIED;
				} else if ( strpos( $response, self::INVALID ) !== false ) {
					$result = self::INVALID;
				} else {
					$result = self::UNKNOWN;
				}
			} else {
				if ( IX_PAYPAL_DEBUG ) {
					IX_Log::error( "First HTTP header: " . $lines[0] );
				}
			}
			fclose( $fp );
		} else {
			if ( IX_PAYPAL_DEBUG ) {
				IX_Log::error( "fsockopen error # $errno message: $errstr" );
			}
		}
		return $result;
	}
}