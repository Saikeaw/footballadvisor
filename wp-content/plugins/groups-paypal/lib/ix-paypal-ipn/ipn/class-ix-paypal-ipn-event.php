<?php
/**
* Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
*
* This code is provided subject to the license granted.
* Unauthorized use and distribution is prohibited.
* See COPYRIGHT.txt and LICENSE.txt.
*
* This code is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* This header and all notices must be kept intact.
*/

/**
 * Event and IPN notification data container.
 * The IX_PayPal_IPN_Handler distributes an IX_PayPal_IPN_Event to
 * event listeners.
 */
class IX_PayPal_IPN_Event {
	
	/**
	 * Holds the notification variables. 
	 * @var array of key-value pairs from IPN notification data
	 */
	private $IPN_variables = array();
	
	/**
	 * Constructor.
	 */
	public function __construct() {
	}
	
	/**
	 * Set IPN variables.
	 * This is set by the handler.
	 * Values are urldecoded.
	 * @param $variables array
	 */
	public function set_IPN_variables( $variables ) {
		//$this->IPN_variables = $variables;
		foreach( $variables as $key => $value ) {
			$this->IPN_variables[$key] = urldecode( $value );
		}
	}
	
	/**
	 * Get IPN variables.
	 * Obtains notification data. 
	 */
	public function get_IPN_variables() {
		return $this->IPN_variables;
	}
}