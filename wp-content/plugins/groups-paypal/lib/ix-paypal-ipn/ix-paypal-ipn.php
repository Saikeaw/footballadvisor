<?php
/**
 * ============================================================================
 * Itthinx PayPal Framework - IPN Event Handling Framework
 * ============================================================================
 * 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is provided subject to the license granted.
 * 
 * Unauthorized use and distribution is prohibited.
 * 
 * See COPYRIGHT.txt and LICENSE.txt.
 * 
 * ============================================================================
 * 
 * You MUST be granted a license by the copyright holder to legitimately
 * use this framework.
 * 
 * If you have not been granted a license DO NOT USE this software until you
 * have BEEN GRANTED A LICENSE.
 * 
 * Use of this software without a granted license constitutes an act of
 * COPYRIGHT INFRINGEMENT and LICENSE VIOLATION and may result in legal action
 * taken against the offending party.
 * 
 * Being granted a license is GOOD because you will get support and contribute
 * to the development of useful software from www.itthinx.com, including free
 * and premium themes and plugins that you will be able to enjoy.
 * 
 * Thank you!
 * 
 * Visit www.itthinx.com for more information.
 * 
 * ============================================================================
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * This header and all notices must be kept intact.
 * 
 * Version: 1.0.1
 */
if ( !defined( 'IX_PAYPAL_DEBUG' ) ) {
	define( 'IX_PAYPAL_DEBUG', false );
}

if ( IX_PAYPAL_DEBUG ) {
	require_once( 'uty/class-ix-log.php' );
}
require_once( 'ipn/interface-ix-paypal-ipn-event-listener.php' );
require_once( 'ipn/class-ix-paypal-ipn-event.php' );
require_once( 'ipn/class-ix-paypal-ipn-handler.php' );