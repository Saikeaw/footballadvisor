<?php
/**
 * fake_bcmath.php
 * 
 * Every time you use any of the fake bcmath functions, a puppy dies.
 * So please don't do it and get yourself bcmath enabled.
 *
 * Copyright 2011 "kento" Karim Rahimpur - www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 */

if ( !function_exists( 'fake_bcadd' ) ) {

global $fake_bcmath_scale;
$fake_bcmath_scale = null;

function fake_bcadd( $left_operand, $right_operand, $scale = null ) {
	return fake_bcmath_scale( ( string ) ( doubleval( $left_operand ) + doubleval( $right_operand ) ), $scale );
}

function fake_bccomp( $left_operand, $right_operand, $scale = null ) {
	$left = doubleval( fake_bcmath_scale( $left_operand, $scale ) );
	$right = doubleval( fake_bcmath_scale( $right_operand, $scale ) );
	$cmp = 0;
	if ( $left < $right ) {
		$cmp = -1;
	} else if ( $left > $right ) {
		$cmp = 1;
	}
	return $cmp;
}

function fake_bcdiv( $left_operand, $right_operand, $scale = null ) {
	return fake_bcmath_scale( ( string ) ( doubleval( $left_operand ) / doubleval( $right_operand ) ), $scale );
}

function fake_bcmod( $left_operand, $modulus ) {
	return ( string ) ( intval( $left_operand ) % intval( $modulus ) );
}

function fake_bcmul( $left_operand, $right_operand, $scale = null ) {
	return fake_bcmath_scale( ( string ) ( doubleval( $left_operand ) * doubleval( $right_operand ) ), $scale );
}

function fake_bcpow( $left_operand, $right_operand, $scale = null ) {
	return fake_bcmath_scale( ( string ) ( pow( doubleval( $left_operand ), doubleval( $right_operand ) ) ), $scale );
}

function fake_bcpowmod( $left_operand , $right_operand , $modulus, $scale = null ) {
	if ( $modulus == 0 ) {
		$r = null;
	} else {
		$r = fake_bcmath_scale( ( string ) ( pow( doubleval( $left_operand ), doubleval( $right_operand ) ) % intval( $modulus ) ), $scale );
	}
	return $r;
} 

function fake_bcscale( $scale ) {
	global $fake_bcmath_scale;
	$s = intval( $scale );
	if ( $s >= 0 ) {
		$fake_bcmath_scale = $s;
		return true;
	} else {
		return false;
	}
}

function fake_bcsqrt( $operand, $scale = null ) {
	return fake_bcmath_scale( ( string ) sqrt( doubleval( $operand ) ), $scale );
}

function fake_bcsub( $left_operand, $right_operand, $scale = null ) {
	return fake_bcmath_scale( ( string ) ( doubleval( $left_operand ) - doubleval( $right_operand ) ), $scale );
}

function fake_bcmath_scale( $value, $scale = null ) {
	global $fake_bcmath_scale;
	$s = null;
	if ( $scale !== null ) {
		$s = intval( $scale );
	} else if ( $fake_bcmath_scale !== null ) {
		$s = intval( $fake_bcmath_scale );
	}
	if ( $s !== null ) {
		return ( string ) round( doubleval( $value ), $s );
	} else {
		return $value;
	}
}

//
// define missing bcmatch functions ... a terrible thing to do
//

if ( !function_exists( 'bcadd' ) ) {
		
	function bcadd( $left_operand, $right_operand, $scale = null ) {
		return fake_bcadd( $left_operand, $right_operand, $scale );
	}

	function bccomp( $left_operand, $right_operand, $scale = null ) {
		return fake_bccomp( $left_operand, $right_operand, $scale );
	}
	
	function bcdiv( $left_operand, $right_operand, $scale = null ) {
		return fake_bcdiv($left_operand, $right_operand, $scale );
	}
	
	function bcmod( $left_operand, $modulus ) {
		return fake_bcmod( $left_operand, $modulus );
	}
	
	function bcmul( $left_operand, $right_operand, $scale = null ) {
		return fake_bcmul( $left_operand, $right_operand, $scale );
	}
	
	function bcpow( $left_operand, $right_operand, $scale = null ) {
		return fake_bcpow($left_operand, $right_operand, $scale );
	}
	
	function bcpowmod( $left_operand , $right_operand , $modulus, $scale = null ) {
		return fake_bcpowmod($left_operand, $right_operand, $modulus, $scale );
	}
	
	function bcscale( $scale ) {
		return fake_bcscale( $scale );
	}
	
	function bcsqrt( $operand, $scale = null ) {
		return fake_bcsqrt( $operand, $scale );
	}
	
	function bcsub( $left_operand, $right_operand, $scale = null ) {
		return fake_bcsub( $left_operand, $right_operand, $scale );
	}
}

}
