<?php
/** 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is provided subject to the license granted. 
 * Unauthorized use and distribution is prohibited. 
 * See COPYRIGHT.txt and LICENSE.txt. 
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * This header and all notices must be kept intact.
 */
if ( !defined( 'IX_PAYPAL_LOG_ECHO' ) ) {
	define( 'IX_PAYPAL_LOG_ECHO', false );
}

if ( !class_exists( 'IX_Log' ) ) {

/**
 * Implements a log file printer.
 */
class IX_Log {
	
	// log prefixes
	const INFO    = 'INFO';
	const WARNING = 'WARNING';
	const ERROR   = 'ERROR';
	
	private static $logfile = 'log.txt';
	
	public static function info( $msg ) {
		self::log( $msg, self::INFO );
	}
	
	public static function warning( $msg ) {
		self::log( $msg, self::WARNINGs );
	}
	
	public static function error( $msg ) {
		self::log( $msg, self::ERROR );
	}
	
	public static function log( $message, $level = self::INFO ) {
		$now = date( 'Y-m-d h:i:s', time() );
		$msg = $now . ' ' . $level . ' ' . $message . "\r\n";
		error_log( $msg, 3, self::$logfile );
		if ( IX_PAYPAL_LOG_ECHO ) {
			echo htmlentities( $msg );
		}
	}
}

}