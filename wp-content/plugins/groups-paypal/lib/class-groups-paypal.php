<?php
/** 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 */

class Groups_PayPal {
	
	const PLUGIN_OPTIONS    = 'groups_paypal_ipn';
	const NONCE             = 'groups_paypal_ipn_admin_nonce';
	const SET_ADMIN_OPTIONS = 'set_admin_options';
	const DELETE_DATA       = 'delete_data';
	const DISMISS_USAGE     = 'dismiss_usage';
	const KEY               = 'key';

	// urls
	const SECURE_URLS         = 'secure_urls';
	const SECURE_URLS_DEFAULT = true;
	const RETURN_URL          = 'return_url';
	const CANCEL_URL          = 'cancel_url';
	const NO_NOTE             = 'no_note';
	const NO_NOTE_DEFAULT     = 1;
	const NO_SHIPPING         = 'no_shipping';
	const NO_SHIPPING_DEFAULT = 1;
	const LC                  = 'lc';
	

	const DEDUCT_SHIPPING = 'deduct_shipping';
	const DEDUCT_HANDLING = 'deduct_handling';
	const DEDUCT_TAX      = 'deduct_tax';
	const DEDUCT_FEE      = 'deduct_fee';

	const DEDUCT_SHIPPING_DEFAULT = true;
	const DEDUCT_HANDLING_DEFAULT = true;
	const DEDUCT_TAX_DEFAULT      = true;
	const DEDUCT_FEE_DEFAULT      = false;

	// PayPal
	const RECEIVER_EMAIL         = 'receiver_email';
	const SANDBOX_RECEIVER_EMAIL = 'sandbox_receiver_email';
	const SANDBOX         = 'sandbox';
	const SANDBOX_DEFAULT = false;
	
	const LIVE_URL        = 'https://www.paypal.com/webscr';
	const SANDBOX_URL     = 'https://www.sandbox.paypal.com/webscr';

	private static $debug = false;
	private static $admin_messages = array();

	/**
	* Tasks performed upon plugin activation.
	*/
	public static function activate() {
		$options = get_option( self::PLUGIN_OPTIONS , null );
		if ( $options === null ) {
			$options = array();
			$options[self::KEY] = md5( rand() ); // 
			$options[self::DELETE_DATA] = false;
			// add the options and there's no need to autoload these
			add_option( self::PLUGIN_OPTIONS, $options, null, 'no' );
		}
	}

	/**
	 * On deactivation hook.
	 */
	public static function deactivate() {
		$options = get_option( self::PLUGIN_OPTIONS , array() );
		if ( isset( $options[self::DELETE_DATA] ) && $options[self::DELETE_DATA] ) {
			delete_option( self::PLUGIN_OPTIONS );
			delete_option( 'groups_paypal_flushed' );
		}
		flush_rewrite_rules();
	}

	/**
	 * On uninstall hook.
	 * Deletes options and tables if requested.
	 */
	public static function uninstall() {
		$options = get_option( self::PLUGIN_OPTIONS , array() );
		if ( isset( $options[self::DELETE_DATA] ) && $options[self::DELETE_DATA] ) {
			delete_option( self::PLUGIN_OPTIONS );
		}
	}

	/**
	 * Prints admin notices.
	 */
	public static function admin_notices() {
		if ( !empty( self::$admin_messages ) ) {
			foreach ( self::$admin_messages as $msg ) {
				echo $msg;
			}
		}
	}

	/**
	 * Initialize plugin with dependency check.
	 */
	public static function init() {
		register_activation_hook( GROUPS_PAYPAL_FILE, array( __CLASS__, 'activate' ) );
		register_deactivation_hook( GROUPS_PAYPAL_FILE, array( __CLASS__, 'deactivate' ) );
		add_action( 'admin_notices', array( __CLASS__, 'admin_notices' ) );
		if ( self::check_dependencies() ) {
			add_action( 'plugins_loaded', array( __CLASS__, 'plugins_loaded' ) );
			require_once( GROUPS_PAYPAL_VIEW_LIB . '/class-groups-paypal-shortcodes.php' );
			// admin
			if ( is_admin() ) {
				require_once( GROUPS_PAYPAL_ADMIN_LIB . '/class-groups-paypal-admin.php' );
			}
			add_filter( 'groups_subscriptions_supported_currencies', array( __CLASS__, 'groups_subscriptions_supported_currencies' ) );
		}
	}

	/**
	 * Pulls in the subscription post type from Groups Subscriptions.
	 * Hooked on init (which comes after plugins_loaded) to make sure everything is defined when needed.
	 */
	public static function plugins_loaded() {
		require_once( GROUPS_SUBSCRIPTIONS_EXT_LIB . '/class-groups-subscription-post-type.php' );
		if ( !get_option( 'groups_paypal_flushed', false ) ) {
			add_action( 'init', array( __CLASS__, 'flush') );
		}
	}

	public static function flush() {
		flush_rewrite_rules();
		add_option( 'groups_paypal_flushed', true, null, 'no' );
	}

	/**
	 * Supported currencies 
	 * @link https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_currency_codes 
	 */
	public static function groups_subscriptions_supported_currencies( $currencies ) {
		return array(
			'AUD' => array( 'label' => __( 'Australian Dollar', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ),
			'BRL' => array( 'label' => __( 'Brazilian Real', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'CAD' => array( 'label' => __( 'Canadian Dollar', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ),
			'CZK' => array( 'label' => __( 'Czech Koruna', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ), 
			'DKK' => array( 'label' => __( 'Danish Krone', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'EUR' => array( 'label' => __( 'Euro', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#8364;' ),
			'HKD' => array( 'label' => __( 'Hong Kong Dollar', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ), 
			'HUF' => array( 'label' => __( 'Hungarian Forint', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'ILS' => array( 'label' => __( 'Israeli New Sheqel', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'JPY' => array( 'label' => __( 'Japanese Yen', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#165;' ),
			'MYR' => array( 'label' => __( 'Malaysian Ringgit', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'MXN' => array( 'label' => __( 'Mexican Peso', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'NOK' => array( 'label' => __( 'Norwegian Krone', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'NZD' => array( 'label' => __( 'New Zealand Dollar', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#36;' ),
			'PHP' => array( 'label' => __( 'Philippine Peso', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'PLN' => array( 'label' => __( 'Polish Zloty', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'GBP' => array( 'label' => __( 'Pound Sterling', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#163;' ),
			'SGD' => array( 'label' => __( 'Singapore Dollar', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'SEK' => array( 'label' => __( 'Swedish Krona', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'CHF' => array( 'label' => __( 'Swiss Franc', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'TWD' => array( 'label' => __( 'Taiwan New Dollar', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'THB' => array( 'label' => __( 'Thai Baht', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'TRY' => array( 'label' => __( 'Turkish Lira', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '' ),
			'USD' => array( 'label' => __( 'U.S. Dollar', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'symbol' => '&#36;' )
		);
	}

	public static function check_dependencies( $disable = false ) {
		$result = true;
		$active_plugins = get_option( 'active_plugins', array() );
		if ( is_multisite() ) {
			$active_sitewide_plugins = get_site_option( 'active_sitewide_plugins', array() );
			$active_sitewide_plugins = array_keys( $active_sitewide_plugins );
			$active_plugins = array_merge( $active_plugins, $active_sitewide_plugins );
		}
		$groups_is_active = in_array( 'groups/groups.php', $active_plugins );
		$groups_subscriptions_is_active = in_array( 'groups-subscriptions/groups-subscriptions.php', $active_plugins );
		if ( !$groups_is_active ) {
			self::$admin_messages[] = "<div class='error'>" . __( '<strong>Groups PayPal</strong> requires <a href="http://www.itthinx.com/plugins/groups" target="_blank">Groups</a>.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . "</div>";
		}
		if ( !$groups_subscriptions_is_active ) {
			self::$admin_messages[] = "<div class='error'>" . __( '<strong>Groups PayPal</strong> requires <a href="http://www.itthinx.com/plugins/groups-subscriptions" target="_blank">Groups Subscriptions</a>.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . "</div>";
		}
		if ( !$groups_is_active || !$groups_subscriptions_is_active ) {
			if ( $disable ) {
				include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
				deactivate_plugins( array( __FILE__ ) );
			}
			$result = false;
		}
		return $result;
	}

	/**
	 * Returns the PayPal URL to post forms to.
	 * @return string PayPal's live or sandbox URL
	 */
	public static function get_paypal_url() {
		$url = self::LIVE_URL;
		$options = get_option( self::PLUGIN_OPTIONS , array() );
		$sandbox = isset( $options[self::SANDBOX] ) ? $options[self::SANDBOX] : self::SANDBOX_DEFAULT;
		if ( $sandbox ) {
			$url = self::SANDBOX_URL;
		}
		return $url;
	}

	/**
	 * Returns the PayPal receiver email depending on whether sandbox mode
	 * is enabled or not.
	 * @return string receiver email or sandbox receiver email
	 */
	public static function get_receiver_email() {
		$options = get_option( self::PLUGIN_OPTIONS , array() );
		$sandbox = isset( $options[self::SANDBOX] ) ? $options[self::SANDBOX] : self::SANDBOX_DEFAULT;
		if ( $sandbox ) {
			$receiver_email = isset( $options[Groups_PayPal::SANDBOX_RECEIVER_EMAIL] ) ? $options[Groups_PayPal::SANDBOX_RECEIVER_EMAIL] : '';
		} else {
			$receiver_email = isset( $options[Groups_PayPal::RECEIVER_EMAIL] ) ? $options[Groups_PayPal::RECEIVER_EMAIL] : '';
		}
		return $receiver_email;
	}

	/**
	 * Return the IPN notification URL, prefixed with http:// or https://
	 * according to the SECURE_URLS option.
	 * @return notification URL
	 */
	public static function get_notify_url() {
		$options     = get_option( self::PLUGIN_OPTIONS , array() );
		$secure_urls = isset( $options[self::SECURE_URLS] ) ? $options[self::SECURE_URLS] : self::SECURE_URLS_DEFAULT;
		$url         = GROUPS_PAYPAL_PLUGIN_URL . 'lib/ipn.php';
		if ( stripos( $url, 'http://' ) === 0 ) {
			$url = substr( $url, 7 );
		} else if ( stripos( $url, 'https://' ) === 0 ) {
			$url = substr( $url, 8 );
		}
		if ( $secure_urls ) {
			$url = 'https://' . $url;
		} else {
			$url = 'http://' . $url;
		}
		return $url;
	}
	
	/**
	 * Suggests http or https based on secure urls setting when the url
	 * is not already prefixed with http:// or https://
	 * @param string $url
	 * @return srting url
	 */
	public static function maybe_prefix_url( $url ) {
		$options     = get_option( self::PLUGIN_OPTIONS , array() );
		$secure_urls = isset( $options[self::SECURE_URLS] ) ? $options[self::SECURE_URLS] : self::SECURE_URLS_DEFAULT;
		if ( ( stripos( $url, 'http://' ) !== 0 ) && ( stripos( $url, 'https://' ) !== 0 ) ) {
			if ( $secure_urls ) {
				$url = 'https://' . $url;
			} else {
				$url = 'http://' . $url;
			}
		}
		return $url;
	}

	/**
	 * Remove the uninstall hook.
	 * 
	 * @param string $file
	 * @param string|array $callback
	 */
	public static function remove_uninstall_hook( $file, $callback ) {
		$uninstallable_plugins = (array) get_option( 'uninstall_plugins' );
		unset( $uninstallable_plugins[plugin_basename($file)] );
		update_option('uninstall_plugins', $uninstallable_plugins);
	}
}

Groups_PayPal::init();
