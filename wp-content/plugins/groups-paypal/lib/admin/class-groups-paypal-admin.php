<?php
/** 
 * Copyright (c) 2012 "kento" Karim Rahimpur www.itthinx.com
 * 
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 */

class Groups_PayPal_Admin {
	

	const NONCE = 'groups_paypal_ipn_admin_nonce';
	const SET_ADMIN_OPTIONS = 'set_admin_options';

	const DISMISS_USAGE = 'dismiss_usage';

	/**
	 * Initialize plugin with dependency check.
	 */
	public static function init() {
		add_action( 'groups_admin_menu', array( __CLASS__, 'groups_admin_menu' ) );
		add_action( 'wp_before_admin_bar_render', array( __CLASS__, 'wp_before_admin_bar_render' ) );
		add_action( 'contextual_help', array( __CLASS__, 'contextual_help' ), 10, 3 );
	}

	/**
	 * Submenu item under Groups.
	 */
	public static function groups_admin_menu() {
		$page = add_submenu_page(
			'groups-admin',
			__( 'PayPal', GROUPS_PAYPAL_PLUGIN_DOMAIN ),
			__( 'PayPal', GROUPS_PAYPAL_PLUGIN_DOMAIN ),
			GROUPS_ADMINISTER_GROUPS,
			'groups-admin-paypal_ipn',
			array( __CLASS__, 'groups_admin_paypal_ipn' )
		);
		$pages[] = $page;
		add_action( 'admin_print_styles-' . $page, array( __CLASS__, 'admin_print_styles' ) );
// 		add_action( 'admin_print_scripts-' . $page, array( __CLASS__, 'admin_print_scripts' ) );
	}

	public static function admin_print_styles() {
		wp_enqueue_style('groups_paypal_admin', GROUPS_PAYPAL_PLUGIN_URL . '/css/groups-paypal-admin.css', array(), GROUPS_PAYPAL_VERSION );
	}

	/**
	 * Renders the 'Sandbox mode' admin bar menu item which links to the
	 * PayPal IPN options page. 
	 */
	public static function wp_before_admin_bar_render() {
		global $wp_admin_bar;
		if ( !is_object( $wp_admin_bar ) ) {
			return false;
		}
		if ( current_user_can( GROUPS_ADMINISTER_OPTIONS ) ) {
			$options = get_option( Groups_PayPal::PLUGIN_OPTIONS , array() );
			$sandbox = isset( $options[Groups_PayPal::SANDBOX] ) ? $options[Groups_PayPal::SANDBOX] : Groups_PayPal::SANDBOX_DEFAULT;
			if ( $sandbox ) {
				$wp_admin_bar->add_menu(
					array(
						'id' => 'groups_paypal',
						'title' => __( "Sandbox mode", GROUPS_PAYPAL_PLUGIN_DOMAIN ),
						'href' => get_option( 'siteurl' ) . '/wp-admin/admin.php?page=groups-admin-paypal_ipn',
						'meta' => array( 'title' => __( 'Groups PayPal is in sandbox mode', GROUPS_PAYPAL_PLUGIN_DOMAIN) )
					)
				);
			}
		}
	}
	
	/**
	* PayPal admin section.
	*/
	public static function groups_admin_paypal_ipn() {

		$output = '';

		if ( !current_user_can( GROUPS_ADMINISTER_OPTIONS ) ) {
			wp_die( __( 'Access denied.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) );
		}
		
		if ( isset( $_GET[self::DISMISS_USAGE] ) ) {
			if ( $_GET[self::DISMISS_USAGE] ) {
				update_user_meta( get_current_user_id(), Groups_PayPal::PLUGIN_OPTIONS . '-' . self::DISMISS_USAGE, true );
			} else {
				delete_user_meta( get_current_user_id(), Groups_PayPal::PLUGIN_OPTIONS . '-' . self::DISMISS_USAGE );
			}
		}

		$options = get_option( Groups_PayPal::PLUGIN_OPTIONS , array() );

		if ( isset( $_POST['submit'] ) ) {
			if ( wp_verify_nonce( $_POST[self::NONCE], self::SET_ADMIN_OPTIONS ) ) {
				$options[Groups_PayPal::SANDBOX]                = !empty( $_POST[Groups_PayPal::SANDBOX] );
				$options[Groups_PayPal::RECEIVER_EMAIL]         = filter_var( trim( $_POST[Groups_PayPal::RECEIVER_EMAIL] ), FILTER_VALIDATE_EMAIL );
				$options[Groups_PayPal::SANDBOX_RECEIVER_EMAIL] = filter_var( trim( $_POST[Groups_PayPal::SANDBOX_RECEIVER_EMAIL] ), FILTER_VALIDATE_EMAIL );

				$options[Groups_PayPal::SECURE_URLS] = !empty( $_POST[Groups_PayPal::SECURE_URLS] );
				$options[Groups_PayPal::RETURN_URL]  = filter_var( Groups_PayPal::maybe_prefix_url( trim( $_POST[Groups_PayPal::RETURN_URL] ) ), FILTER_VALIDATE_URL );
				$options[Groups_PayPal::CANCEL_URL]  = filter_var( Groups_PayPal::maybe_prefix_url( trim( $_POST[Groups_PayPal::CANCEL_URL] ) ), FILTER_VALIDATE_URL );

				$options[Groups_PayPal::NO_NOTE]     = !empty( $_POST[Groups_PayPal::NO_NOTE] ) ? 1 : 0;
				$options[Groups_PayPal::NO_SHIPPING] = !empty( $_POST[Groups_PayPal::NO_SHIPPING] ) ? intval( $_POST[Groups_PayPal::NO_SHIPPING] ) : 0;
				if ( $options[Groups_PayPal::NO_SHIPPING] < 0 ) {
					$options[Groups_PayPal::NO_SHIPPING] = 0;
				}
				if ( $options[Groups_PayPal::NO_SHIPPING] > 2 ) {
					$options[Groups_PayPal::NO_SHIPPING] = 2;
				}
				$options[Groups_PayPal::LC]          = trim( $_POST[Groups_PayPal::LC] );

				$options[Groups_PayPal::DELETE_DATA] = !empty( $_POST[Groups_PayPal::DELETE_DATA] );
			}
			update_option( Groups_PayPal::PLUGIN_OPTIONS, $options );
		}

		$sandbox                = isset( $options[Groups_PayPal::SANDBOX] ) ? $options[Groups_PayPal::SANDBOX] : Groups_PayPal::SANDBOX_DEFAULT;
		$receiver_email         = isset( $options[Groups_PayPal::RECEIVER_EMAIL] ) ? $options[Groups_PayPal::RECEIVER_EMAIL] : '';
		$sandbox_receiver_email = isset( $options[Groups_PayPal::SANDBOX_RECEIVER_EMAIL] ) ? $options[Groups_PayPal::SANDBOX_RECEIVER_EMAIL] : '';

		$secure_urls       = isset( $options[Groups_PayPal::SECURE_URLS] ) ? $options[Groups_PayPal::SECURE_URLS] : Groups_PayPal::SECURE_URLS_DEFAULT;
		$return_url        = isset( $options[Groups_PayPal::RETURN_URL] ) ? esc_attr( wp_filter_nohtml_kses( $options[Groups_PayPal::RETURN_URL] ) ) : '';
		$cancel_url        = isset( $options[Groups_PayPal::CANCEL_URL] ) ? esc_attr( wp_filter_nohtml_kses( $options[Groups_PayPal::CANCEL_URL] ) ) : '';
		
		$no_note           = isset( $options[Groups_PayPal::NO_NOTE] ) ? $options[Groups_PayPal::NO_NOTE] : Groups_PayPal::NO_NOTE_DEFAULT;
		$no_shipping       = isset( $options[Groups_PayPal::NO_SHIPPING] ) ? $options[Groups_PayPal::NO_SHIPPING] : Groups_PayPal::NO_SHIPPING_DEFAULT;
		$lc                = isset( $options[Groups_PayPal::LC] ) ? esc_attr( wp_filter_nohtml_kses( $options[Groups_PayPal::LC] ) ) : '';

		$delete_data       = isset( $options[Groups_PayPal::DELETE_DATA] ) ? $options[Groups_PayPal::DELETE_DATA] : false;

		if ( $delete_data ) {
			register_uninstall_hook( GROUPS_PAYPAL_FILE, array( 'Groups_PayPal', 'uninstall' ) );
		} else {
			Groups_PayPal::remove_uninstall_hook( GROUPS_PAYPAL_FILE, array( 'Groups_PayPal', 'uninstall' ) );
		}

		echo
			'<h2>' .
			__( 'Settings', GROUPS_PAYPAL_PLUGIN_DOMAIN ) .
			'</h2>';

		$output .= '<form action="" name="options" method="post">';
		$output .= '<div class="settings">';

		if ( !get_user_meta( get_current_user_id(), Groups_PayPal::PLUGIN_OPTIONS . '-' . self::DISMISS_USAGE, true ) ) {
			$output .= __( 'The information below is available from the Help section on this page.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
			$output .= ' ';
			$dismiss_url = admin_url( 'admin.php?page=groups-admin-paypal_ipn' ) . '&' . self::DISMISS_USAGE . '=1';
			$output .= '<a href="' . esc_url( $dismiss_url ) . '">' . __( 'Ok I got it. Don\'t show it here anymore.', GROUPS_PAYPAL_PLUGIN_DOMAIN  ) . '</a>';
			$output .= self::get_help();
		}

		$output .= '<h3>' . __( 'PayPal', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h3>';
		
		$output .= '<h4>' . __( 'Email address', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h4>';
		$pp_email_class = '';
		if ( !$sandbox && !$receiver_email ) {
			$pp_email_class = ' class="error" ';
		}
		$output .= '<p ' . $pp_email_class . '>';
		$output .= '<label style="display:block" for="' . Groups_PayPal::RECEIVER_EMAIL . '">' . __( 'Payment recipient email address.', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</label>';
		$output .= '<input style="width:40em" name="' . Groups_PayPal::RECEIVER_EMAIL . '" type="text" value="' . $receiver_email . '" />';
		$output .= '<br/>';
		$output .= '<span style="display:block" class="description">' . __( 'This must be an email address of your PayPal account to receive payments.', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</span>';
		$output .= '</p>';

		$output .= '<h4>' . __( 'Sandbox mode', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h4>';
		$output .= '<p>';
		$output .= '<input name="' . Groups_PayPal::SANDBOX . '" type="checkbox" ' . ( $sandbox ? ' checked="checked" ' : '' ) . ' />';
		$output .= '&nbsp;';
		$output .= '<label for="' . Groups_PayPal::SANDBOX . '">' . __( 'Work in sandbox mode', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</label>';
		$output .= '</p>';
		$output .= '<p class="description">' . __( 'Use the sandbox mode for testing purposes.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</p>';
		
		$pp_sandbox_email_class = '';
		if ( $sandbox && !$sandbox_receiver_email ) {
			$pp_sandbox_email_class = ' class="error" ';
		}
		$output .= '<p ' . $pp_sandbox_email_class . '>';
		$output .= '<label style="display:block" for="' . Groups_PayPal::SANDBOX_RECEIVER_EMAIL . '">' . __( 'Sandbox payment recipient email address.', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</label>';
		$output .= '<input style="width:40em" name="' . Groups_PayPal::SANDBOX_RECEIVER_EMAIL . '" type="text" value="' . esc_attr( $sandbox_receiver_email ) . '" />';
		$output .= '<br/>';
		$output .= '<span style="display:block" class="description">' . __( 'This must be the an email address of your PayPal Sandbox account.', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</span>';
		$output .= '</p>';

		$output .= '<h4>' . __( 'URLs', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h4>';
		
		$output .= '<p>';
		$output .= sprintf( __( 'The notication URL for IPN messages from PayPal is <code>%s</code>.', GROUPS_PAYPAL_PLUGIN_DOMAIN ), Groups_PayPal::get_notify_url() );
		$output .= '</p>';

		$output .= '<p>';
		$output .= '<input name="' . Groups_PayPal::SECURE_URLS . '" type="checkbox" ' . ( $secure_urls ? ' checked="checked" ' : '' ) . ' />';
		$output .= '&nbsp;';
		$output .= '<label for="' . Groups_PayPal::SECURE_URLS . '">' . __( 'Use secure URLs.', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</label>';
		$output .= '<br/>';
		$output .= '<span class="description">';
		$output .= sprintf( __( 'If this option is checked, the IPN URL will use https instead of http. It is recommended to use secure URLs, but your web server must use a <strong>valid security certificate</strong>. Notifications will <strong>fail</strong> if secure URLs is enabled and an invalid security certificate is used. You can <a href="%s" target="_blank">visit</a> the notification URL to see if your browser issues any warnings.', GROUPS_PAYPAL_PLUGIN_DOMAIN ), Groups_PayPal::get_notify_url() );
		$output .= '</span>';
		$output .= '</p>';
		
		$output .= '<p>';
		$output .= '<label style="display:block" for="' . Groups_PayPal::RETURN_URL . '">' . __( 'Return URL', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</label>';
		$output .= '<input style="width:40em" name="' . Groups_PayPal::RETURN_URL . '" type="text" value="' . $return_url . '" />';
		$output .= '<br/>';
		$output .= '<span class="description">';
		$output .= __( 'The URL to which PayPal redirects the buyers after they complete their payments. You can specify the URL of a page on your site that displays a notice according to the <a href="https://www.paypal.com/hk/cgi-bin/webscr?cmd=p/pop/express_return_reqs-outside">Return URL Requirements</a>. By default, PayPal redirects the browser to a PayPal webpage. The <code>[groups_paypal_return]</code> shortcode can be used to render details about the subscription purchased on that page.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
		$output .= '</span>';
		$output .= '</p>';
		
		$output .= '<p>';
		$output .= '<label style="display:block" for="' . Groups_PayPal::CANCEL_URL . '">' . __( 'Cancel URL', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</label>';
		$output .= '<input style="width:40em" name="' . Groups_PayPal::CANCEL_URL . '" type="text" value="' . $cancel_url . '" />';
		$output .= '<br/>';
		$output .= '<span class="description">';
		$output .= __( 'The URL to which PayPal redirects the buyers if they cancel checkout before completing their payments. For example, specify the URL of a page on your site that displays &quot;The payment has been cancelled&quot;. By default, PayPal redirects the browser to a PayPal webpage.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
		$output .= '</span>';
		$output .= '</p>';
		
		$output .= '<p>';
		$output .= sprintf( __( 'To automatically redirect buyers back to your site after payment, you should  enable <a href="%s">Auto Return</a> for your PayPal account.', GROUPS_PAYPAL_PLUGIN_DOMAIN ), 'https://www.paypal.com/ie/cgi-bin/webscr?cmd=p/mer/express_return_summary-outside' );
		$output .= '</p>';
		
		$output .= '<h4>' . __( 'Other', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h4>';
		
		$output .= '<p>';
		$output .= '<label>';
		$output .= '<input type="checkbox" name="' . Groups_PayPal::NO_NOTE . '" ' . ( $no_note ? ' checked="checked" ' : '' ) . ' />';
		$output .= ' ' . __( 'Do not prompt buyers to include a note with their payments.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
		$output .= '</label>';
		$output .= '</p>';
		
		$no_shipping_options = array(
			'0' => __( 'prompt for an address, but do not require one', GROUPS_PAYPAL_PLUGIN_DOMAIN ),
			'1' => __( 'do not prompt for an address', GROUPS_PAYPAL_PLUGIN_DOMAIN ),
			'2' => __( 'prompt for an address, and require one', GROUPS_PAYPAL_PLUGIN_DOMAIN ),
		);
		$output .= '<p>';
		$output .= '<label>';
		$output .=  __( 'Shipping address', GROUPS_PAYPAL_PLUGIN_DOMAIN );
		$output .= ' ';
		$output .= '<select name="' . Groups_PayPal::NO_SHIPPING . '">';
		foreach( $no_shipping_options as $key => $label ) {
			$output .= sprintf( '<option value="%d" %s>%s</option>', esc_attr( $key ), $key === $no_shipping ? ' selected="selected" ' : '', $label );
		}
		$output .= '</select>';
		$output .= '</label>';
		$output .= '</p>';
		
		$output .= '<p>';
		$output .= '<label>';
		$output .= __( 'PayPal locale', GROUPS_PAYPAL_PLUGIN_DOMAIN );
		$output.= ' ';
		$output .= '<input style="width:4em" name="' . Groups_PayPal::LC . '" type="text" value="' . $lc . '" />';
		$output .= '</label>';
		$output .= '<br/>';
		$output .= '<span class="description">';
		$output .= __( 'The locale of the login or sign-up page, which may have the specific country\'s language available, depending on localization. If unspecified, PayPal determines the locale by using a cookie in the subscriber’s browser. If there is no PayPal cookie, the default locale is US. Please refer to <a href="https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_html_Appx_websitestandard_htmlvariables">HTML Variables for PayPal Payments Standard</a> for allowed values.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
		$output .= '</span>';
		$output .= '</p>';

		$output .= '<h3>' . __( 'Delete data', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h3>';
		$output .= '<p>';
		$output .= '<input name="' . Groups_PayPal::DELETE_DATA . '" type="checkbox" ' . ( $delete_data ? ' checked="checked" ' : '' ) . ' />';
		$output .= '&nbsp;';
		$output .= '<label for="' . Groups_PayPal::DELETE_DATA . '">' . __( 'Delete plugin data when the plugin is deleted?', GROUPS_PAYPAL_PLUGIN_DOMAIN) . '</label>';
		$output .= '</p>';
		$output .= '<p class="description warning">' . __( 'CAUTION: This will delete this plugin&lsquo;s data and settings when the plugin is deleted. By enabling this option you agree to be solely responsible for any loss of data or any other consequences thereof.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</p>';

		$output .= '<p>' . __( 'Click <em>Save</em> to store the settings.', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</p>';
		$output .= '<p>';
		$output .= wp_nonce_field( self::SET_ADMIN_OPTIONS, self::NONCE, true, false );
		$output .= '<input class="button" type="submit" name="submit" value="' . __( 'Save', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '"/>';
		$output .= '</p>';

		$output .= '</div>'; // .settings
		$output .= '</form>';

		echo $output;


	}
	
	public static function contextual_help( $contextual_help, $screen_id, $screen ) {
		$output = '';
		switch ( $screen_id ) {
			case 'groups_page_groups-admin-paypal_ipn' :
				$output .= self::get_help();
				break;
		}
		return $output;
	}
	
	public static function get_help( $what = 'usage' ) {
		$output = '';
		switch( $what ) {
			case 'usage' :
				$output .= '<div class="help">';
				$output .= '<h3>' . __( 'Groups PayPal', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h3>';
				
				$output .= '<p>';
				$output .= __( 'Groups PayPal uses PayPal Payments Standard and captures PayPal\'s IPN messages to track payments.' , GROUPS_PAYPAL_PLUGIN_DOMAIN );
				$output .= '</p>';
				
				$output .= '<h4>' . __( 'Setup', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h4>';
				
				$output .= '<ul>';
				$output .= '<li>';
				$output .= __( 'Carefully <strong>read the information</strong> about all settings on this page and adjust them adequately.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
				$output .= '</li>';
				$output .= '<li>';
				$output .= __( 'Make sure to fill in the <strong>email address</strong> of your PayPal account where payments should be received.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
				$output .= '</li>';
				$output .= '<li>';
				$output .= __( 'Use the <strong>Sandbox mode</strong> to test your setup and disable it when going live.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
				$output .= '</li>';
				$output .= '</ul>';
				
				$output .= '<h4>' . __( 'Usage', GROUPS_PAYPAL_PLUGIN_DOMAIN ) . '</h4>';
				
				$output .= '<ul>';
				$output .= '<li>';
				$output .= sprintf( __( 'Add a <a href="%s">New Subscription</a>.', GROUPS_PAYPAL_PLUGIN_DOMAIN ), admin_url( 'post-new.php?post_type=subscription' ) );
				$output .= '</li>';
				$output .= '<li>';
				$output .= __( 'Use the <code>[groups_paypal_subscription]</code> shortcode on a subscription page to allow users to purchase the subscription.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
				$output .= '</li>';
				$output .= '<li>';
				$output .= sprintf( __( 'To allow users to purchase a subscription on any page, use the <code>[groups_paypal_subscription id="x"]</code> shortcode, where <code>x</code> is the post ID of a <a href="%s">Subscription</a>.', GROUPS_PAYPAL_PLUGIN_DOMAIN ), admin_url( 'edit.php?post_type=subscription' ) );
				$output .= '</li>';
				$output .= '<li>';
				$output .= sprintf( __( 'The <code>[groups_paypal_return]</code> shortcode can be used to render information about a subscription purchased after payment. The shortcode should be placed on the page that the <em>Return URL</em> points to.', GROUPS_PAYPAL_PLUGIN_DOMAIN ), admin_url( 'edit.php?post_type=subscription' ) );
				$output .= '</li>';
				$output .= '<li>';
				$output .= sprintf( __( 'If you get <strong>Page not found</strong> (404) when trying to visit a Subscription page, please visit <a href="%s">Permalinks</a> to rebuild the permalink structure.', GROUPS_PAYPAL_PLUGIN_DOMAIN ), admin_url( 'options-permalink.php') );
				$output .= '</li>';
				$output .= '<li>';
				$output .= __( 'Please also refer to the <a href="http://www.itthinx.com/documentation/groups-paypal/">Groups PayPal</a> documentation pages.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
				$output .= '</li>';
				$output .= '</ul>';

				$output .= '<p class="description">';
				$output .= __( 'PayPal is a registered trademark of PayPal, Inc.', GROUPS_PAYPAL_PLUGIN_DOMAIN );
				$output .= '</p>';
				
				$output .= '</div>';
				
				break;
		}
		return $output;
	}

}

Groups_PayPal_Admin::init();
