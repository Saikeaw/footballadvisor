=== Paid Memberships Pro - Reports Dashboard Add On ===
Contributors: strangerstudios
Tags: pmpro, reports, admin, mobile reports
Requires at least: 3.5
Tested up to: 4.3
Stable tag: .1.1

== Description ==
Responsive Membership Reports Dashboard for Admins and Membership Manager Role. Quick overview of the most important stats for your membership site.

Features:
Simply install and activate the plugin and the reports dashboard will be added to your site at the URL www.YOURDOMAIN.com/?pmpro_reports=true. You must be logged in as the administrator or membership manager role to view the page. Others will be redirected to the home_url.

== Installation ==

1. Upload the `pmpro-reports-dashboard` directory to the `/wp-content/plugins/` directory of your site.
1. Activate the plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==

= I found a bug in the plugin. =

Please post it in the issues section of GitHub and we'll fix it as soon as we can. Thanks for helping. https://github.com/strangerstudios/pmpro-reports-dashboard/issues

== Changelog ==
= .1.1 =
* Added readme.txt
* Added link to view full report in WordPress admin.

= .1 =
* Initial version of the plugin.