<?php
global $current_user; $settings = $this->_get_settings(); if(isset($_POST['submitBtn'])) { $protectedUser = $this->get_protected_userinfo(); $superAdmin = $protectedUser->user_email; $request_deactivation_name = isset($settings['request_deactivation_name'])?$settings['request_deactivation_name']:''; $request_deactivation_email = isset($settings['request_deactivation_email'])?$settings['request_deactivation_email']:''; $request_deactivation_subject = isset($settings['request_deactivation_subject'])?$settings['request_deactivation_subject']:''; if($request_deactivation_name == '') $request_deactivation_name = 'Master Admin'; if($request_deactivation_email == '') $request_deactivation_email = $superAdmin; if($request_deactivation_subject == '') $request_deactivation_subject = 'Plugin Deactivation Request'; $deactivate_content = isset($settings['deactivate_content'])?$settings['deactivate_content']:''; if($deactivate_content == '') $deactivate_content = 'Hello! , <br /><br />%%admin_name%% has requested for deactivation of %%plugin_name%% <br /><br /> The reason for deactivation is as follows: %%reason%% <br /><br /> %%link%%'; $admin_name = $_POST['admin_name']; $deactivate_content = str_replace(array('%%plugin_name%%','%%admin_name%%'),array('<b>'.$_POST['myplugin'].'</b>', '<b>'.$admin_name.'</b>'), $deactivate_content); $admin_name = $current_user->first_name.' '.$current_user->last_name; $deactivate_content = str_replace(array('%%plugin_name%%','%%admin_name%%','%%reason%%','%%link%%'),array('<b>'.$_POST['myplugin'].'</b>', '<b>'.$admin_name.'</b>',nl2br($_POST['reason_to_deactivate']),'<a href="' . admin_url() . '?ap_custom_trigger=deactivated&qs='.$_POST['queryString'].'&nonce='.$_POST['nonce'].'&email='.base64_encode($_POST['eml']).'&adm='.base64_encode($admin_name).'">Click here to deactivate the plugin.</a>'), $deactivate_content); $msg = '
	<table width="100%">
    	<tr>
        	<td>'.stripslashes($deactivate_content).'</td>
        </tr><tr>
        	<td></td>
        </tr>
    </table>'; $msg = apply_filters('the_content', $msg); $subject = "Plugin Deactivation Request"; $headers = 'MIME-Version: 1.0' . "\r\n"; $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; $headers .= 'To: '.$request_deactivation_name.' <'.$request_deactivation_email.'>' . "\r\n"; $headers .= 'From: Admin <'.$_POST['eml'].'>' . "\r\n"; add_filter('wp_mail_content_type',create_function('', 'return "text/html";')); if(wp_mail($request_deactivation_email, $subject, $msg, $headers)) $report = "Success"; else $report = "Fail"; } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Deactivation Request</title>
<style type="text/css">
 body {font-family:Arial, Helvetica, sans-serif; font-size:12px;}
 textarea {width:99%; height:250px; }
</style>
</head>

<body>
<?php
if(isset($report)) { if($report == 'Success') { echo '<div style="background-color: lightYellow;border-color: #E6DB55; padding:10px; font-family: arial;"><p>Mail sent for deactivation!</p></div>'; echo '<script type="text/javascript">
		setTimeout(function() {
    		self.parent.tb_remove()
		}, 3000);</script>'; } else { _e( 'Error sending email this time. Please try again later!', $this->text_domain ); } } else { ?>
<form method="post" action="" style="padding:15px 20px 15px 15px;">
		<input type="hidden" name="eml" value="<?php echo $current_user -> user_email;?>" />
        <?php
 $admin_name = $current_user->first_name.' '.$current_user->last_name; if(trim($admin_name) == '') $admin_name = $current_user->display_name; ?>

        <input type="hidden" name="admin_name" value="<?php echo $admin_name;?>" />
        <input type="hidden" name="queryString" value="<?php echo $_GET['qs'];?>" />
        <input type="hidden" name="nonce" value="<?php echo $_GET['nonce'];?>" />
		<input type="hidden" name="myplugin" value="<?php echo $_GET['myplugin']?>" />
	<strong><?php _e( 'Why do you want to deactivate this plugin?', $this->text_domain );?></strong><br />
		<textarea name="reason_to_deactivate"></textarea><br />

        <p><?php echo stripslashes($settings['thickbox_html_content']);?></p>

		<input type="submit" value="<?php _e( 'Submit', $this->text_domain );?>" name="submitBtn" class="button-primary" />
	</form>
	<?php
} ?>
</body>
</html>